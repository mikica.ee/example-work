<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Compare extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('url'));
        $this->load->model('newproduct_model');
    }

    public function index() {
        $products = array();
        if (isset($_SESSION['compare']))
        foreach ($_SESSION['compare'] as $key => $value) {
            $product = $this->newproduct_model->select_from_id($value);
            $product[0]['key'] = $key;
            $products[] = $product[0];
        }
        $data['products'] = $products;
        $this->load->view('header');
        $this->load->view('cart/compare', $data);
        $this->load->view('footer');
    }

    public function addproduct() {
        $id = $this->input->post('web_products_id');
        if (isset($_SESSION['compare'])) {
            if (!in_array($id, $_SESSION['compare'])) {
                if (count($_SESSION['compare']) > 3) {
                    array_shift($_SESSION['compare']);
                }
                $_SESSION['compare'][] = $id;
            }
        } else {
            $_SESSION['compare'] = array();
            $_SESSION['compare'][] = $id;
        }
    }
     public function delcompare() {
        unset($_SESSION['compare'][$_POST['key']]);
       
    }

}
