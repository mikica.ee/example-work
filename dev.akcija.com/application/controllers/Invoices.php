<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 *
 * @extends CI_Controller
 */
class Invoices extends CI_Controller {

    /**
     * __construct function.
     *
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('url'));
        $this->load->model(array('products_model', 'items_model', 'orders_model', 'gallery_model'));
    }

    public function invoice() {
        if ($orders = $this->orders_model->select_all_joined(["where" => '`order_id`=' . '"' . $this->uri->segment(3) . '"'])) {
            $items = $this->items_model->select_all_joined(["where" => '`item_order_id`=' . '"' . $this->uri->segment(3) . '"']);
            foreach ($items as $x => $item) {
                $gall = $this->gallery_model->select_all_joined(["where" => '`gallery_product_id`=' . $item['product_id'], "limit" => 1]);
                $items[$x]['img'] = $gall[0]['gallery_folder'] . "/" . $gall[0]['gallery_file'];
            }
            $data = ["orders" => $orders, "items" => $items];

            $html = $this->load->view('invoice/pdf', $data, true);

            $pdfFilePath = "faktura-" . $orders[0]['order_id'] . ".pdf";
            $this->load->library('m_pdf');
            $this->m_pdf->pdf->WriteHTML($html);
            $this->m_pdf->pdf->Output($pdfFilePath, "D");
        } else {
            redirect();
        }
    }

}
