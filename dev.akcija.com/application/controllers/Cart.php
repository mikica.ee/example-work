<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Cart extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('url'));
        $this->load->model('user_model');
        $this->load->model('gallery_model');
        $this->load->model('products_model');
        $this->load->model('web_products_model');
        $this->load->model('promotions_model');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('promomw_model');
    }

    public function index() {
        $this->load->view('header');

        $this->load->view('footer');
    }

    public function insertprommw() {
        //print_r($_POST);
        $item1 = $this->products_model->select_all_joined(["where" => "`product_id` = " . $_POST['po1']]);
        $item1 = $item1[0];

        $gallery1 = $this->gallery_model->select_all_joined(["where" => '`gallery_product_id`=' . $_POST['po1']]);
        $img1 = $gallery1[0];

        $item1['img'] = site_url('app/img/gallery/' . $img1['gallery_folder'] . '/' . $img1['gallery_file']);
        $item1['title'] = $_POST['promomw_po1'];

        $item2 = $this->products_model->select_all_joined(["where" => "`product_id` = " . $_POST['po2']]);
        $item2 = $item2[0];

        $gallery2 = $this->gallery_model->select_all_joined(["where" => '`gallery_product_id`=' . $_POST['po2']]);
        $img2 = $gallery2[0];

        $item2['img'] = site_url('app/img/gallery/' . $img2['gallery_folder'] . '/' . $img2['gallery_file']);
        $item2['title'] = $_POST['promomw_po2'];

        $prom = $this->promomw_model->select_all_joined(["where" => '`promomw_id`=' . $_POST['promomw_id']]);

        if (!empty($_SESSION['cart'])) {
            $br = max(array_keys($_SESSION['cart'])) + 1;
        } else {
            $br = 0;
        }
        $_SESSION['cart'][$br]['item1'] = $item1;
        $_SESSION['cart'][$br]['item2'] = $item2;
        $_SESSION['cart'][$br]['promomw'] = $prom[0];
        echo 'Успешно ја додадовте промоцијата: "' . $prom[0]['promomw_name'] . '" во Вашата кошница';
    }

    public function insertprommw3() {
        //print_r($_POST);
        $item1 = $this->products_model->select_all_joined(["where" => "`product_id` = " . $_POST['po1']]);
        $item1 = $item1[0];

        $gallery1 = $this->gallery_model->select_all_joined(["where" => '`gallery_product_id`=' . $_POST['po1']]);
        $img1 = $gallery1[0];

        $item1['img'] = site_url('app/img/gallery/' . $img1['gallery_folder'] . '/' . $img1['gallery_file']);
        $item1['title'] = $_POST['promomw_po1'];

        $item2 = $this->products_model->select_all_joined(["where" => "`product_id` = " . $_POST['po2']]);
        $item2 = $item2[0];

        $gallery2 = $this->gallery_model->select_all_joined(["where" => '`gallery_product_id`=' . $_POST['po2']]);
        $img2 = $gallery2[0];

        $item2['img'] = site_url('app/img/gallery/' . $img2['gallery_folder'] . '/' . $img2['gallery_file']);
        $item2['title'] = $_POST['promomw_po2'];

        $item3 = $this->products_model->select_all_joined(["where" => "`product_id` = " . $_POST['po3']]);
        $item3 = $item3[0];

        $gallery3 = $this->gallery_model->select_all_joined(["where" => '`gallery_product_id`=' . $_POST['po3']]);
        $img3 = $gallery3[0];

        $item3['img'] = site_url('app/img/gallery/' . $img3['gallery_folder'] . '/' . $img3['gallery_file']);
        $item3['title'] = $_POST['promomw_po3'];

        $prom = $this->promomw_model->select_all_joined(["where" => '`promomw_id`=' . $_POST['promomw_id']]);

        if (!empty($_SESSION['cart'])) {
            $br = max(array_keys($_SESSION['cart'])) + 1;
        } else {
            $br = 0;
        }
        $_SESSION['cart'][$br]['item1'] = $item1;
        $_SESSION['cart'][$br]['item2'] = $item2;
        $_SESSION['cart'][$br]['item3'] = $item3;
        $_SESSION['cart'][$br]['promomw'] = $prom[0];
        echo 'Успешно ја додадовте промоцијата: "' . $prom[0]['promomw_name'] . '" во Вашата кошница';
    }

    public function insertprom() {
        $item1 = $this->products_model->select_all_joined(["where" => "`product_id` = " . $_POST['product_id1']]);
        $item1 = $item1[0];

        $gallery1 = $this->gallery_model->select_all_joined(["where" => '`gallery_product_id`=' . $_POST['product_id1']]);
        $img1 = $gallery1[0];

        $item1['img'] = site_url('app/img/gallery/' . $img1['gallery_folder'] . '/' . $img1['gallery_file']);
        $item1['title'] = $_POST['web_name1'];

        $item2 = $this->products_model->select_all_joined(["where" => "`product_id` = " . $_POST['product_id2']]);
        $item2 = $item2[0];

        $gallery2 = $this->gallery_model->select_all_joined(["where" => '`gallery_product_id`=' . $_POST['product_id2']]);
        $img2 = $gallery2[0];

        $item2['img'] = site_url('app/img/gallery/' . $img2['gallery_folder'] . '/' . $img2['gallery_file']);
        $item2['title'] = $_POST['web_name2'];

        $prom = $this->promotions_model->select_all_joined(["where" => '`promotion_id`=' . $_POST['prom_id']]);

        if (!empty($_SESSION['cart'])) {
            $br = max(array_keys($_SESSION['cart'])) + 1;
        } else {
            $br = 0;
        }
        $_SESSION['cart'][$br]['item1'] = $item1;
        $_SESSION['cart'][$br]['item2'] = $item2;
        $_SESSION['cart'][$br]['prom'] = $prom[0];
        echo 'Успешно ја додадовте промоцијата: "' . $prom[0]['promotion_name'] . '" во вашата кошничка';
    }

    public function insertvar() {

        if (!empty($_POST['color'])) {
            if (!empty($_POST['product_id'])) {
                if (!empty($_SESSION['cart'])) {
                    $br = max(array_keys($_SESSION['cart'])) + 1;
                } else {
                    $br = 0;
                }
                $item = $this->products_model->select_all_joined(["where" => "`product_id` = " . $_POST['product_id']]);
                $item = $item[0];
                $product = $this->web_products_model->select_all_joined(["where" => "`web_products_id` = " . $_POST['web_products_id']]);
                $product = $product[0];

                $gallery = $this->gallery_model->select_all_joined(["where" => '`gallery_product_id`=' . $_POST['product_id']]);
                if (!empty($gallery)) {
                    $img = $gallery[0];
                    $item['img'] = site_url('app/img/gallery/' . $img['gallery_folder'] . '/' . $img['gallery_file']);
                } else {
                    $item['img'] = site_url("app/" . $product['web_products_img']);
                }
                $item['num'] = $_POST['num'];
                $item['title'] = $_POST['web_name'];
                $item['web_products_id'] = $_POST['web_products_id'];
                $item['dsc'] = 0;
                $_SESSION['cart'][$br] = $item;
                echo 'Успешно го додадовте производот: ' . $_POST['web_name'] . " Во вашата кошничка";

                usort($_SESSION['cart'], function ($a, $b) {
                    return $a['delivery_type'] < $b['delivery_type'];
                });
            } else {
                echo 'Изберете прво Боја па Големина';
            }
        } else {
            echo 'Изберете Боја';
        }
    }

    public function delall() {
        unset($_SESSION['cart']);
    }

    public function delitem() {
        unset($_SESSION['cart'][$_POST['cart_item_id']]);
        echo 'Успешно го избришавте производот од вашата кошничка';
    }

    public function cart() {

        $this->load->view('header');
        $this->load->view('cart/cart');
        $this->load->view('footer');
    }

}
