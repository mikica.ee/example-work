<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Cat extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->library(array('session', 'pagination'));
        $this->load->helper(array('url'));
        $this->load->model(array('user_model', 'categories_model', 'web_products_model', 'cat_model'));
    }

    public function category() {
        if (!empty($_GET['category'])) {
            $category = $_GET['category'];
        } else {
            $category = 'sezavasiotdom';
        }
        if (isset($_GET['orderby'])) {
            switch ($_GET['orderby']) {
                case "nf":
                    $orderby = "ORDER BY web_time desc";
                    break;
                case "of":
                    $orderby = "ORDER BY web_time asc";
                    break;
                case "lp":
                    $orderby = "ORDER BY web_price asc";
                    break;
                case "hp":
                    $orderby = "ORDER BY web_price desc";
                    break;
                default:
                    $orderby = "ORDER BY web_time desc";
                    break;
            }
        } else {
            $orderby = "ORDER BY web_time desc";
        }
        $config = array();
        $config["base_url"] = base_url("listing");
        $config['reuse_query_string'] = true;
        $sql = "SELECT COUNT(`web_products_id`) as count FROM `web_products`\n"
                . "LEFT JOIN `cat_rel` ON cat_rel.cat_rel_p = web_products.web_products_id\n"
                . "LEFT JOIN `categories` ON categories.cat_id = cat_rel.cat_rel_c\n"
                //   . "LEFT JOIN `product_relation` ON product_relation.product_relation_w = web_products.web_products_id\n"
//                . "LEFT JOIN `products` ON product_relation.product_relation_p = products.product_id\n"
                . "WHERE feat = 1 AND `cat_slug` = '" . $category . "'" . "\n"
                . $orderby;
        $count = $this->web_products_model->query($sql);
        $count = $count->result_array();
        $config["total_rows"] = $count[0]['count'];
        $config["per_page"] = 20;
        $config["uri_segment"] = 2;
        $config["first_url"] = $config['base_url'] . '?' . http_build_query($_GET);
        $config['num_tag_open'] = '<li class="page-item page-link">';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item"><a class="page-link current" href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['first_tag_open'] = '<li class="page-item page-link">';
        $config['first_tag_close'] = '</li>';
        $config['first_link'] = '<span class="fa fa-angle-left"></span><span class="fa fa-angle-left"></span>';
        $config['last_link'] = '<span class="fa fa-angle-right"></span><span class="fa fa-angle-right"></span>';
        $config['last_tag_open'] = '<li class="page-item page-link">';
        $config['last_tag_close'] = '</li>';
        $config['prev_link'] = '<span class="fa fa-angle-left"></span>';
        $config['prev_tag_open'] = '<li class="page-item page-link">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '<span class="fa fa-angle-right"></span>';
        $config['next_tag_open'] = '<li class="page-item page-link">';
        $config['next_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $links = $this->pagination->create_links();
        $sql = "SELECT * FROM `web_products`\n"
                . "LEFT JOIN `cat_rel` ON cat_rel.cat_rel_p = web_products.web_products_id\n"
                . "LEFT JOIN `categories` ON categories.cat_id = cat_rel.cat_rel_c\n"
                // . "LEFT JOIN `product_relation` ON product_relation.product_relation_w = web_products.web_products_id\n"
//                . "LEFT JOIN `products` ON product_relation.product_relation_p = products.product_id\n"
                . "WHERE feat = 1 AND `cat_slug` = '" . $category . "'" . "\n"
                . $orderby . "\n"
                . "LIMIT " . $config["per_page"] . " OFFSET " . $page;

        $res = $this->web_products_model->query($sql);
        $product = $res->result_array();
        $info = $this->cat_model->select_all_joined(["where" => '`cat_slug` = ' . '"' . $category . '"']);
        $list = $this->cat_model->select_all_joined(["where" => '`parent_id` = 0 AND `cat_feat` = 1']);
        $subcat = $this->categories_model->sub_categories_sub($info[0]['cat_id']);
        $subcat = $this->objectToArray($subcat);
        $I = 0;
        if ($info[0]['parent_id'] != 0) {
            $above[0]['parent_id'] = $info[0]['parent_id'];
            while (true) {
                if ($above = $this->cat_model->select_all_joined(["where" => '`cat_id` = ' . '"' . $above[0]['parent_id'] . '"'])) {
                    $I++;
                    if ($above[0]['parent_id'] == 0) {
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        $sent_data = ["products" => $product, "links" => $links, "info" => $info[0], "categories" => $list, "subcat" => $subcat, "above" => $I];
        $meta['name'] = $info[0]['cat_name'];
        $meta['keywords'] = $info[0]['cat_keyword'];
        $meta['description'] = $info[0]['cat_description'];
        $meta['img'] = "img/cat/" . $info[0]['cat_img'];
        $meta['url'] = site_url('listing?category=' . $info[0]['cat_slug']);
        $this->load->view('header', ["meta" => $meta]);
        $this->load->view('listing/category', $sent_data);
        $this->load->view('footer');
    }

    private function functionName($param) {
        
    }

    private function objectToArray($data) {
        if (is_array($data) || is_object($data)) {
            $result = array();
            foreach ($data as $key => $value) {
                $result[$key] = $this->objectToArray($value);
            }
            return $result;
        } else {
            return $data;
        }
    }

    private function catlistid($element) {
        $html = "";
        foreach ($element as $key => $value) {
            //$html .= "<li>";
            $html .= $value['cat_id'] . ", ";
            if (!empty($value['sub'])) {

                $html .= $this->catlistid($element[$key]['sub']);
            }
        }
        $html .= "";
        return $html;
    }

    private function catlist($element) {
        $html = "";
        foreach ($element as $key => $value) {
            $html .= "<li>";
            // $html .= '<option value="'.$value['cat_slug'].'">'.$value['cat_name'].'</option>';
            $html .= '<a class="dropdown-item" href="' . site_url('listing?category=' . $value['cat_slug']) . '">' . $value['cat_name'] . '</a>';
            if (!empty($value['sub'])) {
                $html .= "<ul>";
                $html .= $this->catlist($element[$key]['sub']);
                $html .= "</ul>";
            }
            $html .= "</li>";
        }
        return $html;
    }

    private function catoption($element) {
        $html = "";
        foreach ($element as $key => $value) {
            //$html .= "<li>";
            $html .= '<option value="' . $value['cat_slug'] . '">' . $value['cat_name'] . '</option>';
            if (!empty($value['sub'])) {
                //$html .= "-";
                $html .= $this->catlist($element[$key]['sub']);
                //$html .= "</ul>";
            }
            //$html .= "</li>";
        }
        return $html;
    }

}
