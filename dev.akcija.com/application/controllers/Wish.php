<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Wish extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('url'));
        $this->load->model('faq_model');
    }

    public function index() {
        $this->load->view('header');
        $this->load->view('cart/wish');
        $this->load->view('footer');
    }
    public function addproduct() {
//        print_r($_POST);
        $cookie = get_cookie('wish');
        if (!empty($cookie)) {
            $cookie = json_decode($cookie);
            if (!in_array($_POST['web_products_id'], $cookie)) {
                if (count($cookie) > 7) {
                    array_shift($cookie);
                }
                $cookie[] = $_POST['web_products_id'];
                $cookie = json_encode($cookie);
                set_cookie('wish', $cookie, '2147483647');
            }
        } else {
            $cookie[] = $_POST['web_products_id'];
            $cookie = json_encode($cookie);
            set_cookie('wish', $cookie, '2147483647');
        }
    }
     public function delitem() {
        unset($cookie['wish'][$_POST['web_products_id']]);
        echo 'Успешно го избришавте производот од омилени';
    }
    public function delall() {
        unset($cookie['wish']);
    }

}
