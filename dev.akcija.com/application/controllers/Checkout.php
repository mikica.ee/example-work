<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Checkout extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('url'));
        $this->load->model(array('orders_model', 'products_model', 'items_model', 'users_model', 'user_model', 'clients_model', 'grad_model', 'quick_users_model'));
    }


    public function checkout() {
        $data['city'] = $city = $this->takecity("");
        $this->load->view('header');
        $this->load->view('cart/checkout', $data);
        $this->load->view('footer');
    }

    public function insertorder() {
        if (isset($_SESSION['cart'])) {

            $data["transport_p"] = 120;
            $data["transport_t"] = 'П-Г';
            $data["status"] = 8;
            $data["order_price"] = $_POST['price'];
            $data["order_id"] = 0;
            $data["order_web_user_id"] = $_SESSION['user_id'];
            $data['order_create'] = date('Y-m-j H:i:s');
            $user = $this->users_model->select_all_joined(["where" => "`web_user_id` = " . $_SESSION['user_id']]);
            $city = $this->grad_model->select_all_joined(["where" => "`grad_id` = " . $user[0]['web_city']]);
            $client['client_name'] = $user[0]['web_first'] . " " . $user[0]['web_last'];
            $client['client_tel'] = $user[0]['web_tel'];
            $client['client_add'] = $user[0]['web_add'];
            $client['client_city'] = $city[0]['grad_ime'];
            $client_id = $this->insertclient($client);
            $data["order_client_id"] = $client_id;

            if ($this->orders_model->insert($data)) {
                $id = $this->orders_model->db->insert_id();
                $total = 0;
                $mailp = "";
                foreach ($_SESSION['cart'] as $item) {
                    if (!isset($item['prom']) AND!isset($item['promomw'])) {
                        $mailp .= $item['product'] . " X " . $item['num'] . " ";
                        //if (!isset($item['prom'])) {
                        $dataitem['item_status'] = 4;
                        $dataitem['item_id'] = 0;
                        $dataitem['item_order_id'] = $id;
                        $dataitem['item_product_id'] = $item['product_id'];
                        $dataitem['num'] = $item['num'];
                        $dataitem['item_price'] = $item['price'];
                        $total += $item['num'] * $item['price'];
                        $this->items_model->insert($dataitem);
                    } else {

                        if ($item['promomw']['promomw_type'] == 1 OR $item['promomw']['promomw_type'] == 2) {
                            $mailp .= $item['item1']['product'] . " X 1";
                            $mailp .= $item['item2']['product'] . " X 1";
                            $dataitem1['item_status'] = 4;
                            $dataitem1['item_id'] = 0;
                            $dataitem1['item_order_id'] = $id;
                            $dataitem1['item_product_id'] = $item['item1']['product_id'];
                            $dataitem1['num'] = 1;
                            $price1 = ($item['promomw']['promomw_price'] / 2);
                            $total += $price1;
                            $dataitem1['item_price'] = $price1;
                            $this->items_model->insert($dataitem1);

                            $dataitem2['item_status'] = 4;
                            $dataitem2['item_id'] = 0;
                            $dataitem2['item_order_id'] = $id;
                            $dataitem2['item_product_id'] = $item['item2']['product_id'];
                            $dataitem2['num'] = 1;
                            $price2 = ($item['promomw']['promomw_price'] / 2);
                            $total += $price2;
                            $dataitem2['item_price'] = $price2;
                            $this->items_model->insert($dataitem2);
                        }
                        if ($item['promomw']['promomw_type'] == 3 OR $item['promomw']['promomw_type'] == 4) {
                            $mailp .= $item['item1']['product'] . " X 1";
                            $mailp .= $item['item2']['product'] . " X 1";
                            $mailp .= $item['item3']['product'] . " X 1";
                            $dataitem1['item_status'] = 4;
                            $dataitem1['item_id'] = 0;
                            $dataitem1['item_order_id'] = $id;
                            $dataitem1['item_product_id'] = $item['item1']['product_id'];
                            $dataitem1['num'] = 1;
                            $price1 = ($item['promomw']['promomw_price'] / 3);
                            $total += $price1;
                            $dataitem1['item_price'] = $price1;
                            $this->items_model->insert($dataitem1);

                            $dataitem2['item_status'] = 4;
                            $dataitem2['item_id'] = 0;
                            $dataitem2['item_order_id'] = $id;
                            $dataitem2['item_product_id'] = $item['item2']['product_id'];
                            $dataitem2['num'] = 1;
                            $price2 = ($item['promomw']['promomw_price'] / 3);
                            $total += $price2;
                            $dataitem2['item_price'] = $price2;
                            $this->items_model->insert($dataitem2);

                            $dataitem3['item_status'] = 4;
                            $dataitem3['item_id'] = 0;
                            $dataitem3['item_order_id'] = $id;
                            $dataitem3['item_product_id'] = $item['item3']['product_id'];
                            $dataitem3['num'] = 1;
                            $price3 = ($item['promomw']['promomw_price'] / 3);
                            $total += $price3;
                            $dataitem3['item_price'] = $price3;
                            $this->items_model->insert($dataitem3);
                        }
                    }
                }
                $order = $this->orders_model->select_all_joined(["where" => "`order_id` = " . $id]);
                $order = $order[0];
                $order['order_price'] = $total;
                $this->orders_model->update($order);

//vadi od zaliha

                $items = $this->items_model->select_all_joined(["where" => "`item_order_id` = " . $order['order_id']]);
                $wait = 1;
                foreach ($items as $key => $item) {
                    $product = $this->products_model->select_all_joined(["where" => "`product_id` = " . $item['product_id']]);
                    $product = $product['0'];
                    $stock = $product['stock'];
                    $newstock = $product['stock'] - $item['num'];
                    if ($newstock > -1) {
                        $item['item_status'] = 1;
                        // da se odzeme od products stock
                        $product['stock'] = $newstock;
                        $this->products_model->update($product);
                    } else {
                        if ($product['is_negativ']) {
                            $item['item_status'] = 2;
                            $wait = 0;
                            // da se odzeme od products stock
                            $product['stock'] = $newstock;
                            $this->products_model->update($product);
                        } else {
                            $item['item_status'] = 3;
                            $wait = 0;
                        }
                    }
                    $this->items_model->update($item);
                }

                unset($_SESSION['cart']);
                echo $id;
                $datamail = "Почитувани клиенти, Вашата нарачка #" . $id . " е примена " . $mailp . ". ";
                $this->load->library('email');
                $this->email->mailtype = 'html';
                $this->email->from('info@akcija.com.mk', 'Akcija');
                $this->email->to($_SESSION['username']);
                $this->email->cc('info@akcija.com.mk');
                $this->email->subject('akcija.com.mk нарачка');
                $this->email->message($datamail);
                $this->email->send();
            } else {
                echo 'Ne e vneseno';
            }
        }
    }

    private function takecity($izbran) {
        //Zemi site gradovi od baza
        $grad = $this->grad_model->select_all_joined(["order_by" => '`grad_ime` ASC']);
        $listaGrad = "<option></option>";
        for ($x = 0; $x < count($grad); $x++) {
            $gradd = $grad[$x];
            $g = $gradd['grad_ime'];
            $id = $gradd['grad_id'];
            $sel = "";
            if ($izbran == $g) {
                $sel = " selected";
            }
            $listaGrad .= PHP_EOL . "<option{$sel} value='{$id}'>{$g}</option>";
        }
        $listaGrad .= PHP_EOL . "</select>";
        return $listaGrad;
    }

    private function insertclient($data) {
        $client = "`client_name` = '{$data['client_name']}' AND `client_tel` = '{$data['client_tel']}' AND `client_add` = '{$data['client_add']}' AND `client_city` = '{$data['client_city']}'";
        $client = $this->clients_model->select_all_joined(["where" => $client]);
        if ($client) {
            $client = $client['0'];
            return $client["client_id"];
        } else {
            $data["client_id"] = 0;
            $this->clients_model->insert($data);
            return $this->clients_model->db->insert_id();
        }
    }

}
