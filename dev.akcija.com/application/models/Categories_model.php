<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 * 
 * @extends CI_Model
 */
class Categories_model extends CI_Model {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->database();
    }

    public function get_categories() {

        $this->db->select('*');
        $this->db->from('categories');
        $this->db->where('parent_id', 0);
        $this->db->where('cat_feat', 1);
        $parent = $this->db->get();

        $categories = $parent->result();
        $i = 0;
        foreach ($categories as $p_cat) {

            $categories[$i]->sub = $this->sub_categories($p_cat->cat_id);
            $i++;
        }
        
        return $categories;
    }

    public function sub_categories($id) {

        $this->db->select('*');
        $this->db->from('categories');
        $this->db->where('parent_id', $id);
        $this->db->where('cat_feat', 1);
        $child = $this->db->get();
        $categories = $child->result();
        $i = 0;
        foreach ($categories as $p_cat) {

            $categories[$i]->sub = $this->sub_categories($p_cat->cat_id);
            $i++;
        }
        //$categories = json_decode(json_encode($categories));
        return $categories;
    }

    public function sub_categories_sub($id) {
        $this->db->select('*');
        $this->db->from('categories');
        $this->db->where('parent_id', $id);
        $this->db->where('cat_feat', 1);
        $parent = $this->db->get();
        $categories = $parent->result();
        $i = 0;
        foreach ($categories as $p_cat) {
            $categories[$i]->sub = $this->sub_categories($p_cat->cat_id);
            $i++;
        }
        return $categories;
    }
}
