<?php

require_once 'application/models/T_Model.php';

class Gallery_model extends T_model {

//----------------------------------------------------------------------------//
    public function __construct() {
        parent::__construct();
        $this->table_name = 'gallery';
        $this->table_index = 'gallery_id';
        $this->table_index2 = null;
        $this->joined_tables = "";

        $this->def_joined_tables = [
//            'package_details' => [
//                'table_name'=>'package_details',
//                'join_condition' => '`package_details_customer_id`=`customer_id`',
//                'join_type' => 'left',
//            ],
//            'package' => [
//                'table_name'=>'package',
//                'join_condition' => '`customer_package_id`=`package_id`',
//                'join_type' => 'left',
//            ],
//            'record' => [
//                'table_name'=>'record',
//                'join_condition' => '`package_record_id`=`record_id`',
//                'join_type' => 'left',
//            ],
//            'record_text' => [
//                'table_name'=>'record_text',
//                'join_condition' => '`record_text_record_id`=`record_id`',
//                'join_type' => 'left',
//            ]
        ];

        $this->set_to_default_join();
    }

//----------------------------------------------------------------------------//

    function filter_data($data) {
        return [
            "gallery_id" =>        $data["gallery_id"],
            "gallery_product_id" =>    isset($data["gallery_product_id"])      && $data["gallery_product_id"]    ? $data["gallery_product_id"] : null,
            "gallery_folder" =>    isset($data["gallery_folder"])      && $data["gallery_folder"]    ? $data["gallery_folder"] : null,
            "gallery_file" =>    isset($data["gallery_file"])      && $data["gallery_file"]    ? $data["gallery_file"] : null,
            
        ];
    }
}