<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 *
 * @extends CI_Model
 */
class Newproduct_model extends CI_Model {

    public function __construct() {

        parent::__construct();
        $this->load->database();
    }

    public function insert_gallery($data) {
        return $this->db->insert('newgallery', $data);
    }

    public function select_gallery($id) {
        $this->db->select('*');
        $this->db->from('newgallery');
        $this->db->where('gallery_p', $id);
        $this->db->order_by("gallery_date", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function feat($webpid, $gallery_id) {
        $this->db->where('gallery_p', $webpid);
        $this->db->update('newgallery', ['gallery_feat' => 0]);
        $this->db->where('gallery_id', $gallery_id);
        $this->db->update('newgallery', ['gallery_feat' => 1]);
        return 1;
    }

    public function count() {
        return $this->db->count_all("products");
    }

    public function insert($data) {
        return $this->db->insert('web_products', $data);
    }

    public function insert_var_rel($data) {
        return $this->db->insert('product_relation', $data);
    }

    public function insert_simular_rel($data) {
        return $this->db->insert('simular_product', $data);
    }

    public function del_rel_sim($id) {
        return $this->db->delete('simular_product', array('simular_product_id' => $id));
    }

    public function del_var_rel($id) {
        return $this->db->delete('product_relation', array('product_relation_id' => $id));
    }

    public function del_img($id) {
        return $this->db->delete('newgallery', array('gallery_id' => $id));
    }

    public function select($limit, $start) {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->join('gallery', 'products.product_id = gallery.gallery_p');
        $this->db->where('gallery_feat', '1');
        $this->db->order_by("product_time", "desc");
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function select_from_id($id) {
        $this->db->select('*');
        $this->db->from('web_products');
        $this->db->where('web_products_id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function select_from_sim_rel($id) {
        $this->db->select('*');
        $this->db->from('web_products');
        $this->db->join('simular_product', 'web_products.web_products_id = simular_product.simular_product_p');
        $this->db->where('simular_product_p', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function select_from_slug($slug) {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->join('gallery', 'products.product_id = gallery.gallery_p');
        $this->db->where('product_slug', $slug);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function select_for_cat($id) {
        $this->db->select('cat_id, cat_name, cat_rel_id');
        $this->db->from('cat');
        $this->db->join('cat_rel', 'cat_rel.cat_rel_c = cat.cat_id');
        $this->db->join('products', 'products.product_id = cat_rel.cat_rel_p');
        $this->db->where('product_id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function select_for_var($id) {
        $this->db->select('var_id, var_name, var_rel_id');
        $this->db->from('var');
        $this->db->join('var_rel', 'var_rel.var_rel_v = var.var_id');
        $this->db->join('products', 'products.product_id = var_rel.var_rel_p');
        $this->db->where('product_id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function update($data) {
        $where = "web_products_id = " . $data['web_products_id'];
        return $this->db->update('web_products', $data, $where);
    }

    public function update_gallery($data) {
        $where = "gallery_id = " . $data['gallery_id'];
        return $this->db->update('newgallery', $data, $where);
    }

}
