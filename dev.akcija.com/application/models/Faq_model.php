<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Faq_model extends CI_Model {

//----------------------------------------------------------------------------//
//    public function __construct() {
//        parent::__construct();
//        $this->table_name = 'faq';
//        $this->table_index = 'faq_id';
//        $this->table_index2 = null;
//        $this->joined_tables = "";
//----------------------------------------------------------------------------//

    public function __construct() {

        parent::__construct();
        $this->load->database();
    }

    public function insert($data) {
        return $this->db->insert('faq', $data);
    }

    function faq() {
        $this->db->select("faq_id");
        $this->db->from('faq');
        $query = $this->db->get();
        return $query->result();
    }

    public function select() {
        $this->db->select('*');
        $this->db->from('faq');
        $this->db->order_by("faq_date_time", "desc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function select_from_id($id) {
        $this->db->select('*');
        $this->db->from('faq');
        $this->db->where('faq_id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function update($data) {
        $where = "faq_id = " . $data['faq_id'];
        return $this->db->update('faq', $data, $where);
    }

}
