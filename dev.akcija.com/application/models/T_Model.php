<?php
abstract class T_Model extends CI_Model {

    protected $table_name = '';
    protected $table_index = '';
    protected $table_index2 = '';
    protected $joined_tables = '';
    
    protected $def_joined_tables = [];
//----------------------------------------------------------------------------//
    public function __construct($db="default") {
        parent::__construct();
        $this->db = $this->load->database($db, true);
    }
//----------------------------------------------------------------------------//
    abstract function filter_data($data);

//----------------------------------------------------------------------------//    
    public function primary_key()
    {
        return $this->table_index;
    }
//----------------------------------------------------------------------------//    
    public function table_keys()
    {
        $query_str = 'SHOW KEYS IN `'.$this->table_name.'`';
        return $this->query($query_str)->result_array();
    }
//----------------------------------------------------------------------------//        
    public function table_keys_assoc()
    {
        $query_str = 'SHOW KEYS IN `'.$this->table_name.'`';
        $qres = $this->query($query_str)->result_array();
        
        $res = [];
        
        foreach($qres as $q)
        {
            $res[$q["Column_name"]] = $q;
        }
        return $res;
    }    
//----------------------------------------------------------------------------//        
    public function table_columns()
    {
        $query_str = 'SHOW COLUMNS IN `'.$this->table_name.'`';
        return $this->query($query_str)->result_array();
    }
//----------------------------------------------------------------------------//        
    public function table_columns_joined()
    {
        $res = $this->table_columns();
        
        foreach($this->joined_tables as $table)
        {
            $this->load->model($table["table_name"]);
            $res_joined = $this->$table["table_name"]->table_columns();
            $res = array_merge($res, $res_joined);
        }
        return $res;
    }
//----------------------------------------------------------------------------//        
    public function table_columns_assoc()
    {
        $query_str = 'SHOW COLUMNS IN `'.$this->table_name.'`';
        $qres = $this->query($query_str)->result_array();
        
        $res = [];
        
        foreach($qres as $q)
        {
            $res[$q["Field"]] = $q;
        }
        return $res;
    }
//----------------------------------------------------------------------------//        
    public function table_columns_joined_assoc()
    {
        
        $qres = $this->table_columns_joined();
        $res = [];
        
        foreach($qres as $q)
        {
            $res[$q["Field"]] = $q;
        }
        return $res;
    }
//----------------------------------------------------------------------------//    
    public function set_join($tables=[])
    {
        if(is_array($tables) && count($tables))
        {
            $this->joined_tables = $tables;
        }
    }
//----------------------------------------------------------------------------//    
    public function get_join()
    {
        return $this->joined_tables;
    }
//----------------------------------------------------------------------------//    
    public function set_to_default_join()
    {
        $this->joined_tables = $this->def_joined_tables;
    }
//----------------------------------------------------------------------------//    
    public function get_default_join()
    {
        return $this->def_joined_tables;
    }
//----------------------------------------------------------------------------//    
    public function count($options=[]) 
    {
        $query_str = 'SELECT COUNT(`'.$this->table_index.'`) AS count FROM `'.$this->table_name.'`';
        if(count($options))
        {
            if(isset($options["join"]) && is_array($options["join"]))
            {
                foreach($options["join"] as $join)
                {
                    
                    $query_str .= ' '.strtoupper($join["join_type"]).' JOIN `'.$join["table_name"]. '` ON '.$join["join_condition"];
                }

            }
            
            if(isset($options["where"]) && $options["where"])
            {
                $query_str.= ' WHERE '.$options["where"];
            }
            
            if(isset($options["group_by"]) && $options["group_by"])
            {
                $query_str.= ' GROUP BY '.$options["group_by"];
            }
        }

        $res = $this->query($query_str);
            
        if (isset($res->row()->count))
            return ($res->row()->count);
        else
            return 0;
    }
//----------------------------------------------------------------------------//
    public function select($options = array()) {
//        $options["where"]      = array('name !=' => $name, 'id <' => $id, 'date >' => $date);
//        $options["limit"]      = 10;
//        $options["offset"]     = 20;
//        $options["order_by"]   = "place_id desc, place_name asc";

        if (!(isset($options['select']) && $options['select'])) {
            $options['select'] = "*";
        } else {
            $this->db->select($options["select"]);
        }

        if (isset($options['from']) && $options['from']) {
            $this->db->from($options["from"]);
        } else
            $this->db->from($this->table_name);

        if (isset($options['where']) && $options['where']) {
            $this->db->where($options["where"]);
        }
        
        if(isset($options["join"]) && is_array($options["join"]))
        {
            foreach($options["join"] as $join)
            {
                if(isset($join["table_name"]))
                {
                    $this->db->join($join["table_name"], $join["join_condition"], $join["join_type"]);
                }
                else
                {
                    if ($join[2])
                        $this->db->join($join[0], $join[1]);
                    else
                        $this->db->join($join[0], $join[1], $join[2]);
                }
            }
        }
        

        if (isset($options['limit']) && $options['limit']) {
            $this->db->limit($options['limit']);
        }

        if (isset($options['offset']) && $options['offset']) {
            $this->db->offset($options['offset']);
        }

        if (isset($options["order_by"]) && $options['order_by']) {
            $this->db->order_by($options["order_by"]);
        }

        if (isset($options["group_by"]) && $options['group_by']) {
            $this->db->group_by($options["group_by"]);
        }
        
        $q = $this->db->get();
//        tmp_print($this->db->last_query());
        return $q;
    }
//----------------------------------------------------------------------------//
    public function first($options=[])
    {
        $options["where"] = '`'.$this->table_index.'` = (SELECT MIN(`'.$this->table_index.'`) FROM `'.$this->table_name.'`)';
        $q = $this->select($options)->result_array();
        return reset($q);
    }
//----------------------------------------------------------------------------//
    public function last($options=[])
    {
        $options["where"] = '`'.$this->table_index.'` = (SELECT MAX(`'.$this->table_index.'`) FROM `'.$this->table_name.'`)';
        $q = $this->select($options)->result_array();
        return reset($q);
    }    
//----------------------------------------------------------------------------//
    public function insert($data) {
        $data_filtered = $this->filter_data($data);
        return $this->db->insert($this->table_name, $data_filtered);
    }
//----------------------------------------------------------------------------//
    public function update($data) 
    {
        $data_filtered = $this->filter_data($data);
        
        if(isset($data_filtered[$this->table_index]))
            $rule = array($this->table_index => $data_filtered[$this->table_index]);
        else
            $rule = array($this->table_index2 => $data_filtered[$this->table_index2]);
        
        return $this->db->update($this->table_name, $data_filtered, $rule);
    }
//----------------------------------------------------------------------------//
    public function delete($data) 
    {
        if($data == "*" || $data == "all" || $data == "ALL")
        {
            return $this->db->empty_table($this->table_name);
        }
        
        if(isset($data[$this->table_index]))
            $rule = array($this->table_index => $data[$this->table_index]);
        else
            $rule = array($this->table_index2 => $data[$this->table_index2]);
        return $this->db->delete($this->table_name, $rule);
    }
//----------------------------------------------------------------------------//
    public function delete_where($where_data) 
    {
        return $this->db->delete($this->table_name, $where_data);
    }
//----------------------------------------------------------------------------//
    public function exists($options)
    {
        $options["limit"] = 1;
        $qr = $this->select_all_joined($options);
        return ($qr ? 1 : 0);
    }
//----------------------------------------------------------------------------//
    public function query($query_str) {
        return $this->db->query($query_str);
    }

//----------------------------------------------------------------------------//
    public function first_joined($options=[])
    {
        $options["where"] = '`'.$this->table_name.'`.`'.$this->table_index.'` = (SELECT MIN(`'.$this->table_index.'`) FROM `'.$this->table_name.'`)';
        return $this->select_one_joined($options);
    }
//----------------------------------------------------------------------------//
    public function last_joined($options=[])
    {
        $options["where"] = '`'.$this->table_name.'`.`'.$this->table_index.'` = (SELECT MAX(`'.$this->table_index.'`) FROM `'.$this->table_name.'`)';
        return $this->select_one_joined($options);
    }
//----------------------------------------------------------------------------//
    public function select_all_joined($options=[]) 
    {
        if($this->joined_tables && is_array($this->joined_tables))
        {
            $options["join"] = $this->joined_tables;
        }
        $qr = $this->select($options)->result_array();
       
        if (count($qr))
        {
            return ($qr);
        }
        else
            return null;
    }
//----------------------------------------------------------------------------//
    public function select_one_joined($options=[]) 
    {
        if($this->joined_tables && is_array($this->joined_tables))
        {
            $options["join"] = $this->joined_tables;
        }
        $qr = $this->select($options)->result_array();
       
        if (count($qr))
            return reset($qr);
        else
            return null;
    }
}

