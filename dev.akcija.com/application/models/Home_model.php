<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model {

//----------------------------------------------------------------------------//
//    public function __construct() {
//        parent::__construct();
//        $this->table_name = 'faq';
//        $this->table_index = 'faq_id';
//        $this->table_index2 = null;
//        $this->joined_tables = "";

//----------------------------------------------------------------------------//

       public function __construct() {

        parent::__construct();
        $this->load->database();
    }

    public function insert($data) {
        return $this->db->insert('home', $data);
    }
    

    public function select() {
        $this->db->select('*');
        $this->db->from('home');
        $this->db->order_by("home_id", "desc");
        $query = $this->db->get();
        return $query->result_array();
    }
    public function select_type($id, $limit) {
        $this->db->select('*');
        $this->db->from('web_products');
        $this->db->join('home', 'web_products.web_products_id = feat.home_title');
        $this->db->where('home_type', $id);
        $this->db->order_by("home_id", "desc");
        $this->db->limit($limit, 0);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function select_from_id($id) {
        $this->db->select('*');
        $this->db->from('home');
        $this->db->where('home_id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function update($data) {
        $where = "home_id = " . $data['home_id'];
        return $this->db->update('home', $data, $where);
    }
    
}