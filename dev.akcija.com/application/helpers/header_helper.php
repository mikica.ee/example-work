<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function feat($id, $limit) {
    $CI = & get_instance();
    $CI->load->model('feat_model');
    return $CI->feat_model->select_type($id, $limit);
}

function home_baner($id) {
    $CI = & get_instance();
    $CI->load->model('home_model');
    return $CI->home_model->select_from_id($id);
}

function home_cat() {
    $CI = & get_instance();
    $CI->load->model('homecat_model');
    return $CI->homecat_model->select_from_parent(0);
}

function home_cat_feat($id) {
    $CI = & get_instance();
    $CI->load->model('newproduct_model');
    return $CI->newproduct_model->select_gallery($id);
}

function home_cat_list($id, $limit, $start, $feat) {
    $CI = & get_instance();
    $CI->load->model('homecat_model');
    return $CI->homecat_model->select_for_list($id, $limit, $start, $feat);
}

function cookie_product($cookie_name) {
    $CI = & get_instance();
    $CI->load->model('newproduct_model');
    $cookie = get_cookie($cookie_name);
    $cookie = json_decode($cookie);
    if (!empty($cookie)) {
        foreach ($cookie as $value) {
            if (!empty($value)) {
                $product = $CI->newproduct_model->select_from_id($value);
                $products[] = $product[0];
            }
        }
    } else {
        $products = null;
    }
    return $products;
}

function calc_delivery() {
    if (isset($_SESSION['cart'])) {
        $price[0] = 0;
        $price[1] = 99;
        $price[2] = 99;
        $price[3] = 199;
        $price[4] = 299;
        $price[5] = 399;
        $price[6] = 599;
        $price[7] = 799;
        usort($_SESSION['cart'], function ($a, $b) {
            return $a['delivery_type'] < $b['delivery_type'];
        });
        foreach ($_SESSION['cart'] as $x => $item) {
            if ($x == 0) {
                $total = $price[$item['delivery_type']];
            } else {
                if ($item['delivery_type'] != 1) {
                    $total += ($price[$item['delivery_type']] / 2);
                }
            }
        }
        return $total;
    } else {
        return 0;
    }
}

function circle_menu() {
    $CI = & get_instance();
    $CI->load->model('prom_cat_model');
    $cat = $CI->prom_cat_model->select_all_joined(["where" => "`prom_cat_act` = 1", "order_by" => "prom_cat_date asc"]);
    return $cat;
}

function get_cat($parent_id, $limit, $offset = null) {
    $CI = & get_instance();
    $CI->load->model('cat_model');
    if ($offset) {
        return $CI->cat_model->select_all_joined(["where" => '`parent_id` = ' . $parent_id . ' AND cat_feat = 1', "limit" => $limit, "offset" => $offset]);
    } else {
        return $CI->cat_model->select_all_joined(["where" => '`parent_id` = ' . $parent_id . ' AND cat_feat = 1', "limit" => $limit]);
    }
}

function get_cat_menu() {
    $CI = & get_instance();
    $CI->load->model('categories_model');
    return objectToArray($CI->categories_model->get_categories());
}

function menu() {
    $CI = & get_instance();
    $CI->load->model('menu_model');

    $data = $CI->menu_model->get_menu();
    $menu = objectToArray($data);
    //return menulist($menu);
    return $menu;
}

function menum() {
    $CI = & get_instance();
    $CI->load->model('menu_model');

    $data = $CI->menu_model->get_menu();
    $menu = objectToArray($data);
    return menulistm($menu);
}

function objectToArray($data) {
    if (is_array($data) || is_object($data)) {
        $result = array();
        foreach ($data as $key => $value) {
            $result[$key] = objectToArray($value);
        }
        return $result;
    } else {
        return $data;
    }
}

function catlistid($element) {
    $html = "";
    foreach ($element as $key => $value) {
        $html .= $value['cat_id'] . ", ";
        if (!empty($value['sub'])) {

            $html .= catlistid($element[$key]['sub']);
        }
    }

    return $html;
}

function menulist($element) {
    $html = "";
    foreach ($element as $key => $value) {
        $html .= "<li class='dropdown dropdown-mega-menu dropdown-two-col'>";
        $html .= '<a href="' . $value['menu_link'] . '" class="dropdown-toggle" data-toggle="dropdown"><span class="act-underline">' . $value['menu_name'] . '</span></a>';
        if (!empty($value['sub'])) {
            $html .= "<ul class='dropdown-menu multicolumn two-col' role='menu'>";
            $html .= menulist($element[$key]['sub']);
            $html .= "</ul>";
        }
        $html .= "</li>";
    }
    return $html;
}

function menulistm($element) {
    $html = "";
    foreach ($element as $key => $value) {
        if (!empty($value['sub'])) {
            $html .= '<li class="u-has-submenu u-header-collapse__submenu">';
            $html .= '<a class="u-header-collapse__nav-link u-header-collapse__nav-pointer" href="javascript:;" data-target="#headerSidebarCollapse' . $value['menu_id'] . '" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="headerSidebarCollapse' . $value['menu_id'] . '">' . $value['menu_name'] . '</a>';
        } else {
            $html .= '<li>';
            $html .= '<a class="u-header-collapse__nav-link font-weight-bold" ' . $value['menu_link'] . '">' . $value['menu_name'] . '</a>';
        }
        if (!empty($value['sub'])) {
            $html .= '<div id="headerSidebarCollapse' . $value['menu_id'] . '" class="collapse" data-parent="#headerSidebarContent">';
            $html .= "<ul class='u-header-collapse__nav-list'>";
            $html .= menulistm($element[$key]['sub']);
            $html .= "</ul>";
            $html .= "</div>";
        }
        $html .= "</li>";
    }
    return $html;
}
