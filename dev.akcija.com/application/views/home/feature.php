<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//echo '<pre>';
//print_r($feature);
//echo '</pre><br>';
?>
<div id="pageContent">
    <div class="content">
        <div class="container">
            <!-- title -->
            <div class="title-with-button">
                <div class="carousel-products__button pull-right"> <span class="btn-prev"></span> <span class="btn-next"></span> </div>
                <h2 class="text-center text-uppercase title-under">Избрани Производи</h2>
            </div>
            <!-- /title --> 
            <!-- carousel -->
            <div class="carousel-products row" id="carouselFeatured">
                <?php foreach ($feature as $product) { ?>


                    <div class="col-lg-3">
                        <!-- product -->
                        <div class="product">
                            <div class="product__inside">
                                <!-- product image -->
                                <div class="product__inside__image">
                                    <a href="<?= site_url("product/" . $product['web_products_slug']) ?>"> <img src="<?= site_url("app/" . $product['web_products_img']) ?>" alt=""> </a> 
                                    <!-- quick-view --> 
                                    <a href="<?= site_url("product/" . $product['web_products_slug']) ?>" class="quick-view"><b><span class="icon icon-visibility"></span> погледни</b> </a>  
                                    <!-- /quick-view --> 
                                </div>
                                <!-- /product image --> 											
                                <!-- product name -->
                                <div class="product__inside__name">
                                    <h2><a href="<?= site_url("product/" . $product['web_products_slug']) ?>"><?= $product['web_products_name'] ?></a></h2>
                                </div>
                                <!-- /product name --> 
                                <!-- product price -->
                                <div class="product__inside__price price-box"><?= $product['web_price'] ?> ден.</div>
                                <!-- /product price --> 
                                <div class="product__inside__hover">
                                    <!-- product info -->
                                    <div class="product__inside__info">
                                        <div class="product__inside__info__btns">
                                            
                                            <a href="<?= site_url("product/" . $product['web_products_slug']) ?>" class="btn btn--ys btn--xl visible-xs"><span class="icon icon-favorite_border"></span></a>
                                            
                                            <a href="<?= site_url("product/" . $product['web_products_slug']) ?>" class="btn btn--ys btn--xl  row-mode-visible hidden-xs"><span class="icon icon-visibility"></span> погледни</a> </div>
                                        <ul class="product__inside__info__link hidden-xs">
                                            <li class="text-right"><span class="icon icon-favorite_border  tooltip-link"></span><a href="#"><span class="text">Add to wishlist</span></a></li>
                                        </ul>
                                    </div>
                                    <!-- /product info --> 
                                </div>
                            </div>
                        </div>
                        <!-- /product --> 
                    </div>

                <?php } ?>
            </div>


        </div>
    </div>
</div>