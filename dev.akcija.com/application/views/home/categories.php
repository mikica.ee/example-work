<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//echo '<pre>';
//print_r($homecats);
//echo '</pre><br>';
?>
<div id="pageContent">
    <!--=== box-baners ===-->
    <div class="container-fluid box-baners">
        <div class="row">

            <?php
            $br = 0;
            foreach ($homecats as $homecat) {
                if ($homecat['homecat_seg']) {
                    $link = $homecat['homecat_link'];
                } else {
                    $link = $homecat['homecat_link'] . $homecat['homecat_id'];
                }
                ?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <!-- banner -->
                    <a href="<?= site_url($link) ?>" class="banner zoom-in">
                        <span class="figure">
                            <img src="<?= site_url('app/img/cat/' . $homecat['homecat_img']) ?>" alt=""/>
                            <span class="figcaption text-left">
                                <span class="block-table">
                                    <span class="block-table-cell">
                                        <span class="block font-size82 text-uppercase font-bold"><span class="color"><?= $homecat['homecat_name'] ?></span></span>
                                        <span class="block text-uppercase font-size26"><?= $homecat['homecat_desc'] ?></span>
                                        <em class="link-btn-20 main-font">Погледни <span class="icon icon-navigate_next"></span></em>
                                    </span>
                                </span>
                            </span>
                        </span>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
    <!--=== /box-baners ===-->
</div>