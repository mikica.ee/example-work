<div id="pageContent">
    <div class="container">				
        <!-- title -->
        <div class="title-box">
            <h1 class="text-center text-uppercase title-under">НАЈАВЕТЕ СЕ ИЛИ КРЕИРАЈТЕ НОВ ПРОФИЛ</h1>

            <?php if (isset($_GET['reg'])) : ?>
                <div class="col-md-12">
                    <h1 class="text-center color">Успешно го креиравте вашиот профил!</h1><br>
                    <h1 class="text-center color">Најавете се во формата за најава за да ја комплетирате нарачката!</h1>
                    <h1 class="text-center color small">Доколку не сте сигурни кој е вашето Корисничко Име и лозинка, проверете го машиот маил</h1>
                </div>
            <?php endif; ?>
        </div>
        <!-- /title -->		
        <div class="row">
            <div class="divider divider--md visible-sm visible-xs"></div>
            <section class="col-sm-12 col-md-6 col-lg-6 col-xl-4 col-xl-offset-2">
                <div class="login-form-box">
                    <h3 class="color small">НАЈАВА</h3>
                    <p>
                        Доколку веќе имате креирано профил, ве молиме најавете се!
                    </p>
                    <?php if (validation_errors()) : ?>
                        <h3 class="color small"><?= validation_errors() ?></h3>
                    <?php endif; ?>
                    <?php if (isset($error)) : ?>
                        <h3 class="color small"><?= $error ?></h3>
                    <?php endif; ?>
                    <?= form_open() ?>
                    <div class="form-group">
                        <label for="email">Email <sup>*</sup></label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Внесете го вашиот email">
                        <p class="help-block">Валидна email адреса</p>
                    </div>
                    <div class="form-group">
                        <label for="password">Лозинка <sup>*</sup></label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Вашата лозинка">
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <button type="submit" class="btn btn--ys btn-top btn--xl"><span class="icon icon-vpn_key"></span>Најави се</button>			               			
                        </div>
                        <div class="divider divider--md visible-xs"></div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="pull-right note btn-top">* Задолжителни полиња</div>
                        </div>
                    </div>			               			                
                    <p class="btn-top">
                        <a class="link-color" href="<?= base_url('user/lostpassword') ?>">Ја заборавивте лозинката?</a>
                    </p>
                    </form>
                </div>
            </section>
            <section class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
                <div class="login-form-box">
                    <h3 class="color small">НОВ КОРИСНИК</h3>
                    <p>Со креирање на кориснички Профил ви се овозможува побрза нарачка на нашите производи, преглед на историјата на вашите нарачки, попусти, промотивни цени и многу други изненадувања</p>
                    <br>
                    <button class="btn btn--ys btn--xl" onclick="location.href = '<?= base_url('register?checkout=1') ?>';"><span class="icon icon-person_add"></span>КРЕИРАЈ ПРОФИЛ</button>
<!--                    <br><hr>
                    <h3 class="color small">БРЗА НАРАЧКА</h3>
                    <p>Со избор на брза нарачка потребно е да внесете само лице за контакт и телефонски број по што нашите оператори ке ве контактираат за да ја потврдите нарачката</p>
                    <br>
                    <button class="btn btn--ys btn--xl" onclick="location.href = '<?= base_url('user/quick') ?>';"><span class="icon icon-person_add"></span>БРЗА НАРАЧКА</button>-->
                </div>
            </section>
        </div>						
    </div>
</div>