<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
    $(document).ready(function () {
        $("button#reset").click(function () {
            var password = $('#password').val();
            var url = "<?php echo site_url('user/changepassword') ?>";
            var obj = {
                password: password
            }
            $.post(url, obj, function (data, textStatus, jqXHR) {
                //alert(data);

                if (data == 1) {
                    swal("Вашата лозинка е успешно променета. Проверете ја вашата email адреса за понатамошни инструкции", {
                        icon: "success",
                        buttons: {
                            home: "Продолжи со купување",
                            login: "Најава",
                        },
                    }).then((value) => {
                        switch (value) {
                            case "home":
                                location.href = '<?= base_url() ?>';
                                break;
                            case "login":
                                location.href = '<?= base_url("login") ?>';
                                break;
                            default:
                                location.href = '<?= base_url("login") ?>';
                        }
                    });

                } else {
                    swal("Со Вашата email адреса: " + email + " нема креирано профил", {
                        icon: "error",
                        buttons: {
                            home: "Обиди се повторно",
                            login: "Најава",
                        },
                    }).then((value) => {
                        switch (value) {
                            case "home":
                                location.reload();
                                break;
                            case "login":
                                location.href = '<?= base_url("login") ?>';
                                break;
                            default:
                                location.reload();
                        }
                    });
                }
            });
        });
    });
</script>
<div class="container">
    <div class="row">
        <div class="divider divider--md visible-sm visible-xs"></div>
        <section class="col-sm-12 col-md-6 col-lg-6 col-xl-6 col-xl-offset-3">
            <div class="login-form-box">
                <h3 class="color small">Промена на лозинка</h3>
                <p>
                    Внесете ја вашата ја вашата нова лозинка и кликнете на копчето "Промени ја лозинка"
                </p>
                <form id="Form" action="javascript:;">
                    <div class="form-group">
                        <label for="password">Лозинка <sup>*</sup></label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Внесете ја лозинката">
                        <p class="help-block">Најмалку 6 карактери</p>
                    </div>
                    <div class="form-group">
                        <label for="password_confirm">Потврди ја лозинката <sup>*</sup></label>
                        <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Потврдете ја вашата лозинка">
                        <p class="help-block">Лозинките мора да се еднакви</p>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <button id="reset" type="submit" class="btn btn--ys btn-top btn--xl"><span class="icon icon-vpn_key"></span>Промени ја лозинка</button>			               			
                        </div>
                        <div class="divider divider--md visible-xs"></div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="pull-right note btn-top">* Задолжителни полиња</div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>