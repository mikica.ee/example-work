<?php
//echo '<pre>';
//print_r($orders);
//echo '</pre>';

?>
<div id="pageContent">
    <div class="container">
        <!-- title -->
        <div class="title-box">
            <h1 class="text-center text-uppercase title-under">Нарачка #<?= $orders[0]['order_id'] ?></h1>
        </div>
        <!-- /title -->
        <div class="row">

            <section class="col-md-8 col-lg-8">
                <!-- Shopping cart table -->
                <div class="container-widget">
                    <table class="shopping-cart-table">
                        <thead>
                            <tr>
                                <th>Производ</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>Цена</th>
                                <th>Количина</th>
                                <th>Вкупно</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total = 0; ?>
                            <?php $delay = 0; ?>
                                <?php foreach ($items as $x => $item) { ?>
                                    <?php if (!isset($item['prom'])) { ?>
                                        <tr>
                                            <td>
                                                <div class="shopping-cart-table__product-image">
                                                    <a href="#">
                                                        <img class="img-responsive" src="<?= base_url("app/img/gallery/".$item['img']) ?>" alt="">
                                                    </a>
                                                </div>
                                            </td>
                                            <td>
                                                <h5 class="shopping-cart-table__product-name text-left text-uppercase">
                                                    <a href="#"><?= $item['product']; ?></a>
                                                </h5>
                                                <ul class="shopping-cart-table__list-parameters">
                                                    <li>
                                                        <span>Код:</span> <?= $item['product']; ?>
                                                    </li>
                                                    <li>
                                                        <span>Боја:</span> <img src="<?= base_url("app/" . $item['color_link']) ?>" alt="" height="40" width="40"/>
                                                    </li>
                                                    <li>
                                                        <span>Големина:</span> <?= $item['size']; ?>
                                                    </li>

                                                    <li class="visible-xs">
                                                        <span>Цена:</span>
                                                        <span class="price-mobile"><?= $item['price']; ?></span>
                                                    </li>
                                                    <li class="visible-xs">
                                                        <span>Количина:</span>
                                                        <!--  -->
                                                        <div class="number input-counter">
                                                            <span class="minus-btn"></span>
                                                            <input type="text" value="<?= $item['num']; ?>" size="5"/>
                                                            <span class="plus-btn"></span>
                                                        </div>
                                                        <!-- / -->
                                                    </li>
                                                </ul>																				
                                            </td>
                                            <td>

                                                <a class="shopping-cart-table__delete icon icon-delete visible-xs" href="javascript:;" id="delete-cart-item" cart_item_id = <?= $x ?>></a>
                                            </td>
                                            <td>
                                                <div class="shopping-cart-table__product-price unit-price">
                                                    <?= $item['price']; ?>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="shopping-cart-table__input">
                                                    <!--  -->
                                                    <div class="number input-counter">
                                                        <span class="minus-btn"></span>
                                                        <input type="text" value="<?= $item['num']; ?>" size="5"/>
                                                        <span class="plus-btn"></span>
                                                    </div>
                                                    <!-- / -->
                                                </div>								
                                            </td>
                                            <td>
                                                <div class="shopping-cart-table__product-price subtotal">
                                                    <?php $productp = $item['num'] * $item['price']; ?>
                                                    <?= $productp; ?>
                                                </div>
                                            </td>
                                            <td>
                                                <a class="shopping-cart-table__delete icon icon-clear" href="javascript:;" id="delete-cart-item" cart_item_id = <?= $x ?>></a>
                                            </td>
                                        </tr>
                                        <?php
                                        $total += $productp;
                                        if ($delay < $item['delay']) {
                                            $delay = $item['delay'];
                                        }
                                    } else {
                                        ?>
                                    <thead>
                                        <tr>
                                            <th>Промоција</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>Цена</th>
                                            <th>&nbsp;</th>
                                            <th>Вкупно</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tr>
                                        <td>
                                            <div class="shopping-cart-table__product-image">
                                                <a href="#">
                                                    <img class="img-responsive" src="<?= $item['item1']['img']; ?>" alt="">
                                                </a>
                                            </div>
                                        </td>
                                        <td>
                                            <h5 class="shopping-cart-table__product-name text-left text-uppercase">
                                                <a href="#"><?= $item['item1']['title']; ?></a>
                                            </h5>
                                            <ul class="shopping-cart-table__list-parameters">
                                                <li>
                                                    <span>Код:</span> <?= $item['item1']['product']; ?>
                                                </li>
                                                <li>
                                                    <span>Боја:</span> <img src="<?= base_url("app/" . $item['item1']['color_link']) ?>" alt="" height="40" width="40"/>
                                                </li>
                                                <li>
                                                    <span>Големина:</span> <?= $item['item1']['size']; ?>
                                                </li>

                                                <li class="visible-xs">
                                                    <span>Цена:</span>
                                                    <span class="price-mobile"><?= $item['item1']['price'] ?>-<?= $item['prom']['promotion_dsc'] / 2 ?></span>
                                                </li>
                                                <li class="visible-xs">
                                                    <span>Вкупно:</span>
                                                    <span class="price-mobile"><?= $item['item1']['price'] - ($item['prom']['promotion_dsc'] / 2) ?></span>
                                                </li>
                                            </ul>																				
                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                            <div class="shopping-cart-table__product-price unit-price">
                                                <?= $item['item1']['price']; ?>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="shopping-cart-table__product-price unit-price">
                                                Попуст:
                                            </div>								
                                        </td>
                                        <td>
                                            <div class="shopping-cart-table__product-price subtotal">
                                                <?php $productp1 = $item['item1']['price']; ?>
                                                <?= $item['item2']['price'] + $item['item1']['price'] ?><br>
                                                -<?= $item['prom']['promotion_dsc']; ?>
                                            </div>
                                        </td>
                                        <td>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="shopping-cart-table__product-image">
                                                <a href="#">
                                                    <img class="img-responsive" src="<?= $item['item2']['img']; ?>" alt="">
                                                </a>
                                            </div>
                                        </td>
                                        <td>
                                            <h5 class="shopping-cart-table__product-name text-left text-uppercase">
                                                <a href="#"><?= $item['item2']['title']; ?></a>
                                            </h5>
                                            <ul class="shopping-cart-table__list-parameters">
                                                <li>
                                                    <span>Код:</span> <?= $item['item2']['product']; ?>
                                                </li>
                                                <li>
                                                    <span>Боја:</span> <img src="<?= base_url("app/" . $item['item2']['color_link']) ?>" alt="" height="40" width="40"/>
                                                </li>
                                                <li>
                                                    <span>Големина:</span> <?= $item['item2']['size']; ?>
                                                </li>

                                                <li class="visible-xs">
                                                    <span>Цена:</span>
                                                    <span class="price-mobile"><?= $item['item2']['price'] ?>-<?= $item['prom']['promotion_dsc'] / 2 ?></span>
                                                </li>
                                                <li class="visible-xs">
                                                    <span>Вкупно:</span>
                                                    <span class="price-mobile"><?= $item['item2']['price'] - ($item['prom']['promotion_dsc'] / 2) ?></span>
                                                </li>
                                            </ul>																				
                                        </td>
                                        <td>

                                            <a class="shopping-cart-table__delete icon icon-delete visible-xs" href="javascript:;" id="delete-cart-item" cart_item_id = <?= $x ?>></a>
                                        </td>
                                        <td>
                                            <div class="shopping-cart-table__product-price unit-price">
                                                <?= $item['item2']['price']; ?>
                                            </div>
                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                            <div class="shopping-cart-table__product-price subtotal">
                                                <?php $productp2 = $item['item2']['price']; ?>
                                                <?= $productp1 + $productp2 - $item['prom']['promotion_dsc'] ?>
                                            </div>
                                        </td>
                                        <td>
                                            <a class="shopping-cart-table__delete icon icon-clear" href="javascript:;" id="delete-cart-item" cart_item_id = <?= $x ?>></a>
                                        </td>
                                    </tr>
                                    <?php
                                    $total += $productp1;
                                    $total += $productp2;
                                    $total -= $item['prom']['promotion_dsc'];
                                    if ($delay < $item['item1']['delay']) {
                                        $delay = $item['item1']['delay'];
                                    }
                                    if ($delay < $item['item2']['delay']) {
                                        $delay = $item['item2']['delay'];
                                    }
                                }
                            }
                        
                        ?>


                        </tbody>
                    </table>
                </div>
                <!-- /Shopping cart table -->						
                <!-- button -->
                <div class="divider divider--xs"></div>
                <div class="row shopping-cart-btns">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <a class="btn btn--ys btn--light pull-left btn-right" href="<?= base_url() ?>"><span class="icon icon-keyboard_arrow_left"></span>ПРОДОЛЖИ СО КУПУВАЊЕ</a>         
                        <div class="divider divider--xs visible-xs"></div>
<!--                        <a class="btn btn--ys btn--light" href="javascript:;" id="deleteall"><span class="icon icon-delete"></span>ИЗПРАЗНИ ЈА КОШНИЧКАТА</a>-->
                    </div>		           

                </div>	
                <!-- /button -->
                <div class="divider visible-sm visible-xs"></div>
            </section>



            <aside class="col-md-4 col-lg-4 shopping_cart-aside">
                <div class="card card--padding fill-bg">
                    <table class="table-total">
                        <tbody>
                            <tr>
                                <th class="text-left">Вкупно:</th>
                                <td class="text-right"><?php echo $total; ?> ден.</td>
                            </tr>
                            <tr>
                                <th class="text-left">Достава:</th>
                                <td class="text-right"><?= $orders[0]['transport_p'] ?> ден.</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ЗА НАПЛАТА:</th>
                                <td><?= $total + $orders[0]['transport_p'] ?><br>ден.</td>
                            </tr>
                        </tfoot>
                    </table>
                    <p>Ова се деталите за вашата нарачка #<?= $orders[0]['order_id'] ?>, доколку сакате да направите измена на нарачаните производи ве молиме обратете се во нашиот контакт центар!</p>
                    <p>Очекувајте достава за <?= $delay ?> дена од денот на нарачката</p>
                    <a href="<?= base_url() ?>" class="btn btn--ys btn--full btn--xl">ПРОДОЛЖИ СО КУПУВАЊЕ<span class="icon icon-reply icon--flippedX"></span></a>
                </div>
            </aside>


        </div>
    </div>
</div>