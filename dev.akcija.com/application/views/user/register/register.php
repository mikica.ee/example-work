<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>



<div id="pageContent">
    <div class="container">				
        <!-- title -->
        <div class="title-box">
            <h1 class="text-center text-uppercase title-under">Креирање на профил</h1>
        </div>
        <!-- /title -->
        <div class="row">
            <section class="col-md-8 col-lg-8">
                <!-- checkout-step -->
                <div class="panel-group" id="checkout">
                    <div class="panel panel-checkout" role="tablist">
                        <!-- panel heading -->
                        <div class="panel-heading active" role="tab">
                            <h4 class="panel-title"> <a role="button" data-toggle="collapse" href="#collapseOne">ИМЕ И АДРЕСА</a> </h4>
                            <div class="panel-heading__number">1</div>
                        </div>
                        <!-- /panel heading -->
                        <!-- panel body -->
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body">

                                <?php if (validation_errors()) : ?>
                                    <div class="col-md-12">
                                        <div class="alert alert-danger" role="alert">
                                            <?= validation_errors() ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if (isset($error)) : ?>
                                    <div class="col-md-12">
                                        <div class="alert alert-danger" role="alert">
                                            <?= $error ?>
                                        </div>
                                    </div>
                                <?php endif; ?>


                                <?= form_open() ?>


                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="web_first">Име <sup>*</sup></label>
                                            <input type="text" class="form-control" id="web_first" name="web_first" placeholder="Вашето Име">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="web_last">Презиме <sup>*</sup></label>
                                            <input type="text" class="form-control" id="web_last" name="web_last" placeholder="Вашето Презиме">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="email">Email <sup>*</sup></label>
                                            <input type="email" class="form-control" id="email" name="email" placeholder="Внесете го вашиот email">
                                            <p class="help-block">Валидна email адреса</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="web_tel">Телефон <sup>*</sup></label>
                                            <input type="text" class="form-control" id="web_tel" name="web_tel">
                                            <p class="help-block">Мобилен телефон</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="web_city">Град <sup>*</sup></label>
                                            <select class='form-control' id="web_city" name='web_city'>
                                                <?php echo $city; ?>
                                            </select>
                                            <p class="help-block">Најмалку 6 карактери</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="web_add">Адреса <sup>*</sup></label>
                                            <input type="text" class="form-control" id="web_add" name="web_add" placeholder="Вашата Адреса">
                                            <p class="help-block">Адресата на која сакате да ви пристигнуваат пратките</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="password">Лозинка <sup>*</sup></label>
                                            <input type="password" class="form-control" id="password" name="password" placeholder="Внесете ја лозинката">
                                            <p class="help-block">Најмалку 6 карактери</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="password_confirm">Потврди ја лозинката <sup>*</sup></label>
                                            <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Потврдете ја вашата лозинка">
                                            <p class="help-block">Лозинките мора да се еднакви</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /panel body -->
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn--ys btn--xl" type="submit"><span class="icon icon-person_add"></span>Регистрирај се</button>
                </div>
                </form>
                <!-- /checkout-step -->
            </section>
            <aside class="col-md-4 col-lg-4 shopping_cart-aside">
                <!--  -->
                <div class="box-border box-border--padding fill-bg">							
                    <h4 class="color small">ДОБРОДОЈДОВТЕ ВО СОВРШЕНОТО СЕМЕЈСТВО</h4>

                    <h6 class="small">ИМЕ И АДРЕСА</h6>
                    <p>Овие информации се задолжителни и мора да бидат точни!</p>
                    <p>По креирањето на профилот и првата нарачка ќе бидете контактирани од нашите оператори.</p>
                </div>
            </aside>
        </div>
    </div>
</div>