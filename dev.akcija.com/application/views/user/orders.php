<div id="pageContent">
    <div class="container">				
        <div class="title-box">
            <h1 class="text-center text-uppercase title-under">Историја на Нарачки</h1>
        </div>
        <?php if (isset($orders)) { ?>
            <h4 class="text-uppercase">Детали</h4>
            <table class="table-order-history">
                <thead>
                    <tr>
                        <th>Нарачка</th>
                        <th>Дата</th>
                        <th>Статус</th>
                        <th>Цена</th>
                        <th>Достава</th>
                        <th>Вкупно</th>
                    </tr>
                </thead>
                <?php foreach ($orders as $order) { ?>          
                    <tbody>
                        <tr>
                            <td><div class="th-title visible-xs">Нарачка</div><a href="<?= base_url('orders/details/'.$order['order_id']) ?>">#<?= $order['order_id'] ?></a></td>
                            <td><div class="th-title visible-xs">Дата</div><?= $order['order_create'] ?></td>
                            <td><div class="th-title visible-xs">Статус</div><?= $order['status'] ?></td>
                            <td><div class="th-title visible-xs">Цена</div><?= $order['order_price'] ?></td>
                            <td><div class="th-title visible-xs">Достава</div><?= $order['transport_p'] ?></td>
                            <td><div class="th-title visible-xs">Вкупно</div><?= $order['order_price'] + $order['transport_p'] ?></td>
                        </tr>
                    </tbody>            
                <?php } ?>
            </table>
        <?php } else { ?>
            <a href="<?= base_url() ?>" class="btn btn--ys">Започни со купување</a>
        <?php } ?>
    </div>
</div>