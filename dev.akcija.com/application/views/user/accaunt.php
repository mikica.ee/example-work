<div id="pageContent">
    <div class="container">				
        <div class="title-box">
            <h1 class="text-center text-uppercase title-under">Кориснички Профил</h1>
        </div>
        <h4 class="text-uppercase">Детали</h4>
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-5">
                <table class="table table-params">
                    <tbody>
                        <tr>
                            <td class="text-right color-dark"><b>Име:</b></td>
                            <td><?= $user['web_first'] ?></td>
                        </tr>
                        <tr>
                            <td class="text-right color-dark"><b>Презиме:</b></td>
                            <td><?= $user['web_last'] ?></td>
                        </tr>
                        <tr>
                            <td class="text-right color-dark"><b>E-mail:</b></td>
                            <td><a href="mailto:<?= $user['web_first'] ?>"><?= $user['web_first'] ?></a></td>
                        </tr>
                        <tr>
                            <td class="text-right color-dark"><b>Адреса:</b></td>
                            <td><?= $user['web_email'] ?></td>
                        </tr>
                        <tr>
                            <td class="text-right color-dark"><b>Тел:</b></td>
                            <td><?= $user['web_tel'] ?></td>
                        </tr>
                    </tbody>
                </table>
                <a href="#" class="btn btn--ys">Промени ги податоците</a>
                <a href="<?= base_url("user/changepass") ?>" class="btn btn--ys">Промени ја лозинка</a>
            </div>
        </div>
    </div>
</div>