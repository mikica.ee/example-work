<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
    $(document).ready(function () {
        $("button#order").click(function () {
            var quick_user_name = $('#quick_user_name').val();
            var quick_user_tel = $('#quick_user_tel').val();
        });
    });
</script>
<div class="container">
    <div class="row">
        <div class="divider divider--md visible-sm visible-xs"></div>
        <section class="col-sm-12 col-md-6 col-lg-6 col-xl-6 col-xl-offset-3">
            <div class="login-form-box">
                <h3 class="color small">БРЗА НАРАЧКА</h3>
                <p>
                    Внесете го вашето Име и Презиме и телефонски број на кој сакате да бидете контактирани за да ја потврдите нарачката! 
                </p>
                <form id="Form" action="javascript:;">
<!--                <form id="Form" action="<?= base_url('checkout/quick') ?>" method="POST">-->
                    <div class="form-group">
                        <label for="quick_user_name">Име и Презиме <sup>*</sup></label>
                        <input type="text" class="form-control" id="quick_user_name" name="quick_user_name" placeholder="Име и Презиме">
                        <p class="help-block">Внесете го вашето Име и Презиме</p>
                    </div>
                    <div class="form-group">
                        <label for="quick_user_tel">Телефон <sup>*</sup></label>
                        <input type="text" class="form-control" id="quick_user_tel" name="quick_user_tel" placeholder="07* *** ***">
                        <p class="help-block">Внесете го вашиот Телефонски број</p>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <a id="order" class="btn btn--ys btn-top btn--xl"><span class="icon icon-vpn_key"></span>Нарачај</a>			               			
                        </div>
                        <div class="divider divider--md visible-xs"></div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="pull-right note btn-top">* Задолжителни полиња</div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>
<script>
    $(document).ready(function ($) {
        $("a#order").click(function () {
            var quick_user_name = $("#quick_user_name").val();
            var quick_user_tel = $("#quick_user_tel").val();
            //alert(price);
            var url = "<?php echo site_url('checkout/quick') ?>";
            var obj = {
                quick_user_name: quick_user_name,
                quick_user_tel: quick_user_tel
            }
            $.post(url, obj, function (data, textStatus, jqXHR) {
                var text = "Успешно ја извршивте нарашката #"+data+" ";
                swal(text, {
                    icon: "success",
                    buttons: {
                        home: "Почетна",
                        //orders: "Прегледај ги нарачките",
                    },
                }).then((value) => {
                    switch (value) {
                        case "home":
                            location.href = '<?= base_url() ?>';
                            break;
                        //case "orders":
                            //location.href = '<?= base_url("orders") ?>';
                            //break;
                        default:
                            location.href = '<?= base_url() ?>';
                    }
                });
            });
        });
    });
</script>