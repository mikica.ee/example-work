<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
    <div class="row">
        
        <div class="divider divider--md visible-sm visible-xs"></div>
        <section class="col-sm-12 col-md-6 col-lg-6 col-xl-6 col-xl-offset-3">
            <div class="login-form-box">
                <h3 class="color small">НАЈАВА</h3>
                <p>
                    
                </p>
                <?php if (validation_errors()) : ?>
                    <h3 class="color small"><?= validation_errors() ?></h3>
                <?php endif; ?>
                <?php if (isset($error)) : ?>
                    <h3 class="color small"><?= $error ?></h3>
                <?php endif; ?>
                <?= form_open() ?>
                <div class="form-group">
                    <label for="email">Email <sup>*</sup></label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Внесете го вашиот email">
                    <p class="help-block">Валидна email адреса</p>
                </div>
                <div class="form-group">
                    <label for="password">Лозинка <sup>*</sup></label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Вашата лозинка">
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <button type="submit" class="btn btn--ys btn-top btn--xl"><span class="icon icon-vpn_key"></span>Најави се</button>			               			
                    </div>
                    <div class="divider divider--md visible-xs"></div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <div class="pull-right note btn-top">* Задолжителни полиња</div>
                    </div>
                </div>			               			                
                <p class="btn-top">
                    <a class="link-color" href="<?= base_url('user/lostpassword') ?>">Ја заборавивте лозинката?</a>
                </p>
                </form>
            </div>
        </section>
    </div><!-- .row -->
</div><!-- .container -->