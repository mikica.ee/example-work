<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
    $(document).ready(function () {
        function ValidateEmail(email) {
            var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            return expr.test(email);
        }
        ;
        $("button#reset").click(function () {
            var email = $('#email').val();
            if (!ValidateEmail(email)) {
                swal("Невалидна email адреса!", "", "error");
            } else {
                var url = "<?php echo site_url('user/passreset') ?>";
                var obj = {
                    email: email
                }
                $.post(url, obj, function (data, textStatus, jqXHR) {
                    //alert(data);

                    if (data == 1) {
                        swal("Вашата лозинка е успешно променета. Проверете ја вашата email адреса за понатамошни инструкции", {
                            icon: "success",
                            buttons: {
                                home: "Продолжи со купување",
                                login: "Најава",
                            },
                        }).then((value) => {
                            switch (value) {
                                case "home":
                                    location.href = '<?= base_url() ?>';
                                    break;
                                case "login":
                                    location.href = '<?= base_url("login") ?>';
                                    break;
                                default:
                                    location.href = '<?= base_url("login") ?>';
                            }
                        });

                    } else {
                        swal("Со Вашата email адреса: "+email+" нема креирано профил", {
                            icon: "error",
                            buttons: {
                                home: "Обиди се повторно",
                                login: "Најава",
                            },
                        }).then((value) => {
                            switch (value) {
                                case "home":
                                    location.reload();
                                    break;
                                case "login":
                                    location.href = '<?= base_url("login") ?>';
                                    break;
                                default:
                                    location.reload();
                            }
                        });
                    }


                });
                //alert("Valid email address.");
            }
        });
    });
</script>
<div class="container">
    <div class="row">
        <div class="divider divider--md visible-sm visible-xs"></div>
        <section class="col-sm-12 col-md-6 col-lg-6 col-xl-6 col-xl-offset-3">
            <div class="login-form-box">
                <h3 class="color small">Изгубена лозинка</h3>
                <p>
                    Внесете ја вашата email адреса и кликнете на копчето "Нова лозинка"
                </p>
                <form id="Form" action="javascript:;">
                    <div class="form-group">
                        <label for="email">Email <sup>*</sup></label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Внесете го вашиот email">
                        <p class="help-block">Валидна email адреса</p>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <button id="reset" type="submit" class="btn btn--ys btn-top btn--xl"><span class="icon icon-vpn_key"></span>Нова лозинка</button>			               			
                        </div>
                        <div class="divider divider--md visible-xs"></div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="pull-right note btn-top">* Задолжителни полиња</div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>