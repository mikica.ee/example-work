<?php
$cookie = get_cookie("clientid");
if (!empty($cookie)) {
    $cookie = json_decode($cookie);
    $cname = explode(" ", $cookie->client_name);
    $name = $cname[0];
    $last = $cname[1];
    ?>
    <script>
        $(document).ready(function ($) {
            $('#web_first').val('<?= $name ?>');
            $('#web_last').val('<?= $last ?>');
            $('#email').val('<?= $cookie->client_mail ?>');
            $('#web_tel').val('<?= $cookie->client_tel ?>');
            $("select option").filter(function () {
                return $(this).text() == '<?= $cookie->client_city ?>';
            }).prop('selected', true);
            $('#web_add').val('<?= $cookie->client_add ?>');
        });
    </script>
    <?php
}
?>


<main id="content" role="main" class="checkout-page">
    <!-- breadcrumb -->
    <div class="bg-gray-13 bg-md-transparent">
        <div class="container">
            <!-- breadcrumb -->
            <div class="my-md-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="../home/index.html">Home</a></li>
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Checkout</li>
                    </ol>
                </nav>
            </div>
            <!-- End breadcrumb -->
        </div>
    </div>
    <!-- End breadcrumb -->

    <div class="container">
        <div class="mb-5">
            <h1 class="text-center card-backg">Форма за наплата</h1>
        </div>
        <!-- Accordion -->
<!--        <div id="shopCartAccordion1" class="accordion rounded mb-6">
             Card 
            <div class="card border-0">
                <div id="shopCartHeadingTwo" class="alert alert-primary mb-0" role="alert">
                    Have a coupon? <a href="#" class="alert-link" data-toggle="collapse" data-target="#shopCartTwo" aria-expanded="false" aria-controls="shopCartTwo">Click here to enter your code</a>
                </div>
                <div id="shopCartTwo" class="collapse border border-top-0" aria-labelledby="shopCartHeadingTwo" data-parent="#shopCartAccordion1" style="">
                    <form class="js-validate p-5" novalidate="novalidate">
                        <p class="w-100 text-gray-90">If you have a coupon code, please apply it below.</p>
                        <div class="input-group input-group-pill max-width-660-xl">
                            <input type="text" class="form-control" name="name" placeholder="Coupon code" aria-label="Promo code">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-block btn-dark font-weight-normal btn-pill px-4">
                                    <i class="fas fa-tags d-md-none"></i>
                                    <span class="d-none d-md-inline">Apply coupon</span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
             End Card 
        </div>-->
        <!-- End Accordion -->
        <form method="post" enctype="multipart/form-data" id="insertorder" action="<?= site_url('payment/payredirect') ?>" name="insertorder">
            <div class="row">
                <div class="col-lg-7 order-lg-1">
                    <div class="pb-7 mb-7">
                        <!-- Title -->
                        <div class="border-bottom border-color-1 mb-5">
                            <h3 class="section-title mb-0 pb-2 font-size-25">ИМЕ И АДРЕСА</h3>
                        </div>
                        <!-- End Title -->

                        <!-- Billing Form -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="web_first">Име <sup>*</sup></label>
                                    <input style="font-style: italic;" type="text" class="form-control" id="web_first" name="web_first" placeholder="Внесете Име" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="web_last">Презиме <sup>*</sup></label>
                                    <input style="font-style: italic;" type="text" class="form-control" id="web_last" name="web_last" placeholder="Внесете Презиме" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">Email <sup>*</sup></label>
                                    <input style="font-style: italic;" type="email" class="form-control" id="email" name="email" placeholder="Валидна email адреса" required>
                                    <!-- <p class="help-block">Валидна email адреса</p> -->
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="web_tel">Телефон <sup>*</sup></label>
                                    <input style="font-style: italic;" type="text" class="form-control" id="web_tel" name="web_tel" placeholder="Мобилен телефон" required>
                                    <!-- <p class="help-block">Мобилен телефон</p> -->
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="web_city">Град <sup>*</sup></label>
                                    <select class='form-control' id="web_city" name='web_city' placeholder="Најмалку 6 карактери..." required>
                                        <?php echo $city; ?>
                                    </select>
                                    <!-- <p class="help-block">Најмалку 6 карактери</p> -->
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="web_add">Адреса <sup>*</sup></label>
                                    <input style="font-style: italic;" type="text" class="form-control" id="web_add" name="web_add" placeholder="Адреса за пристигнување на пратката" required>
                                    <!-- <p class="help-block">Адресата на која сакате да ви пристигнуваат пратките</p> -->
                                </div>
                            </div>
                        </div>
                        <!-- End Billing Form -->
                    </div>
                </div>
                <div class="col-lg-5 order-lg-2 mb-7 mb-lg-0">
                    <div class="pl-lg-3 ">
                        <div class="bg-gray-1 rounded-lg">
                            <!-- Order Summary -->
                            <div class="p-4-1 mb-4 checkout-table">
                                <!-- Title -->
                                <div class="border-bottom border-color-1 mb-5">
                                    <h3 class="section-title mb-0 pb-2 font-size-25">НАРАЧАНИ ПРОИЗВОДИ</h3>
                                </div>
                                <!-- End Title -->

                                <!-- Product Content -->
                                <table class="table">

                                    <thead>
                                        <tr>
                                            <th class="product-name">Производи</th>
                                            <th class="product-total">Пресметка</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $total = 0; ?>
                                        <?php $delay = 0; ?>
                                        <?php $delivery = calc_delivery(); ?>
                                        <?php if (isset($_SESSION['cart'])) { ?>
                                            <?php foreach ($_SESSION['cart'] as $x => $item) { ?>
                                                <tr class="cart_item">
                                                    <td><?= $item['title']; ?>&nbsp;<strong class="product-quantity">× 1</strong></td>
                                                    <td><?= $item['price']; ?></td>
                                                </tr>
                                                <?php
                                                $total += $item['num'] * $item['price'];
                                                if ($delay < $item['delay']) {
                                                    $delay = $item['delay'];
                                                }
                                                ?>    
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>

                                    <tfoot>
                                        <tr>
                                            <th>Вкупно:</th>
                                            <td><?= $total ?> ден.</td>
                                        </tr>
                                        <tr>
                                            <th>Достава:</th>
                                            <td><?= $delivery ?> ден.</td>
                                        </tr>
                                        <tr>
                                            <th>ЗА НАПЛАТА:</th>
                                            <td><strong><?= $total + $delivery ?><br>ден.</strong></td>
                                        </tr>
                                    </tfoot>

                                </table>
                                <!-- End Product Content -->
                                <div class="border-top border-width-3 border-color-1 pt-3 mb-3">
                                    <!-- Basics Accordion -->
                                    <div id="basicsAccordion1">
                                        <!-- Card -->
                                        <div class="border-bottom border-color-1 border-dotted-bottom">
                                            <div class="p-3" id="basicsHeadingOne">
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" class="custom-control-input" id="dostavar" name="order_pay_type" value="1" checked>
                                                    <label class="custom-control-label form-label" for="dostavar"
                                                           data-toggle="collapse"
                                                           data-target="#basicsCollapseOne"
                                                           aria-expanded="true"
                                                           aria-controls="basicsCollapseOne">
                                                        Плаќање при достава
                                                    </label>
                                                </div>
                                                
                                            </div>
                                            <div id="basicsCollapseOne" class="collapse show border-top border-color-1 border-dotted-top bg-dark-lighter"
                                                 aria-labelledby="basicsHeadingOne"
                                                 data-parent="#basicsAccordion1">
                                                <div class="p-4-1-p">
                                                    Плаќање при достава
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Card -->

                                        <!-- Card -->
                                        <div class="border-bottom border-color-1 border-dotted-bottom">
                                            <div class="p-3" id="basicsHeadingThree">
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" class="custom-control-input" id="nlb" name="order_pay_type" value="2">
                                                    <label class="custom-control-label form-label" for="nlb"
                                                           data-toggle="collapse"
                                                           data-target="#basicsCollapseThree"
                                                           aria-expanded="false"
                                                           aria-controls="basicsCollapseThree">
                                                        Плаќање со картичка
                                                    </label>
                                                </div>
                                            </div>
                                            <div id="basicsCollapseThree" class="collapse border-top border-color-1 border-dotted-top bg-dark-lighter"
                                                 aria-labelledby="basicsHeadingThree"
                                                 data-parent="#basicsAccordion1">
                                                <div class="p-4-1-p">
                                                    <img class="paysera" src="<?= base_url('img/visa-maestro-master.png') ?>" class="img-responsive" height="45px">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Card -->

                                        <!-- Card -->
                                        <div class="border-bottom border-color-1 border-dotted-bottom">
                                            <div class="p-3" id="basicsHeadingTwo">
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" class="custom-control-input" id="diners" name="order_pay_type" value="3">
                                                    <label class="custom-control-label form-label" for="diners"
                                                           data-toggle="collapse"
                                                           data-target="#basicsCollapseTwo"
                                                           aria-expanded="false"
                                                           aria-controls="basicsCollapseTwo">
                                                        Плаќање на рати со Diners
                                                    </label>
                                                </div>
                                            </div>
                                            <div id="basicsCollapseTwo" class="collapse border-top border-color-1 border-dotted-top bg-dark-lighter"
                                                 aria-labelledby="basicsHeadingTwo"
                                                 data-parent="#basicsAccordion1">
                                                <div class="p-4-1-p ">
                                                    <img class="paysera" src="<?= base_url('img/Diners_Club_Logo3.svg') ?>" class="img-responsive" height="45px">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Card -->
                                        <!-- Card -->
                                        <div class="border-bottom border-color-1 border-dotted-bottom">
                                            <div class="p-3" id="basicsHeadingTwo3">
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" class="custom-control-input" id="iute" name="order_pay_type" value="4">
                                                    <label class="custom-control-label form-label" for="iute"
                                                           data-toggle="collapse"
                                                           data-target="#basicsCollapseTwo3"
                                                           aria-expanded="false"
                                                           aria-controls="basicsCollapseTwo3">
                                                        Плаќање на рати со IuteCredit
                                                    </label>
                                                </div>
                                            </div>
                                            <div id="basicsCollapseTwo3" class="collapse border-top border-color-1 border-dotted-top bg-dark-lighter"
                                                 aria-labelledby="basicsHeadingTwo3"
                                                 data-parent="#basicsAccordion1">
                                                <div class="p-4-1-p">
                                                    <img class="iute" src="<?= base_url('img/iute-img.png') ?>" class="img-responsive" height="45px">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Card -->
                                    </div>
                                    <!-- End Basics Accordion -->
                                </div>
                                <div class="form-group d-flex align-items-center justify-content-between px-3 mb-5">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="terms"
                                               data-msg="Please agree terms and conditions."
                                               data-error-class="u-has-error"
                                               data-success-class="u-has-success">
                                        <label class="form-check-label form-label" for="defaultCheck10">
                                            Се согласувам со  <a class="color" target="_blank" href="<?= site_url('page/terms') ?>">условите за користење</a>
                                            <span class="text-danger">*</span>
                                        </label>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary-dark-w btn-block btn-pill font-size-20 mb-3 py-3">НАРАЧАЈ</button>
                            </div>
                            <!-- End Order Summary -->
                        </div>

                    </div>
                </div>

            </div>
        </form>
    </div>
</main>
<script>
    $(document).ready(function ($) {
        $('#insertorder').submit(function (e) {
            if ($('#terms').is(":checked"))
            {
            } else {
                e.preventDefault();
                swal("Ве молиме согласете се со условите за користење!");
            }
        });
        $("a#deleteall").click(function () {
            var cart_item_id = 1;
            var url = "<?php echo site_url('cart/delall') ?>";
            var obj = {
                cart_item_id: cart_item_id
            }
            $.post(url, obj, function (data, textStatus, jqXHR) {
                //alert(data);
            }).done(function (data) {
                location.reload();
            }).fail(function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown + ": " + jqXHR.responseText);
            });
        });
        $("a#delete-cart-item").click(function () {
            var cart_item_id = $(this).attr("cart_item_id");
            var url = "<?php echo site_url('cart/delitem') ?>";
            var obj = {
                cart_item_id: cart_item_id
            }
            $.post(url, obj, function (data, textStatus, jqXHR) {

            }).done(function (data) {
                location.reload();
            }).fail(function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown + ": " + jqXHR.responseText);
            });
        });
    });
</script>