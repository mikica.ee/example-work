
<main id="content" role="main">
    <!-- breadcrumb -->
    <div class="bg-gray-13 bg-md-transparent">
        <div class="container">
            <!-- breadcrumb -->
            <div class="my-md-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="../home/index.html">Home</a></li>
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Compare</li>
                    </ol>
                </nav>
            </div>
            <!-- End breadcrumb -->
        </div>
    </div>
    <!-- End breadcrumb -->

    <div class="container">
        <div class="table-responsive table-bordered table-compare-list mb-10 border-0">
            <table class="table">
                <?php if (!empty($products)) { ?>
                    <tbody>
                        <tr>
                            <th class="min-width-200">Производ</th>
                            <td>
                                <?php if (isset($products[0])) { ?>
                                    <a href="<?= site_url("product/" . $products[0]['web_products_slug']) ?>" class="product d-block">
                                        <div class="product-compare-image">
                                            <div class="d-flex mb-3">
                                                <img class="img-fluid mx-auto" height="250" width="250"  src="<?= site_url("app/" . $products[0]['web_products_img']) ?>" alt="Image Description">
                                            </div>
                                        </div>
                                        <h3 class="product-item__title text-blue font-weight-bold mb-3"><?= $products[0]['web_products_name'] ?></h3>
                                    </a>
                                <?php } ?>
                            </td>
                            <td>
                                <?php if (isset($products[1])) { ?>
                                    <a href="<?= site_url("product/" . $products[1]['web_products_slug']) ?>" class="product">
                                        <div class="product-compare-image">
                                            <div class="d-flex mb-3">
                                                <img class="img-fluid mx-auto" height="250" width="250" src="<?= site_url("app/" . $products[1]['web_products_img']) ?>" alt="Image Description">
                                            </div>
                                        </div>
                                        <h3 class="product-item__title text-blue font-weight-bold mb-3"><?= $products[1]['web_products_name'] ?></h3>
                                    </a>
                                <?php } ?>
                            </td>
                            <td>
                                <?php if (isset($products[2])) { ?>
                                    <a href="<?= site_url("product/" . $products[2]['web_products_slug']) ?>" class="product">
                                        <div class="product-compare-image">
                                            <div class="d-flex mb-3">
                                                <img class="img-fluid mx-auto" height="250" width="250" src="<?= site_url("app/" . $products[2]['web_products_img']) ?>" alt="Image Description">
                                            </div>
                                        </div>
                                        <h3 class="product-item__title text-blue font-weight-bold mb-3"><?= $products[2]['web_products_name'] ?></h3>
                                    </a>
                                <?php } ?>
                            </td>
                            <td>
                                <?php if (isset($products[3])) { ?>
                                    <a href="<?= site_url("product/" . $products[3]['web_products_slug']) ?>" class="product">
                                        <div class="product-compare-image">
                                            <div class="d-flex mb-3">
                                                <img class="img-fluid mx-auto" height="250" width="250" src="<?= site_url("app/" . $products[3]['web_products_img']) ?>" alt="Image Description">
                                            </div>
                                        </div>
                                        <h3 class="product-item__title text-blue font-weight-bold mb-3"><?= $products[3]['web_products_name'] ?></h3>
                                    </a>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Опис</th>
                            <td>
                                <?php
                                if (isset($products[0])) {
                                    echo $products[0]['web_products_text'];
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (isset($products[1])) {
                                    echo $products[1]['web_products_text'];
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (isset($products[2])) {
                                    echo $products[2]['web_products_text'];
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (isset($products[3])) {
                                    echo $products[3]['web_products_text'];
                                }
                                ?>
                            </td>
                        </tr>



                        <tr>
                            <th>Цена</th>
                            <td>
                                <?php if (isset($products[0])) { ?>
                                    <div class="product-price price"><?= $products[0]['web_price'] ?> ден.</div>
                                <?php } ?>
                            </td>
                            <td>
                                <?php if (isset($products[1])) { ?>
                                    <div class="product-price price"><?= $products[1]['web_price'] ?> ден.</div>
                                <?php } ?>
                            </td>
                            <td>
                                <?php if (isset($products[2])) { ?>
                                    <div class="product-price price"><?= $products[2]['web_price'] ?> ден.</div>
                                <?php } ?>
                            </td>
                            <td>
                                <?php if (isset($products[3])) { ?>
                                    <div class="product-price price"><?= $products[3]['web_price'] ?> ден.</div>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Погледни го Производот</th>
                            <td>
                                <?php if (isset($products[0])) { ?>
                                    <div class=""><a href="<?= site_url("product/" . $products[0]['web_products_slug']) ?>" class="btn btn-soft-secondary mb-3 mb-md-0 font-weight-normal px-5 px-md-3 px-xl-5">Погледни</a></div>
                                <?php } ?>
                            </td>
                            <td>
                                <?php if (isset($products[1])) { ?>
                                    <div class=""><a href="<?= site_url("product/" . $products[1]['web_products_slug']) ?>" class="btn btn-soft-secondary mb-3 mb-md-0 font-weight-normal px-5 px-md-3 px-xl-5">Погледни</a></div>
                                <?php } ?>
                            </td>
                            <td>
                                <?php if (isset($products[2])) { ?>
                                    <div class=""><a href="<?= site_url("product/" . $products[2]['web_products_slug']) ?>" class="btn btn-soft-secondary mb-3 mb-md-0 font-weight-normal px-5 px-md-3 px-xl-5">Погледни</a></div>
                                <?php } ?>
                            </td>
                            <td>
                                <?php if (isset($products[3])) { ?>
                                    <div class=""><a href="<?= site_url("product/" . $products[3]['web_products_slug']) ?>" class="btn btn-soft-secondary mb-3 mb-md-0 font-weight-normal px-5 px-md-3 px-xl-5">Погледни</a></div>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Избриши</th>
                            <td class="text-center">
                                <?php if (isset($products[0])) { ?>
                                    <a href="javascript:;" key ="<?= $products[0]['key'] ?>" id ="delcompare" class="text-gray-90"><i class="fa fa-times"></i></a>
                                <?php } ?>
                            </td>
                            <td class="text-center">
                                <?php if (isset($products[1])) { ?>
                                    <a href="javascript:;" key ="<?= $products[1]['key'] ?>" id ="delcompare" class="text-gray-90"><i class="fa fa-times"></i></a>
                                <?php } ?>
                            </td>
                            <td class="text-center">
                                <?php if (isset($products[2])) { ?>
                                    <a href="javascript:;" key ="<?= $products[2]['key'] ?>" id ="delcompare" class="text-gray-90"><i class="fa fa-times"></i></a>
                                <?php } ?>
                            </td>
                            <td class="text-center">
                                <?php if (isset($products[3])) { ?>
                                    <a href="javascript:;" key ="<?= $products[3]['key'] ?>" id ="delcompare" class="text-gray-90"><i class="fa fa-times"></i></a>
                                <?php } ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            <?php } else { ?>
                <div class="col-sm-12 col-md-12">
                    <div class="panel panel-default font-size-24">
                        <div class="panel-heading text-center">Немате внесено производи за споредба</div>
                        <div class="mb-12 text-center">
                            <h1>Внесете производ</h1>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</main>
<script>
    $(document).ready(function ($) {
        $("a#delcompare").click(function () {
            var key = $(this).attr("key");
            var url = "<?php echo site_url('compare/delcompare') ?>";
            var obj = {
                key: key
            }
            $.post(url, obj, function (data, textStatus, jqXHR) {

            }).done(function (data) {
                location.reload();
            }).fail(function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown + ": " + jqXHR.responseText);
            });
        });
    });
</script>