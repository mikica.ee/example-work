<main id="content" role="main" class="cart-page">
    <!-- breadcrumb -->
    <div class="bg-gray-13 bg-md-transparent">
        <div class="container">
            <!-- breadcrumb -->
            <div class="my-md-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="<?= base_url('') ?>">Почетна</a></li>
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Кошничка</li>
                    </ol>
                </nav>
            </div>
            <!-- End breadcrumb -->
        </div>
    </div>
    <!-- End breadcrumb -->

    <div class="container">
        <div class="mb-4">
            <h1 class="text-center card-backg"><i class="ec ec-shopping-bag"></i> Кошничка</h1>
        </div>
        <div class="mb-10 cart-table">
            <form class="mb-4" action="#" method="post">
                <table class="table" cellspacing="0">
                    <thead class="table-mrgn row-titles">
                        <tr>
                            <th style="font-weight:bold; font-size:16px; color:white;" class="product-remove">&nbsp;</th>
                            <th style="font-weight:bold; font-size:16px; color:white;" class="product-thumbnail">&nbsp;</th>
                            <th style="font-weight:bold; font-size:16px; color:white;" class="product-name">Производ</th>
                            <th style="font-weight:bold; font-size:16px; color:white;" class="product-price">Цена</th>
                            <th style="font-weight:bold; font-size:16px; color:white;" class="product-quantity w-lg-15">Количина</th>
                            <th style="font-weight:bold; font-size:16px; color:white;" class="product-rabat">Рабат</th>
                            <th style="font-weight:bold; font-size:16px; color:white;" class="product-subtotal">Вкупно</th>
                        </tr>
                    </thead>
                    <tbody>



                        <?php $total = 0; ?>
                        <?php $delay = 0; ?>
                        <?php if (isset($_SESSION['cart'])) { ?>
                            <?php foreach ($_SESSION['cart'] as $x => $item) { ?>

                                <tr class="">
                                    <td class="text-center">
                                        <a class="shopping-cart-table__delete icon icon-clear" ></a>
                                        <a href="javascript:;" id="delete-cart-item" cart_item_id = "<?= $x ?>" class="text-gray-32 font-size-26">×</a>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <a href="#"><img class="img-fluid max-width-100 p-1 border border-color-1" src="<?= $item['img'] ?>" alt="Image Description"></a>
                                    </td>

                                    <td data-title="">
                                        <a href="#"><img class="product-none img-fluid max-width-100 p-1 border border-color-1" src="<?= $item['img'] ?>" alt="Image Description"></a>
                                        <a href="#" class="title-css txt-bold"><?= $item['title'] ?></a>
                                    </td>

                                    <td data-title="Цена">
                                        <span class=""><?= $item['price'] ?> ден.</span>
                                    </td>

                                    <td data-title="Количина">
                                        <span class="sr-only">Количина</span>
                                        <!-- Quantity -->
                                        <div class="border rounded-pill-1 py-1 width-122c w-xl-80 px-3">
                                            <div class="js-quantity row align-items-center">
                                                <div class="col">
                                                    <input class="js-result form-control h-auto border-0 rounded p-0 shadow-none" type="text" value="<?= $item['num']; ?>">
                                                </div>
                                                <div class="col-auto pr-1">
                                                    <a class="js-minus btn btn-icon btn-xs btn-outline-secondary rounded-circle border-0" href="javascript:;">
                                                        <small class="fas fa-minus btn-icon__inner"></small>
                                                    </a>
                                                    <a class="js-plus btn btn-icon btn-xs btn-outline-secondary rounded-circle border-0" href="javascript:;">
                                                        <small class="fas fa-plus btn-icon__inner"></small>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Quantity -->
                                    </td>
                                    <td class="" data-title="Рабат">
                                        <span class="">

                                            <?= $item['dsc']; ?> %
                                        </span>
                                    </td>

                                    <td class="total-css" data-title="Вкупно">
                                        <span class="prize-css">
                                            <?php $productp = $item['num'] * $item['price']; ?>
                                            <?= $productp; ?> ден.
                                        </span>
                                    </td>
                                </tr> 
                                <?php
                                $total += $productp;
                                if ($delay < $item['delay']) {
                                    $delay = $item['delay'];
                                }
                                ?>
                            <?php } ?>
                        <?php } ?>
                <!-- <tr>
                    <td colspan="6" class="border-top space-top-2 justify-content-center">
                        <div class="pt-md-3">
                            <div class="d-block d-md-flex flex-center-between">
                                <div class="mb-3 mb-md-0 w-xl-40">
                                   Apply coupon Form -->
                        <!--                                            <form class="js-focus-state">
                                                                        <label class="sr-only" for="subscribeSrEmailExample1">Coupon code</label>
                                                                        <div class="input-group">
                                                                            <input type="text" class="form-control" name="text" id="subscribeSrEmailExample1" placeholder="Coupon code" aria-label="Coupon code" aria-describedby="subscribeButtonExample2" required>
                                                                            <div class="input-group-append">
                                                                                <button class="btn btn-block btn-dark px-4" type="button" id="subscribeButtonExample2"><i class="fas fa-tags d-md-none"></i><span class="d-none d-md-inline">Apply coupon</span></button>
                                                                            </div>
                                                                        </div>
                                                                    </form>-->
                        <!-- End Apply coupon Form -->
                        </div>
                        <!-- Tuka beshe Buttoon Naracaj -->
                        </div>
                        </div>
                        </td>
                        </tr> 
                    </tbody>
                </table>
            </form>
        </div>
        <div class="mb-8 cart-total">
            <div class="row">
                <div class="col-xl-5 col-lg-6 offset-lg-6 offset-xl-7 col-md-8 offset-md-4">
                    <div class="border-bottom border-color-1 mb-3">
                        <h3 class="d-inline-block section-title mb-0 pb-2 font-size-26">Вкупно во Кошничка</h3>
                    </div>
                    <?php $delivery = calc_delivery(); ?>
                    <table class="table mb-3 mb-md-0">
                        <tbody>
                            <tr class="cart-subtotal">
                                <th>Вкупно:</th>
                                <td data-title="Subtotal"><span class="amount"><?= $total ?></span> ден.</td>
                            </tr>
                            <tr class="shipping">
                                <th>Достава:</th>
                                <td data-title="Shipping">
                                    достава за <?= $delay ?> дена - <span class="amount"><?= $delivery ?></span> ден.

                                </td>
                            </tr>
                            <tr class="order-total">
                                <th>ЗА НАПЛАТА:</th>
                                <td data-title="Total"><strong><span class="amount"><?= $total + $delivery ?></span></strong> ден.</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class=" d-flex justify-content-center shadow p-4-1-p">
                        <a href="<?= base_url('checkout') ?>" class=" btn btn-primary-dark-w ml-md-2 px-5 px-md-4 px-lg-5 w-100 w-md-auto d-md-inline-block ">Нарачај</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>


<script>
    //fbq('track', 'AddToCart');
</script>

<script>
    $(document).ready(function ($) {
        $("a#delete-cart-item").click(function () {
            var cart_item_id = $(this).attr("cart_item_id");
            var url = "<?php echo site_url('cart/delitem') ?>";
            var obj = {
                cart_item_id: cart_item_id
            }

            $.post(url, obj, function (data, textStatus, jqXHR) {

            }).done(function (data) {
                location.reload();
            }).fail(function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown + ": " + jqXHR.responseText);
            });

        });
        $("a#deleteall").click(function (e) {
            var cart_item_id = 1;
            var url = "<?php echo site_url('cart/delall') ?>";
            var obj = {
                cart_item_id: cart_item_id
            }
            //if (confirm("Испразнете ја целата кошничка: " + cart_item_id)) {
            $.post(url, obj, function (data, textStatus, jqXHR) {
                //alert(data);
            }).done(function (data) {
                location.reload();
            }).fail(function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown + ": " + jqXHR.responseText);
            });
            //}
        });
    });
</script>