<?php ?>


<main id="content" role="main" class="cart-page">
    <!-- breadcrumb -->
    <div class="bg-gray-13 bg-md-transparent">
        <div class="container">
            <!-- breadcrumb -->
            <div class="my-md-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="../home/index.html">Home</a></li>
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Wishlist</li>
                    </ol>
                </nav>
            </div>
            <!-- End breadcrumb -->
        </div>
    </div>
    <!-- End breadcrumb -->
    <div class="container">
        <div class="my-6">
            <h1 class="text-center">Омилени Производи</h1>
        </div>
        <div class="mb-16 wishlist-table">
            <form class="mb-4" action="#" method="post">
                <div class="table-responsive">
                    <table class="table" cellspacing="0">
                        <thead>
                            <tr>
                                <th class="product-remove">&nbsp;</th>
                                <th class="product-thumbnail">&nbsp;</th>
                                <th class="product-name">Производ</th>
                                <th class="product-price">Цена</th>
                                <th class="product-subtotal min-width-200-md-lg">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $wishes = cookie_product('wish'); ?>
                            <?php if (isset($wishes)) { ?>
                                <?php foreach ($wishes as $x => $item) { ?>
                                    <tr>
                                        <td class="text-center">
                                            <a class="shopping-cart-table__delete icon icon-clear" ></a>
                                            <a href="javascript:;" id="delete-wish-item" web_products_id = "<?= $x ?>" <a class="shoppray-32 font-size-26">×</a>
                                        </td>
                                        <td class="d-none d-md-table-cell">
                                            <a href="#"><img class="img-fluid max-width-100 p-1 border border-color-1" src="<?= site_url("app/" . $item['web_products_img']) ?>" alt="Image Description"></a>
                                        </td>

                                        <td data-title="Product">
                                            <a href="#" class="text-gray-90"><?= $item['web_products_name'] ?></a>
                                        </td>

                                        <td data-title="Unit Price">
                                            <span class=""><?= $item['web_price']; ?> .ден</span>
                                        </td>

                                        <td>
                                            <div class=""><a href="<?= site_url("product/" . $item['web_products_slug']) ?>" class="btn btn-soft-secondary mb-3 mb-md-0 font-weight-normal px-5 px-md-3 px-xl-5">Погледни го Производот</a></div>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } else { ?>
                            <div class="col-sm-12 col-md-12">
                                <div class="panel panel-default font-size-24">
                                    <div class="panel-heading text-center">Немате додадено производи во омилени</div>
                                    <div class="mb-12 text-center">
                                        <h1>Додадете производ</h1>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </div>
</main>
<script>
    $(document).ready(function ($) {
        $("a#delete-wish-item").click(function () {
            var web_products_id = $(this).attr("web_products_id");
            var url = "<?php echo site_url('wish/delitem') ?>";
            var obj = {
                web_products_id: web_products_id
            }

            $.post(url, obj, function (data, textStatus, jqXHR) {

            }).done(function (data) {
                location.reload();
            }).fail(function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown + ": " + jqXHR.responseText);
            });

        });
        $("a#deleteall").click(function (e) {
            var web_products_id = 1;
            var url = "<?php echo site_url('wish/delall') ?>";
            var obj = {
                web_products_id: web_products_id
            }
            //if (confirm("Испразнете ја целата кошничка: " + cart_item_id)) {
            $.post(url, obj, function (data, textStatus, jqXHR) {
                //alert(data);
            }).done(function (data) {
                location.reload();
            }).fail(function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown + ": " + jqXHR.responseText);
            });
            //}
        });
    });
</script>
