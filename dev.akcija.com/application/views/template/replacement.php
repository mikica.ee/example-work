<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

        <main id="content" role="main">
            <!-- breadcrumb -->
            <div class="bg-gray-13 bg-md-transparent">
                <div class="container">
                    <!-- breadcrumb -->
                    <div class="my-md-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="../home/index.html">Home</a></li>
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Terms and Conditions</li>
                            </ol>
                        </nav>
                    </div>
                    <!-- End breadcrumb -->
                </div>
            </div>
            <!-- End breadcrumb -->

            <div class="container">
                <div class="mb-12 text-center">
                    <h1>Замена на производ</h1>
                </div>
                <div class="mb-10">
                    <ol>
                        <li> Замена на производ се врши во рок од 14 дена од приемот на пратката. Производот треба да биде во оригиналното пакување, да не биде перен, носен или пеглан. Доставата при процес на замена е на сметка на купувачот.</li>
                        <li> Откако вашата нарачка ќе биде доставена,ве молиме проверете го производот и доколку на него има видливи оштетување или производот не е во согласност со вашата нарачката, контактирајте не на телефонскиот број 075 330 699  во рок од 24 часа од приемот на пратката</li>
                        <li><a href="<?=site_url() ?>">Akcija.com.mk</a> ги превзема трошковите за враќање или замена на производот доколку производот е пристигнат со фабричка грешка, оштетен при транспорт или не одговара на Вашата нарачка.</li>

                    </ol>
                </div>
                <div class="mb-10">
                    <h3 class="mb-6 pb-2 font-size-25">КОНТАКТ ИНФОРМАЦИИ</h3>
                    <p> Kонтактирајте не на телефонскиот број <a href="tel:+38975330699" class="font-size-20 text-blue-90">075 330 699 </a>  во рок од 24 часа од приемот на пратката</p>
                </div>
                <!-- Brand Carousel -->
                <!-- End Brand Carousel -->
            </div>
        </main>
