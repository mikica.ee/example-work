<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
    <head>
        <!-- Basic -->
        <meta charset="utf-8">
        <title>Sovrshena.mk</title>
        <meta name="keywords" content="HTML5 Template" />
        <meta name="description" content="Sovrshena.mk">
        <meta name="author" content="novak">		
        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- External Plugins CSS -->
        <style>
            img{ vertical-align:top; border:0;}
            table, table td { 
                padding:0;
                border:none;
                border-collapse:collapse;
                vertical-align: top;
            }
            a{ color:#1fc0a0;}
            a:hover{ text-decoration:none; color:#333;}
            ul{
                list-style: none;
                padding:0;
                margin: 0;
            }
        </style>

    </head>
    <body style="background:#e8e8e8; padding:0; margin:0; " bgcolor="#e8e8e8">
        <table cellpadding="0" cellspacing="0" width="100%"  bgcolor="#e8e8e8" style="background:#e8e8e8; vertical-align:top;">			  
            <tr>
                <td width="100%" valign="top" align="center" bgcolor="#e8e8e8" style="background:#e8e8e8; vertical-align:top;">
                    <table cellpadding="0" cellspacing="0" width="600" style="vertical-align:top; font-family:Arial, Helvetica, sans-serif;">
                        <!--header-->
                        <tr>
                            <td style="height:90px; text-align:left; background: #333333;"><table style="height:90px; text-align:left; background: #333333;" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="363" style="padding:15px 0 0 0;">
                                            <!--logo-->
                                            <a target="_blank" class="logo" href="http://sovrshena.mk/"><img style="margin-left:21px; border:0; display:block; width:100px; height:55px" src="http://test.sovrshena.mk/img/logo.jpg" alt="Sovrshena"></a>
                                            <!--/logo-->
                                        </td>
                                        <td width="237" style="padding:38px 0 0 0; color:#fff;  font-size:16px; line-height:1.1em; font-family:Arial, Helvetica, sans-serif;">                       		                           
                                            Секогаш совршени!                  

                                        </td>
                                    </tr>
                                </table></td>
                        </tr>
                        <!--/header-->

                        <!-- content -->

                        <tr>
                            <td style="width: 100%; background:#fff; text-align: center; font-size: 14px; font-family:Arial; color:#999999;  padding:0px 0 28px 0;">
                                <?= $data ?>
                            </td>
                        </tr>


                        <tr>
                            <td style="width: 100%; background:#fff; text-align: center; font-size: 30px; font-family:Arial; text-transform: uppercase; color:#333333;">
                                <img style="border:0; width:24px; height:23px; position: relative; top:4px; margin-right:4px; padding:0px 0 0px 0; " src="http://test.sovrshena.mk/img/icon_telephone.png" alt=""> <a  href="tel:078 381 616" value="+389 78 381 616" target="_blank" style="text-decoration: none; color:#333333;">078 381 616</a> 
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%; background:#fff; text-align: center; font-size: 14px; font-family:Arial; color:#999999;  padding:0px 0 28px 0;">
                                Секогаш тука за вас!
                            </td>
                        </tr>
                        <!-- footer -->
                        <tr>
                            <td style="background:#333333; text-align:left; padding:30px 0px 30px 0px;">
                                <table cellpadding="0" cellspacing="0" style=" background:#333333; text-align:left; vertical-align:top; font-family:Arial, Helvetica, sans-serif; width:100%;">
                                    <tr><td style="text-align: center;">
                                            <a href="http://www.facebook.com/sovrshena.mk/"><img style="border:0;" src="http://test.sovrshena.mk/img/soc_icon_1.png" height="52" width="52" alt="Facebook"></a><img src="http://test.sovrshena.mk/img/spacer.png" style="border:0;" height="52" width="10" alt="">
                                            <a href="http://www.twitter.com/"><img style="border:0;" src="http://test.sovrshena.mk/img/soc_icon_2.png" height="52" width="52" alt="Twitter"></a><img src="http://test.sovrshena.mk/img/spacer.png" height="52" width="10" style="border:0;" alt="">
                                            <a href="http://www.google.com/"><img src="http://test.sovrshena.mk/img/soc_icon_3.png" style="border:0;" height="52" width="52" alt="Google +"></a><img src="http://test.sovrshena.mk/img/spacer.png" style="border:0;" height="52" width="10" alt="">
                                            <a href="https://instagram.com/"><img src="http://test.sovrshena.mk/img/soc_icon_4.png" height="52" style="border:0;" width="52" alt="Instagram"></a><img src="http://test.sovrshena.mk/img/spacer.png" style="border:0;" height="52" width="10" alt="">
                                            <a href="https://www.youtube.com/channel/UCU-MvL5x8st-w05ngdjShBQ/videos"><img src="http://test.sovrshena.mk/img/soc_icon_5.png"  style="border:0;" height="52" width="52" alt="Youtube"></a>
                                        </td></tr>
                                    <tr><td style="text-align: center; color:#fff; font-size: 14px; padding:28px 0 24px;">
                                            <span style="font-size: 16px;"><strong>YOUR</strong>Store</span> © 2016 . All Rights Reserved.
                                        </td></tr>

                                </table>
                            </td>
                        </tr>
                        <!-- /footer -->
                    </table>	
                </td>
            </tr>
        </table>

    </body>
</html>