<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
     <main id="content" role="main">
            <!-- breadcrumb -->
            <div class="bg-gray-13 bg-md-transparent">
                <div class="container">
                    <!-- breadcrumb -->
                    <div class="my-md-3">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="../home/index.html">Home</a></li>
                                <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">Terms and Conditions</li>
                            </ol>
                        </nav>
                    </div>
                    <!-- End breadcrumb -->
                </div>
            </div>
            <!-- End breadcrumb -->

            <div class="container">
                <div class="mb-12 text-center">
                    <h1>Достава  и начин на плаќање</h1>
                </div>
                <div class="mb-10">
                    <ol>
                        <li>Доставата  на проиводите чини 99ден. Доставата се врши на цела територија на Република Македонија секој работен ден од 9 до 17часот.  Пред да се достави пратката доставувачот е должен да Ве исконтактира на дадениот телефон за да Ве извести за доставата и да се договорите за времето и местото.</li>
                        <li> Плаќањето е во готово при прием на пратката.</li>

                    </ol>
                </div>
                <div class="mb-10">
                    <h3 class="mb-6 pb-2 font-size-25">КОНТАКТ ИНФОРМАЦИИ</h3>
                    <p> Kонтактирајте не на телефонскиот број <a href="tel:+38975330699" class="font-size-20 text-blue-90">075 330 699 </a>  во рок од 24 часа од приемот на пратката</p>
                </div>
                <!-- Brand Carousel -->
                <!-- End Brand Carousel -->
            </div>
        </main>