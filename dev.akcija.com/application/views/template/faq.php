<main id="content" role="main">
    <!-- breadcrumb -->
    <div class="bg-gray-13 bg-md-transparent">
        <div class="container">
            <!-- breadcrumb -->
            <div class="my-md-3">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1"><a href="../home/index.html">Home</a></li>
                        <li class="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active" aria-current="page">FAQ</li>
                    </ol>
                </nav>
            </div>
            <!-- End breadcrumb -->
        </div>
    </div>
    <!-- End breadcrumb -->

    <div class="container">

        <div class="mb-12 text-center">
            <h1>Често поставувани прашања (ЧПП)</h1>
        </div>
        
        <!-- Basics Accordion -->
        <div id="basicsAccordion" class="mb-12">



            <?php foreach ($faqs as $key => $faq) { ?>
                <div class="card mb-3 border-top-0 border-left-0 border-right-0 border-color-1 rounded-0">
                    <div class="card-header card-collapse bg-transparent-on-hover border-0" id="basicsHeading<?= $key ?>">
                        <h5 class="mb-0">
                            <button type="button" class="px-0 btn btn-link btn-block d-flex justify-content-between card-btn py-3 font-size-25 border-0"
                                    data-toggle="collapse"
                                    <?php if ($key == 0) { ?>
                                        aria-expanded="true"
                                    <?php } else { ?>
                                        aria-expanded="false"
                                    <?php } ?>
                                    data-target="#basicsCollapse<?= $key ?>"
                                    aria-controls="basicsCollapse<?= $key ?>">
                                        <?= $faq['faq_title'] ?>

                                <span class="card-btn-arrow">
                                    <i class="fas fa-chevron-down text-gray-90 font-size-18"></i>
                                </span>
                            </button>
                        </h5>
                    </div>
                    <div id="basicsCollapse<?= $key ?>" class="collapse <?php if ($key == 0) { ?> show <?php } ?>"
                         aria-labelledby="basicsHeading<?= $key ?>"
                         data-parent="#basicsAccordion">
                        <div class="card-body pl-0 pb-8">
                            <p class="mb-0"><?= $faq['faq_text'] ?></p>
                        </div>
                    </div>
                </div>
            <?php } ?>




        </div>
        <!-- End Basics Accordion -->

    </div>
</main>


<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//echo '<pre>';
//print_r($faqs);
//echo '</pre>';
?>


<script>
    (function ($) {

//        var allPanels = $('.accordion > dd').hide();
//
//        $('.accordion > dt > a').click(function () {
//            allPanels.slideUp();
//            $(this).parent().next().slideDown();
//            return false;
//        });

    });
</script>
