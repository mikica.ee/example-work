<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Newproduct extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->library(array('session', 'pagination'));
        $this->load->helper(array('url'));
        $this->load->helper(array('form'));
        $this->load->model('products_model');
        $this->load->model('web_products_model');
        $this->load->model('categories_model');
        $this->load->model('cat_model');
        $this->load->model('tag_model');
        $this->load->model('product_relation_model');
        $this->load->model('color_model');
        $this->load->model('newproduct_model');
    }

    public function select_cat_up() {
        $cat = $this->categories_model->select_cat($this->uri->segment(3));
        $cat_id = $cat[0]['parent_id'];
        if ($cat_id > 0) {
            while (true) {
                $upcat = $this->categories_model->select_cat($cat_id);
                $cat_id = $upcat[0]['parent_id'];
                $cat[] = $upcat[0];
                if ($upcat[0]['parent_id'] == 0) {
                    break;
                }
            }
        }
        echo json_encode($cat);
    }

    public function insert_simular() {
        $data['simular_product_p'] = $this->input->post('simular_product_p');
        foreach ($this->input->post('simular_product_s') as $value) {
            $data['simular_product_s'] = $value;
            $this->newproduct_model->insert_simular_rel($data);
        }
    }

    public function listing() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            $this->load->view('newheader');

            $config = array();
            $config["base_url"] = base_url("newproduct/listing");
            $config['reuse_query_string'] = true;
            if (isset($_GET['cat'])) {
                if ($_GET['cat'] != 0) {
                    $sql = $sql = "SELECT COUNT(`web_products_id`) as count FROM `web_products`\n"
                            . "LEFT JOIN `cat_rel` ON cat_rel.cat_rel_p = web_products.web_products_id\n"
                            . "WHERE `cat_rel_c` = '" . $_GET['cat'] . "'" . "\n"
                            . "ORDER BY web_products_id asc \n";
                } else {
                    $sql = "SELECT COUNT(`web_products_id`) as count FROM `web_products` ORDER BY web_products_id asc";
                }
            } else {
                $sql = "SELECT COUNT(`web_products_id`) as count FROM `web_products` ORDER BY web_products_id asc";
            }

            $count = $this->web_products_model->query($sql);
            $count = $count->result_array();
            $config["total_rows"] = $count[0]['count'];
            $config["per_page"] = 50;
            $config["uri_segment"] = 3;
            $config["first_url"] = $config['base_url'] . '?' . http_build_query($_GET);
            $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
            $config['full_tag_close'] = '</ul></nav></div>';
            $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close'] = '</span></li>';
            $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['next_tag_close'] = '<span aria-hidden="true"></span></span></li>';
            $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['prev_tag_close'] = '</span></li>';
            $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['first_tag_close'] = '</span></li>';
            $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['last_tag_close'] = '</span></li>';

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $links = $this->pagination->create_links();

            if (isset($_GET['cat'])) {
                if ($_GET['cat'] != 0) {
                    $sql = $sql = "SELECT * FROM `web_products`\n"
                            . "LEFT JOIN `cat_rel` ON cat_rel.cat_rel_p = web_products.web_products_id\n"
                            . "WHERE `cat_rel_c` = '" . $_GET['cat'] . "'" . "\n"
                            . "ORDER BY web_products_id asc \n"
                            . "LIMIT " . $config["per_page"] . " OFFSET " . $page;
                } else {
                    $sql = "SELECT * FROM `web_products`\n"
                            . "ORDER BY web_products_id asc "
                            . "LIMIT " . $config["per_page"] . " OFFSET " . $page;
                }
            } else {
                $sql = "SELECT * FROM `web_products`\n"
                        . "ORDER BY web_products_id asc "
                        . "LIMIT " . $config["per_page"] . " OFFSET " . $page;
            }

            $res = $this->web_products_model->query($sql);
            $product = $res->result_array();

            $res = $this->cat_model->query("SELECT * FROM `categories` WHERE `cat_id` NOT IN ( SELECT DISTINCT `parent_id` FROM `categories` WHERE `parent_id` != '0')");
            $cat = $res->result_array();

            $sent_data = ["products" => $product, "links" => $links, "cat" => $cat];
            $this->load->view('web/newproduct/listing', $sent_data);
            $this->load->view('newfooter');
        } else {
            redirect('login');
        }
    }

    public function insert() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            $color = $this->color_model->select_all_joined();
            $res = $this->cat_model->query("SELECT * FROM `categories` WHERE `cat_id` NOT IN ( SELECT DISTINCT `parent_id` FROM `categories` WHERE `parent_id` != '0')");
            $cat = $res->result_array();
            $sent_data = ["color" => $color, "cat" => $cat];
            $this->load->view('newheader');
            $this->load->view('web/newproduct/insert', $sent_data);
            $this->load->view('newfooter');
        } else {
            redirect('login');
        }
    }

    public function gallery_update() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            $data['gallery_id'] = $this->input->post('gallery_id');
            $data['gallery_date'] = $this->input->post('gallery_date');
            $data['gallery_alt'] = $this->input->post('gallery_alt');
            if ($this->newproduct_model->update_gallery($data)) {
                echo 'Success';
            } else {
                echo 'Error';
            }
        } else {
            redirect('login');
        }
    }

    public function update() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            $product = $this->web_products_model->select_all_joined(["where" => '`web_products_id`=' . $this->uri->segment(3)]);
            $color = $this->color_model->select_all_joined();
            $catrel = $this->categories_model->select_for_cat($this->uri->segment(3));
            $products = $this->product_relation_model->select_all_joined(["where" => '`product_relation_w`=' . $this->uri->segment(3)]);
            $gallery = $this->newproduct_model->select_gallery($this->uri->segment(3));
            $tags = $this->tag_model->select_from_rel($this->uri->segment(3));
            $sims = $this->newproduct_model->select_from_sim_rel($this->uri->segment(3));
            $res = $this->cat_model->query("SELECT * FROM `categories` WHERE `cat_id` NOT IN ( SELECT DISTINCT `parent_id` FROM `categories` WHERE `parent_id` != '0')");
            $cat = $res->result_array();
            $sent_data = ["color" => $color, "product" => $product[0], "catrel" => $catrel, "products" => $products, "cat" => $cat];
            $sent_data2 = ["gallery" => $gallery];
            $sent_data3 = ["product" => $product[0], "tags" => $tags, "sims" => $sims];
            $this->load->view('newheader');
            $this->load->view('web/newproduct/update', $sent_data);
            $this->load->view('web/newproduct/simular_product', $sent_data3);
            $this->load->view('web/newproduct/gallery', $sent_data2);

            $this->load->view('newfooter');
        } else {
            redirect('login');
        }
    }

    public function update_product() {
//         print_r($_FILES);
        $data['web_products_id'] = $this->input->post('web_products_id');
        $data['web_products_slug'] = $this->input->post('web_products_slug');
        $data['web_products_name'] = $this->input->post('web_products_name');
        $data['web_products_text'] = $this->input->post('web_products_text');
        $data['web_products_cat'] = $this->input->post('web_products_cat');
        $data['tab1'] = $this->input->post('tab1');
        $data['tab2'] = $this->input->post('tab2');
        $data['feat'] = $this->input->post('feat');
        $data['web_keyword'] = $this->input->post('web_keyword');
        $data['web_seo_des'] = strip_tags($this->input->post('web_seo_des'));
        $data['web_price'] = $this->input->post('web_price');
        $data['web_price_old'] = $this->input->post('web_price_old');
        $data['web_type'] = $this->input->post('web_type');
        $data['web_time'] = $this->input->post('web_time');
        if ($this->newproduct_model->update($data)) {

            if (!empty($this->input->post('var'))) {
                foreach ($this->input->post('var') as $varrel) {
                    $var['product_relation_w'] = $data['web_products_id'];
                    $var['product_relation_p'] = $varrel;
                    $this->newproduct_model->insert_var_rel($var);
                }
            }
            if (!empty($this->input->post('cat'))) {
                foreach ($this->input->post('cat') as $catrel) {
                    $cat['cat_rel_p'] = $data['web_products_id'];
                    $cat['cat_rel_c'] = $catrel;
                    $this->categories_model->insert_cat_rel($cat);
                }
            }
            $config['upload_path'] = "./img/products/gallery/" . date("Y/m") . "/";
            if (!is_dir($config['upload_path'])) {
                mkdir($config['upload_path'], 0755, true);
            }
            $config['allowed_types'] = "*";
            $config['max_size'] = "10000";
            $config['max_width'] = "5000";
            $config['max_height'] = "5000";
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            for ($count = 0; $count < count($_FILES["gallery"]["name"]); $count++) {
                $_FILES["file"]["name"] = $_FILES["gallery"]["name"][$count];
                $_FILES["file"]["type"] = $_FILES["gallery"]["type"][$count];
                $_FILES["file"]["tmp_name"] = $_FILES["gallery"]["tmp_name"][$count];
                $_FILES["file"]["error"] = $_FILES["gallery"]["error"][$count];
                $_FILES["file"]["size"] = $_FILES["gallery"]["size"][$count];
                if ($this->upload->do_upload('file')) {
                    $img = $this->upload->data();
                    $gallery['gallery_p'] = $data['web_products_id'];
                    $gallery['gallery_img'] = "img/products/gallery/" . date("Y/m") . "/" . $img['raw_name'] . $img['file_ext'];
//                        if ($count == 0) {
//                            $gallery['gallery_feat'] = 1;
//                            $this->newproduct_model->update(['web_products_id' => $data['web_products_id'], 'web_products_img' => $gallery['gallery_img']]);
//                        } else {
//                            $gallery['gallery_feat'] = 0;
//                        }
                    $gallery['gallery_feat'] = 0;
                    $this->newproduct_model->insert_gallery($gallery);
                }
            }
            echo 'Success';
        } else {
            echo 'Error';
        }
        // print_r($data);
    }

    public function insert_product() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            $data['web_products_slug'] = $this->input->post('web_products_slug');
            $data['web_products_name'] = $this->input->post('web_products_name');
            $data['web_products_text'] = $this->input->post('web_products_text');
            $data['web_products_cat'] = $this->input->post('web_products_cat');
            $data['tab1'] = $this->input->post('tab1');
            $data['tab2'] = $this->input->post('tab2');
            $data['feat'] = $this->input->post('feat');
            $data['web_keyword'] = $this->input->post('web_keyword');
            $data['web_seo_des'] = $this->input->post('web_seo_des');
            $data['web_price'] = $this->input->post('web_price');
            $data['web_price_old'] = $this->input->post('web_price_old');
            $data['web_type'] = $this->input->post('web_type');
            $data['web_time'] = date('Y-m-j H:i:s');
            //print_r($data);
            if ($this->newproduct_model->insert($data)) {
                $insert_id = $this->newproduct_model->db->insert_id();
                foreach ($this->input->post('cat') as $catrel) {
                    $cat['cat_rel_p'] = $insert_id;
                    $cat['cat_rel_c'] = $catrel;
                    $this->categories_model->insert_cat_rel($cat);
                }
                foreach ($this->input->post('var') as $varrel) {
                    $var['product_relation_w'] = $insert_id;
                    $var['product_relation_p'] = $varrel;
                    $this->newproduct_model->insert_var_rel($var);
                }
                if (!empty($this->input->post('simular_product_s'))) {
                    foreach ($this->input->post('simular_product_s') as $value) {
                        $datas['simular_product_p'] = $insert_id;
                        $datas['simular_product_s'] = $value;
                        $this->newproduct_model->insert_simular_rel($datas);
                    }
                }
                if (!empty($this->input->post('tag_rel_t'))) {
                    foreach ($this->input->post('tag_rel_t') as $value) {
                        //echo "<br> tag: " . $value;
                        $datat['tag_rel_p'] = $insert_id;
                        $datat['tag_rel_t'] = $value;
                        $this->tag_model->insert_rel($datat);
                    }
                }
                $config['upload_path'] = "./img/products/gallery/" . date("Y/m") . "/";
                if (!is_dir($config['upload_path'])) {
                    mkdir($config['upload_path'], 0755, true);
                }
                $config['allowed_types'] = "*";
                $config['max_size'] = "10000";
                $config['max_width'] = "5000";
                $config['max_height'] = "5000";
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                for ($count = 0; $count < count($_FILES["gallery"]["name"]); $count++) {
                    $_FILES["file"]["name"] = $_FILES["gallery"]["name"][$count];
                    $_FILES["file"]["type"] = $_FILES["gallery"]["type"][$count];
                    $_FILES["file"]["tmp_name"] = $_FILES["gallery"]["tmp_name"][$count];
                    $_FILES["file"]["error"] = $_FILES["gallery"]["error"][$count];
                    $_FILES["file"]["size"] = $_FILES["gallery"]["size"][$count];
                    if ($this->upload->do_upload('file')) {
                        $img = $this->upload->data();
                        $gallery['gallery_p'] = $insert_id;
                        $gallery['gallery_img'] = "img/products/gallery/" . date("Y/m") . "/" . $img['raw_name'] . $img['file_ext'];
                        if ($count == 0) {
                            $gallery['gallery_feat'] = 1;
                            $this->newproduct_model->update(['web_products_id' => $insert_id, 'web_products_img' => $gallery['gallery_img']]);
                        } else {
                            $gallery['gallery_feat'] = 0;
                        }
                        $this->newproduct_model->insert_gallery($gallery);
                    }
                }
                echo "Success";
            } else {
                echo 'Error';
            }
        } else {
            redirect('login');
        }
    }

    public function slugcheck() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            if (!empty($_POST["keyword"])) {
                $product = $this->web_products_model->select_all_joined(["where" => '`web_products_slug`=' . "'" . $_POST['keyword'] . "'"]);
                if (!empty($product)) {
                    echo '1';
                } else {
                    echo '0';
                }
            }
        } else {
            redirect('login');
        }
    }

    public function selectproduct() {
        //  echo 'nesto';
        if (!empty($_POST["searchTerm"])) {
            //$cats = $this->categories_model->select($_POST["searchTerm"]);
            $res = $this->products_model->query("SELECT * FROM products WHERE product like '%" . $_POST["searchTerm"] . "%' ORDER BY product asc LIMIT 5");
            $res = $res->result_array();
            if (!empty($res)) {
                foreach ($res as $var) {
                    $data[] = array("id" => $var["product_id"], "text" => $var['product']);
                }
                echo json_encode($data);
            }
        }
    }

    public function del_rel_sim() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            if ($this->newproduct_model->del_rel_sim($this->input->post('simular_product_id'))) {
                echo 'Success';
            } else {
                echo 'Error';
            }
        } else {
            redirect('login');
        }
    }

    public function deleteimg() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {

            if ($this->newproduct_model->del_img($this->input->post('gallery_id'))) {
                unlink($this->input->post('imglink'));
                echo 'успешно избришана';
            } else {
                echo 'Настана грешка ве молиме обидете се повторно';
            }
            echo $this->input->post('gallery_id') . $this->input->post('imglink');
        } else {
            redirect('login');
        }
    }

    public function feat() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            if ($this->newproduct_model->feat($this->input->post('gallery_p'), $this->input->post('gallery_id'))) {
                $this->newproduct_model->update(['web_products_id' => $this->input->post('gallery_p'), 'web_products_img' => $this->input->post('imglink')]);
                echo "Success";
            } else {
                echo 'Error';
            }
        }
    }

    public function selectproduct_forsim() {
        //  echo 'nesto';
        if (!empty($_POST["searchTerm"])) {
            //$cats = $this->categories_model->select($_POST["searchTerm"]);
            $res = $this->products_model->query("SELECT * FROM web_products WHERE web_products_name like '%" . $_POST["searchTerm"] . "%' ORDER BY web_products_name  asc");
            $res = $res->result_array();
            if (!empty($res)) {
                foreach ($res as $var) {
                    $data[] = array("id" => $var["web_products_id"], "text" => $var['web_products_name']);
                }
                echo json_encode($data);
            }
        }
    }

}
