<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ininvoice extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->library(array('session','pagination'));
        $this->load->helper(array('url'));
        $this->load->helper(array('form'));
        $this->load->model('ininvoice_model');
        $this->load->model('products_model');
    }

    public function select_ininvoice() {
        //  echo 'nesto';
        if (!empty($_POST["searchTerm"])) {
            $res = $this->products_model->query("SELECT * FROM in_invoice WHERE in_invoice_no like '%" . $_POST["searchTerm"] . "%' ORDER BY in_invoice_no  asc");
            $res = $res->result_array();
            if (!empty($res)) {
                foreach ($res as $var) {
                    $data[] = array("id" => $var["in_invoice_id"], "text" => $var['in_invoice_no']);
                }
                echo json_encode($data);
            }
        }
    }

    public function insert() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            
            $config = array();
            $config["base_url"] = base_url("ininvoice/insert");
            $config['reuse_query_string'] = true;
            $config["total_rows"] = $this->ininvoice_model->count();
            $config["per_page"] = 8;
            $config["uri_segment"] = 3;
            $config["first_url"] = $config['base_url'] . '?' . http_build_query($_GET);
            $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
            $config['full_tag_close'] = '</ul></nav></div>';
            $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close'] = '</span></li>';
            $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['next_tag_close'] = '<span aria-hidden="true"></span></span></li>';
            $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['prev_tag_close'] = '</span></li>';
            $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['first_tag_close'] = '</span></li>';
            $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['last_tag_close'] = '</span></li>';

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $sent_data['links'] = $this->pagination->create_links();
            $sent_data['ininvoices'] = $this->ininvoice_model->select($config["per_page"],$page);
            $this->load->view('newheader');
            $this->load->view('ininvoice/ininvoice', $sent_data);
            $this->load->view('newfooter');
        } else {
            redirect('login');
        }
    }
    
    

    public function insert_ininvoice() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            $data['in_invoice_no'] = $this->input->post('in_invoice_no');
            if ($this->ininvoice_model->insert($data)) {
                echo "Success";
            } else {
                echo 'Error';
            }
        } else {
            redirect('login');
        }
    }


}
