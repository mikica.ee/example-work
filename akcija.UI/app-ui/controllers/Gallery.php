<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('url'));
        $this->load->helper(array('form'));
        $this->load->model('gallery_model');
        $this->load->model('products_model');
    }

    public function index() {
        $this->load->view('header');
        $this->load->view('footer');
    }

    public function delete() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            $data = array('gallery_id' => $_POST["gallery_id"]);
            if ($this->gallery_model->delete($data)) {
                unlink($_POST["imglink"]);
                echo 'успешно избришана';
            } else {
                 echo 'Настана грешка ве молиме обидете се повторно';
            }
        } else {
            redirect('login');
        }
    }

    public function product() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            $gallery = $this->gallery_model->select_all_joined(["where" => '`gallery_product_id`=' . $this->uri->segment(3)]);
            $product = $this->products_model->select_all_joined(["where" => '`product_id`=' . $this->uri->segment(3)]);
            $this->load->view('header');
            $sent_data = ["gallery" => $gallery, "product" => $product[0]];
            $this->load->view('products/gallery', $sent_data);
            $this->load->view('footer');
        } else {
            redirect('login');
        }
    }

    public function addimg() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            if (!empty($_POST['gallery_product_id'])) {
                $config['upload_path'] = "./img/gallery/" . date("Y/m") . "/";
                if (!is_dir($config['upload_path'])) {
                    mkdir($config['upload_path'], 0755, true);
                }
                $config['allowed_types'] = "*";
                $config['max_size'] = "10000";
                $config['max_width'] = "5000";
                $config['max_height'] = "5000";
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload()) {

                    echo $this->upload->display_errors();
                } else {
                    $finfo = $this->upload->data();
                    $data['gallery_id'] = 0;
                    $data['gallery_product_id'] = $_POST['gallery_product_id'];
                    $data['gallery_folder'] = date("Y/m");
                    $data['gallery_file'] = $finfo['raw_name'] . $finfo['file_ext'];
                    $this->gallery_model->insert($data);
                }
                redirect('gallery/product/' . $_POST['gallery_product_id']);
            }
        } else {
            redirect('login');
        }
    }

    public function addimgupdate() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            if (!empty($_POST['gallery_product_id'])) {
                $config['upload_path'] = "./img/gallery/" . date("Y/m") . "/";
                if (!is_dir($config['upload_path'])) {
                    mkdir($config['upload_path'], 0755, true);
                }
                $config['allowed_types'] = "*";
                $config['max_size'] = "10000";
                $config['max_width'] = "5000";
                $config['max_height'] = "5000";
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload()) {

                    echo $this->upload->display_errors();
                } else {
                    $finfo = $this->upload->data();
                    $data['gallery_id'] = 0;
                    $data['gallery_product_id'] = $_POST['gallery_product_id'];
                    $data['gallery_folder'] = date("Y/m");
                    $data['gallery_file'] = $finfo['raw_name'] . $finfo['file_ext'];
                    $this->gallery_model->insert($data);
                }
                redirect('updateproduct?product=' . $_POST['gallery_product_id']);
            }
        } else {
            redirect('login');
        }
    }

}
