<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Brand extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->library(array('session', 'pagination'));
        $this->load->helper(array('url'));
        $this->load->helper(array('form'));
        $this->load->model(array('brand_model', 'products_model'));
    }

    public function insert() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {

            $config = array();
            $config["base_url"] = base_url("brand/insert");
            $config['reuse_query_string'] = true;
            $config["total_rows"] = $this->brand_model->count();
            $config["per_page"] = 20;
            $config["uri_segment"] = 3;
            $config["first_url"] = $config['base_url'] . '?' . http_build_query($_GET);
            $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination">';
            $config['full_tag_close'] = '</ul></nav></div>';
            $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close'] = '</span></li>';
            $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['next_tag_close'] = '<span aria-hidden="true"></span></span></li>';
            $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['prev_tag_close'] = '</span></li>';
            $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['first_tag_close'] = '</span></li>';
            $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
            $config['last_tag_close'] = '</span></li>';

            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $sent_data['links'] = $this->pagination->create_links();


            $sent_data['brands'] = $this->brand_model->select($config["per_page"], $page);
            $this->load->view('newheader');
            $this->load->view('brand/brand', $sent_data);
            $this->load->view('newfooter');
        } else {
            redirect('login');
        }
    }

    public function brandcheck() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            if (!empty($_POST["keyword"])) {
                $brand = $this->brand_model->select_from_name($_POST["keyword"]);
                if (!empty($brand)) {
                    echo '1';
                } else {
                    echo '0';
                }
            }
        } else {
            redirect('login');
        }
    }

    public function insert_brand() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            $data['brand_name'] = $this->input->post('brand_name');
            if ($this->brand_model->insert($data)) {
                echo "Success";
            } else {
                echo 'Error';
            }
        } else {
            redirect('login');
        }
    }

    public function selectbrand() {
        //  echo 'nesto';
        if (!empty($_POST["searchTerm"])) {
            //$cats = $this->categories_model->select($_POST["searchTerm"]);
            $res = $this->products_model->query("SELECT * FROM brand WHERE brand_name like '" . $_POST["searchTerm"] . "%' ORDER BY brand_name asc LIMIT 5");
            $res = $res->result_array();
            if (!empty($res)) {
                foreach ($res as $var) {
                    $data[] = array("id" => $var["brand_id"], "text" => $var['brand_name']);
                }
                echo json_encode($data);
            }
        }
    }

    public function update() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            $sent_data['brand'] = $this->brand_model->select_from_id($this->uri->segment(3));
            $this->load->view('newheader');
            $this->load->view('brand/update', $sent_data);
            $this->load->view('newfooter');
        } else {
            redirect('login');
        }
    }

    public function update_brand() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            $data['brand_name'] = $this->input->post('brand_name');
            $data['brand_id'] = $this->input->post('brand_id');
            if ($this->brand_model->update($data)) {
                echo "Success";
            } else {
                echo 'Error';
            }
        } else {
            redirect('login');
        }
    }

}
