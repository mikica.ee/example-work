<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('url'));
        $this->load->helper('form');
        $this->load->model(array('home_model', 'feat_model'));
    }

    public function home() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
//            $sent_data['homes'] = $this->home_model->select_type($this->uri->segment(3));
            $sent_data['home1'] = $this->home_model->select_from_id(1);
            $sent_data['home2'] = $this->home_model->select_from_id(2);
            $sent_data['home3'] = $this->home_model->select_from_id(3);
            $sent_data['home4'] = $this->home_model->select_from_id(4);
            $sent_data['home5'] = $this->home_model->select_from_id(5);
            $sent_data['home6'] = $this->home_model->select_from_id(6);
            $sent_data['home7'] = $this->home_model->select_from_id(7);
            $sent_data['home8'] = $this->home_model->select_from_id(8);
            $sent_data['feats'] = $this->feat_model->select_type($this->uri->segment(3));
            $this->load->view('newheader');
            $this->load->view('home/home', $sent_data);
            $this->load->view('newfooter');
        } else {
            redirect('login');
        }
    }

    public function insert() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            $data['home_id'] = $this->input->post('home_id');
            $data['home_type'] = $this->input->post('home_type');
            $data['home_name'] = $this->input->post('home_name');
            $data['home_btn'] = $this->input->post('home_btn');
            $data['home_url'] = $this->input->post('home_url');
            $data['home_img'] = $this->input->post('home_img');
            $config['upload_path'] = "./img/home/" . date("Y/m") . "/";
            if (!is_dir($config['upload_path'])) {
                mkdir($config['upload_path'], 0755, true);
            }
            $config['allowed_types'] = "*";
            $config['max_size'] = "10000";
            $config['max_width'] = "5000";
            $config['max_height'] = "5000";
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            for ($count = 0; $count < count($_FILES["gallery"]["name"]); $count++) {
                $_FILES["file"]["name"] = $_FILES["gallery"]["name"][$count];
                $_FILES["file"]["type"] = $_FILES["gallery"]["type"][$count];
                $_FILES["file"]["tmp_name"] = $_FILES["gallery"]["tmp_name"][$count];
                $_FILES["file"]["error"] = $_FILES["gallery"]["error"][$count];
                $_FILES["file"]["size"] = $_FILES["gallery"]["size"][$count];
                if ($this->upload->do_upload('file')) {
                    $img = $this->upload->data();
                    $data['home_img'] = "img/home/" . date("Y/m") . "/" . $img['raw_name'] . $img['file_ext'];
                }
            }
            if ($this->home_model->update($data)) {
                echo "Success";
            } else {
                echo 'Error';
            }
        } else {
            redirect('login');
        }
    }

    public function delete() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            if (!empty($_POST['home_id'])) {
                $data = array('home_id' => $_POST["home_id"]);
                $this->home_model->delete($data);

                echo 'Success';
            } else {
                echo 'Error';
            }
        } else {
            redirect('login');
        }
    }

    public function update() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            $data['home_id'] = $this->input->post('home_id');
            $data['home_url'] = $this->input->post('home_url');
            $data['home_name'] = $this->input->post('home_name');
            $data['home_type'] = $this->input->post('home_type');
            $data['home_img'] = $this->input->post('home_img');
            $data['home_time'] = $this->input->post('home_time');
            if ($this->home_model->update($data)) {
                echo "Success";
            } else {
                echo 'Error';
            }
        } else {
            redirect('login');
        }
    }

    public function insertfeat() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {

            $data['feat_type'] = $this->input->post('feat_type');
            $data['feat_time'] = $this->input->post('feat_timei');
            //print_r($_POST);
            foreach ($this->input->post('feat_products') as $value) {
                $data['feat_product'] = $value;
                $this->feat_model->insert($data);
            }
            echo 'Success';
        } else {
            redirect('login');
        }
    }

    public function deletefeat() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            if (!empty($_POST['feat_id'])) {
                $data = array('feat_id' => $_POST["feat_id"]);
                if ($this->feat_model->delete($_POST["feat_id"])) {
                    echo 'Success';
                } else {
                    echo 'Error';
                }
            } else {
                echo 'Error';
            }
        } else {
            redirect('login');
        }
    }

    public function updatefeat() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            $data['feat_product'] = $this->input->post('feat_product');
            $data['feat_type'] = $this->input->post('feat_typeu');
            $data['feat_id'] = $this->input->post('feat_id');
            $data['feat_time'] = $this->input->post('feattime');
            if ($this->feat_model->update($data)) {
                echo "Success";
            } else {
                echo 'Error';
            }
        } else {
            redirect('login');
        }
    }

}
