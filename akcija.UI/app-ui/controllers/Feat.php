<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Feat extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('url'));
        $this->load->helper('form');;
        $this->load->model('feat_model');
    }

     public function feat() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            $sent_data['feats'] = $this->feat_model->select_type($this->uri->segment(3));
            $this->load->view('newheader');
            $this->load->view('feat/feat', $sent_data);
            $this->load->view('newfooter');
        } else {
            redirect('login');
        }
    }
    public function insert() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            $data['feat_product'] = $this->input->post('feat_product');
            $data['feat_type'] = $this->input->post('feat_type');
            $data['feat_time'] = date('Y-m-j H:i:s');
            if ($this->feat_model->insert($data)) {
                echo "Success";
            } else {
                echo 'Error';
            }
        } else {
            redirect('login');
        }
    }
   

   

    public function delete() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            if (!empty($_POST['feat_id'])) {
                $data = array('feat_id' => $_POST["feat_id"]);
                $this->feat_model->delete($data);

                echo 'Success';
            } else {
                echo 'Error';
            }
        } else {
            redirect('login');
        }
    }
    public function update() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            $data['feat_product'] = $this->input->post('feat_product');
            $data['feat_type'] = $this->input->post('feat_type');
            $data['feat_id'] = $this->input->post('feat_id');
            $data['feat_time'] = $this->input->post('feat_time');
            if ($this->feat_model->update($data)) {
                echo "Success";
            } else {
                echo 'Error';
            }
        } else {
            redirect('login');
        }
    }
    

}
