<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('url'));
        $this->load->helper('form');
        $this->load->model('menu_model');
        $this->load->model('categories_model');
        $this->load->model('prom_cat_model');
    }

    public function index() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            $data = $this->menu_model->get_menu();
            $menu = $this->objectToArray($data);
            $list = $this->menulist($menu);
            $dropdown = $this->menudrop($menu);
            $datac = $this->categories_model->get_categories();
            $categories = $this->objectToArray($datac);
            $dropdownc = $this->catdrop($categories);
            
            $dropdownp = $this->prom_cat_model->select_all_joined([]);
            
            $this->load->view('newheader');
            $sent_data = ["dropdown" => $dropdown, "list" => $list, "dropdownc" => $dropdownc, "dropdownp" => $dropdownp];
            $this->load->view('web/menu/menu', $sent_data);
            $this->load->view('newfooter');
        } else {
            redirect('login');
        }
    }

    public function delete() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            echo $_POST['menu_id'];
            $data = array('menu_parent_id' => $_POST["menu_id"]);
            $this->menu_model->delete_where($data);
            $data = array('menu_id' => $_POST["menu_id"]);
            $this->menu_model->delete($data);
        } else {
            redirect('login');
        }
    }

    public function addmenu() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            if (!empty($_POST['menu_name']) AND ! empty($_POST['menu_link'])) {
                $data = $_POST;
                $data['menu_id'] = 0;
                echo $data['menu_parent_id'];
                if ($data['menu_parent_id'] == 0) {
                    $data['menu_parent_id'] = " ";
                }
                $this->menu_model->insert($data);
                //$insert_id = $this->menu_model->db->insert_id();
                echo 'setirani se ';
            } else {
                echo 'Nemate vneseno nekoja od potrebnite vrednosti';
            }
        } else {
            redirect('login');
        }
    }

    public function update() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            $cat = $this->cat_model->select_all_joined(["where" => '`cat_id`=' . $this->uri->segment(3)]);
            $cat = $cat['0'];
            $sent_data = ["cat" => $cat];
            $this->load->view('header');
            $this->load->view('web/cat/updatecat', $sent_data);
            $this->load->view('footer');
        } else {
            redirect('login');
        }
    }

    private function objectToArray($data) {
        if (is_array($data) || is_object($data)) {
            $result = array();
            foreach ($data as $key => $value) {
                $result[$key] = $this->objectToArray($value);
            }
            return $result;
        } else {
            return $data;
        }
    }

    private function menulist($element) {
        $html = "";
        foreach ($element as $key => $value) {
            $html .= "<li>";
            $html .= $value['menu_id'] . " " . $value['menu_name'] . '->' . $value['menu_link'] . ' ';
            $html .= '<a href="javascript:;" id = "delete_menu" menu_id = "' . $value['menu_id'] . '"><span class="fas fa-trash" style="color:red;"></span></a>';
            if (!empty($value['sub'])) {
                $html .= "<ul>";
                $html .= $this->menulist($element[$key]['sub']);
                $html .= "</ul>";
            }
            $html .= "</li>";
        }
        return $html;
    }

    private function menudrop($element, $space = '') {
        $html = "";
        foreach ($element as $key => $value) {
            $html .= '<option value="' . $value['menu_id'] . '">' . $space . $value['menu_name'] . "</optopn>";
            if (!empty($value['sub'])) {
                //$space .= "->";
                $html .= $this->menudrop($element[$key]['sub'], $space);
            }
        }
        return $html;
    }

    private function catdrop($element, $space = '') {
        $html = "";
        foreach ($element as $key => $value) {
            $html .= '<option value="' . $value['cat_slug'] . '">' . $space . $value['cat_name'] . "->" . $value['cat_slug'] . "</optopn>";
            if (!empty($value['sub'])) {
                $html .= $this->catdrop($element[$key]['sub'], $space);
            }
        }
        return $html;
    }

}
