<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 * 
 * @extends CI_Controller
 */
class User extends CI_Controller {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('url'));
        $this->load->model('user_model');
        $this->load->model('users_model');
    }

    public function index() {

        if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            $this->load->view('newheader');
            //$this->load->view('user/login/login');
            $this->load->view('newfooter');
        } else {
            redirect('login');
        }
    }

    public function kom() {
        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            if (!empty($_POST["searchTerm"])) {
                if (strlen($_POST["searchTerm"]) > 2) {
                    $res = $this->users_model->query("SELECT * FROM `users` WHERE `type` = 4 AND `name` like '%" . $_POST["searchTerm"] . "%'");
                    $res = $res->result_array();
                    if (!empty($res)) {
                        foreach ($res as $city) {
                            $data[] = array("id" => $city["id"], "text" => $city['name']);
                        }
                        //$data[] = array("id" => 'firma_id', "text" => "firma ime");
                        echo json_encode($data);
                    }
                }
            }
        }
    }

    public function listausers() {
        if (isset($_SESSION['username']) && $_SESSION['type'] == '1' && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {


            $this->load->view('newheader');
            $config = array();
            $config["base_url"] = base_url("user/listausers");
            $config['reuse_query_string'] = true;
            if (isset($_GET['user'])) {
                if ($_GET['user'] != 0) {
                    $res = $this->users_model->select_all_joined(["where" => "type = " . $_GET['user']]);
                } else {
                    $res = $this->users_model->select_all_joined();
                }
            } else {
                $res = $this->users_model->select_all_joined();
            }
            $sent_data = ["res" => $res];
            $this->load->view("user/listusers", $sent_data);
            $this->load->view('newfooter');
        } else {
            redirect('login');
        }
    }

    public function select_from_list() {
        if (!empty($_POST["searchTerm"])) {
            $res = $this->users_model->query("SELECT * FROM users WHERE name like '%" . $_POST["searchTerm"] . "%' ORDER BY name  asc");
            $res = $res->result_array();
            if (!empty($res)) {
                foreach ($res as $var) {
                    $data[] = array("id" => $var["id"], "text" => $var['name']);
                }
                echo json_encode($data);
            }
        }
    }

//    public function update() {
//        if (isset($_SESSION['username']) && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
//            $data['id'] = $this->input->post('user_id');
//            $data['username'] = $this->input->post('username');
//            $data['name'] = $this->input->post('name');
//            $data['email'] = $this->input->post('email');
//            $data['type'] = $this->input->post('type');
//            if ($this->users_model->update($data)) {
//                echo "Податоците се успешно променети";
//            } else {
//                echo 'Настаната е грешка, ве молиме обидете се повторно';
//            }
//        } else {
//            redirect('login');
//        }
//    }

    public function update() {
        if (isset($_SESSION['username']) && $_SESSION['type'] == '1' && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            if (isset($_POST['username'])) {
                $res = $this->users_model->select_all_joined(["where" => '`id`=' . $_POST['user_id']]);
                $res = $res['0'];
                $res['id'] = $_POST['user_id'];
                $res['username'] = $_POST['username'];
                $res['name'] = $_POST['name'];
                $res['email'] = $_POST['email'];
                $res['type'] = $_POST['type'];
                if (isset($_POST['password']) && $_POST['password'] != "") {
                    $res['password'] = password_hash($_POST['password'], PASSWORD_BCRYPT);
                }
                if ($this->users_model->update($res)) {
                    echo 'Податоците се успешно променети';
                } else {
                    echo 'Настаната е грешка, ве молиме обидете се повторно';
                }
            }
        } else {
            redirect('login');
        }
    }

    public function updateuser() {
        if (isset($_SESSION['username']) && $_SESSION['type'] == '1' && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            if (isset($_GET['user'])) {
                $res = $_GET['user'];
                $res = $this->users_model->select_all_joined(["where" => '`id`=' . $res]);
                $res = $res['0'];
                $alert = "";
                $sent_data = ["res" => $res, "alert" => $alert];
                $this->load->view('newheader');
                $this->load->view("user/updateuser", $sent_data);
                $this->load->view('newfooter');
            }
        } else {
            redirect('login');
        }
    }

    /**
     * register function.
     * 
     * @access public
     * @return void
     */
    public function register() {
        if (isset($_SESSION['username']) && $_SESSION['type'] == '1' && $_SESSION['logged_in'] === true && isset($_SESSION['type'])) {
            // create the data object
            $data = new stdClass();

            // load form helper and validation library
            $this->load->helper('form');
            $this->load->library('form_validation');

            // set validation rules
            $this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_numeric|min_length[4]|is_unique[users.username]', array('is_unique' => 'This username already exists. Please choose another one.'));
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
            $this->form_validation->set_rules('password_confirm', 'Confirm Password', 'trim|required|min_length[6]|matches[password]');

            if ($this->form_validation->run() === false) {

                // validation not ok, send validation errors to the view
                $this->load->view('newheader');
                $this->load->view('user/register/register', $data);
                $this->load->view('newfooter');
            } else {

                // set variables from the form
                $username = $this->input->post('username');
                $name = $this->input->post('name');
                $email = $this->input->post('email');
                $password = $this->input->post('password');
                $type = $this->input->post('type');

                if ($this->user_model->create_user($username, $name, $email, $password, $type)) {

                    // user creation ok
                    $this->load->view('newheader');
                    $this->load->view('user/register/register_success', $data);
                    $this->load->view('newfooter');
                } else {

                    // user creation failed, this should never happen
                    $data->error = 'There was a problem creating your new account. Please try again.';

                    // send error to the view
                    $this->load->view('header');
                    $this->load->view('user/register/register', $data);
                    $this->load->view('footer');
                }
            }
        } else {
            redirect('login');
        }
    }

    /**
     * login function.
     * 
     * @access public
     * @return void
     */
    public function login() {

        // create the data object
        $data = new stdClass();

        // load form helper and validation library
        $this->load->helper('form');
        $this->load->library('form_validation');

        // set validation rules
        $this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == false) {

            // validation not ok, send validation errors to the view
            $this->load->view('newheader');
            $this->load->view('user/login/login');
            $this->load->view('newfooter');
        } else {

            // set variables from the form
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            if ($this->user_model->resolve_user_login($username, $password)) {

                $user_id = $this->user_model->get_user_id_from_username($username);
                $user = $this->user_model->get_user($user_id);

                // set session user datas
                $_SESSION['user_id'] = (int) $user->id;
                $_SESSION['username'] = (string) $user->username;
                $_SESSION['logged_in'] = (bool) true;
                $_SESSION['type'] = (int) $user->type;
                $_SESSION['is_confirmed'] = (bool) $user->is_confirmed;
                $_SESSION['is_admin'] = (bool) $user->is_admin;

                // user login ok
                $this->load->view('newheader');
                $this->load->view('user/login/login_success', $data);
                $this->load->view('newfooter');
            } else {

                // login failed
                $data->error = 'Wrong username or password.';

                // send error to the view
                $this->load->view('newheader');
                $this->load->view('user/login/login', $data);
                $this->load->view('newfooter');
            }
        }
    }

    /**
     * logout function.
     * 
     * @access public
     * @return void
     */
    public function logout() {

        // create the data object
        $data = new stdClass();

        if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) {

            // remove session datas
            foreach ($_SESSION as $key => $value) {
                unset($_SESSION[$key]);
            }

            // user logout ok
            $this->load->view('newheader');
            $this->load->view('user/logout/logout_success', $data);
            $this->load->view('newfooter');
        } else {

            // there user was not logged in, we cannot logged him out,
            // redirect him to site root
            redirect('/');
        }
    }

}
