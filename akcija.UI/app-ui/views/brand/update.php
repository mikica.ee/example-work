
<?php
//echo '<pre>';
//print_r($brand);
//echo '</pre>';
?>
<div class="dashboard-wrapper">
    <div class="splash-container">
        <div class="card ">
            <div class="card-header text-center">
                <a class="navbar-brand" href="<?= site_url() ?>">Brand </a>
            </div>
            <div class="card-body">
                <form method="post" enctype="multipart/form-data" id="updatebrand" action="javascript:;" name="updatebrand">
                <fieldset>
                    <div class="form-group">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="product">Бренд</label>
                                <input class="form-control" id="brand_id" name="brand_name" type="text" value="<?= $brand[0]['brand_name'] ?>">
                                <input type="hidden" value="<?= $brand[0]['brand_id'] ?>" id="brand_id" name="brand_id">
                            </div>
                            <div class="form-group col-md-12">
                                <button type="submit" class="btn btn-primary btn-block">Ажурирај го Брендот</button>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
            </div>
        </div>
    </div>
</div> 
<script>
    $(document).ready(function ($) {
        $('form[id="updatebrand"]').validate({
            rules: {
                brand_name: 'required',
            },
            messages: {
                brand_name: 'This value is required.',
            },
            errorPlacement: function (label, element) {
                if (element.is("textarea")) {
                    label.insertAfter(element.next());
                } else {
                    label.insertAfter(element)
                }
            },
            submitHandler: function (form) {
                var brand = new FormData($("#updatebrand")[0]);
                //alert(service);
                $.ajax({
                    url: '<?= site_url('brand/update_brand') ?>',
                    type: 'POST',
                    data: brand,
                    beforeSend: function () {
                        // Show image container
                    },
                    success: function (data) {
                        alert(data);
                        location.reload();
                    },
                    complete: function (data) {
                        // Hide image container
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
        });

    });
</script>