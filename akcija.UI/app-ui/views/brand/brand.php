<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header text-center">
                    <a class="navbar-brand" href="<?= site_url() ?>">Brand</a>
                </div>
            </div>
        </div>
        <form method="post" enctype="multipart/form-data" id="insertbrand" action="javascript:;" name="insertbrand">
            <div class="row">
                <div class="col-xl-7 col-lg-6 col-md-12 col-sm-12 col-12 offset-lg-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="brand_name"><b>Brands</b></label>
                                    <input class="form-control" id="brand_name" name="brand_name" type="text" autocomplete="off">
                                </div>
                            </div>
                            <br>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <p class="text-">
                                    <button type="submit" id="btnSubmit" class="btn btn-primary btn-block">Додади</button>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="col-xl-7 col-lg-6 col-md-12 col-sm-12 col-12 offset-lg-3">
            <div class="card">
                <h5 class="card-header"><b>Листа на Брендови</b></h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                            <nav aria-label="Page navigation example">
                                <?= $links ?>
                            </nav>
                        </div>
                        <hr>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered first">
                                <tbody>
                                    <tr class="danger">
                                        <td><h5><b>No.</b></h5></td>
                                        <td><h5><b>id</b></h5></td>
                                        <td><h5><b>Brand</b></h5></td>
                                        <td><h5><b></b></h5></td>
                                    </tr>
                                    <?php
                                    $cs = 0;
                                    $br = 0;
                                    foreach ($brands as $brand) {
                                        if ($cs) {
                                            $css = ' class="info"';
                                            $cs = 0;
                                        } else {
                                            $css = ' class="active"';
                                            $cs = 1;
                                        }
                                        ?>

                                        <tr<?php echo $css; ?>>
                                            <td><?= ++$br; ?></td>
                                            <td><?= $brand['brand_id']; ?></td>
                                            <td><?= $brand['brand_name']; ?></td>
                                            <td><a href="<?= site_url('brand/update/' . $brand['brand_id']) ?>"><i class="fas fa-edit"></i></a></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>    
                    </div>
                    <hr>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                        <nav aria-label="Page navigation example">
                            <?= $links ?>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function ($) {
        $("#brand_name").keyup(function () {
            $.ajax({
                type: "POST",
                url: "<?= site_url('brand/brandcheck') ?>",
                data: 'keyword=' + $("#brand_name").val(),
                beforeSend: function () {
                    $("#brand_name").css("background", "#FFF url(<?= site_url('LoaderIcon.gif') ?>) no-repeat 165px");
                },
                success: function (data) {
                    //alert(data);
                    if (data == 0) {
                        $("#brand_name").css("background", "#cefdce");
                        $("#btnSubmit").attr("disabled", false);
                    } else {
                        $("#brand_name").css("background", "#f44242");
                        $('#btnSubmit').attr("disabled", true);
                    }
                }
            });
        });
        $('form[id="insertbrand"]').validate({
            rules: {
                brand_name: 'required',
            },
            messages: {
                brand_name: 'This value is required.',
            },
            errorPlacement: function (label, element) {
                if (element.is("textarea")) {
                    label.insertAfter(element.next());
                } else {
                    label.insertAfter(element)
                }
            },
            submitHandler: function (form) {
                var brand = new FormData($("#insertbrand")[0]);
                //alert(service);
                $.ajax({
                    url: '<?= site_url('brand/insert_brand') ?>',
                    type: 'POST',
                    data: brand,
                    beforeSend: function () {
                        // Show image container
                    },
                    success: function (data) {
                        alert(data);
                        location.reload();
                    },
                    complete: function (data) {
                        // Hide image container
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
        });

    });
</script>