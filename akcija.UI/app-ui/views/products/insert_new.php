<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<script>
    $(document).ready(function () {
        $("#kom").select2({
            width: '100%',
            minimumInputLength: 1,
            placeholder: "Изберете Коминтент",
            allowClear: true,
            ajax: {
                url: "<?= site_url('user/kom') ?>",
                type: "post",
                dataType: 'json',
                delay: 0,
                data: function (params) {
                    return {
                        searchTerm: params.term, // search term
                        //web_city_id: $("#web_city").val()
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: false
            }
        });
        //$("#insertitem").hide();
        $("#product").keyup(function (e) {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('selectproduct') ?>",
                data: 'keyword=' + $(this).val(),
                beforeSend: function () {
                    $("#product").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    $("#product").css("background-color", "#FFF");
                },
                success: function (data) {
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(data);
                    $("#search-box").css("background", "#FFF");
                }
            });
        });
    });

    function selectProductid(val) {
        $("#" + val).css("background-color", "yellow");
    }
</script>
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header text-center">
                    <h2 class="pageheader-title"><b>Внеси нов Продукт</b></h2>

                </div>
            </div>
        </div>
        <form method="post" enctype="multipart/form-data" id="insertproduct" action="javascript:;" name="insertproduct">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header"><b>Генералии</b></h5>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="kom"><b>Коминтент</b></label>
                                    <select id='kom' name="kom" class="form-control" data-width="100%">
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <label for="product"><b>Име на Производ</b></label>
                                    <input class="form-control" id="product" name="product" type="text" autocomplete="off" required>
                                    <div id="suggesstion-box"></div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                    <div class="form-group">
                                        <label for="color"><b>Боја</b></label>
                                        <select class='form-control'name='color'>
                                            <?php foreach ($color as $col) { ?>
                                                <option value="<?= $col['color_id'] ?>"><?= $col['color_name'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                    <label for="storage"><b>Големина</b></label>
                                    <input class="form-control" id="size" name="size" type="text">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                    <div class="form-group">
                                        <label for="price"><b>Цена</b></label>
                                        <input type="text" class="form-control" id="price" name="price">
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                    <div class="form-group">
                                        <label for="pricen"><b>Набавна</b></label>
                                        <input type="text" class="form-control" id="pricen" name="pricen">
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                    <label for="product_vat"><b>ДДВ</b></label>
                                    <select class='form-control'name='product_vat'>
                                        <option value="18">18%</option>
                                        <option value="5">5%</option>
                                    </select> 
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                    <label for="gallery_product_id"><b>Додади нова слика за Продуктот</b></label>
                                    <div class="custom-file mb-3">
                                        <input class="custom-file-input" type="file" id="gallery" name="gallery[]" multiple>
                                        <label class="custom-file-label" for="customFile">Додади нова слика</label>
                                    </div>
                                </div>                              
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">Залиха</h5>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                    <div class="form-group">
                                        <label for="is_negativ"><b>Тип на Залиха</b></label>
                                        <select class='form-control'name='is_negativ'>
                                            <option value="0">Позитивна залиха</option>
                                            <option value="1">Негативна залиха</option>
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                    <label for="stock"><b>Залиха</b></label>
                                    <input class="form-control" id="stock" name="stock" type="text" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                    <div class="form-group">
                                        <label for="storage"><b>Магацинско Место</b></label>
                                        <input class="form-control" id="storage" name="storage" type="text">
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                    <div class="form-group">
                                        <label for="delay"><b>Достава за:</b></label>
                                        <input class="form-control" id="delay" name="delay" type="text">
                                    </div>
                                </div>                               
                            </div>
                            <div class="form-row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <div class="form-group">
                                        <label for="delivery_type"><b>Цена на Достава</b></label> 
                                        <select class='form-control'  id="delivery_type" name='delivery_type'>
                                            <option value="0">0</option>
                                            <option value="1">99-0%</option>
                                            <option value="2">99-50%</option>
                                            <option value="3">199</option>
                                            <option value="4">299</option>
                                            <option value="5">399</option>
                                            <option value="6">599</option>
                                            <option value="7">799</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row pt-2 pt-sm-2 mt-1">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <label class="control-label" for="search-box"> </label>
                                    <p class="text-left">
                                        <button type="reset" class="btn btn-space btn-secondary">Поништи</button>
                                        <button type="submit" class="btn btn-space btn-primary">Додади</button>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {


        $('form[id="insertproduct"]').validate({
            //errorClass: 'is-invalid',
            //validClass: 'is-valid',
            rules: {
                kom: 'required',
                product: 'required',
                size: 'required',
                price: 'required',
                stock: 'required',
                delivery_type: 'required',
                "gallery[]": 'required',
            },
            messages: {
                kom: 'This value is required.',
                product: 'This value is required.',
                size: 'This value is required.',
                price: 'This value is required.',
                stock: 'This value is required.',
                delivery_type: 'This value is required.',
            },
            errorPlacement: function (label, element) {
                if (element.is("textarea")) {
                    label.insertAfter(element.next());
                } else {
                    label.insertAfter(element)
                }
            },
            submitHandler: function (form) {
                var service = new FormData($("#insertproduct")[0]);
                //alert(service);
                $.ajax({
                    url: '<?= site_url('insertproduct2') ?>',
                    type: 'POST',
                    data: service,
                    beforeSend: function () {
                        // Show image container
                    },
                    success: function (data) {
                        alert(data);
                        location.reload();
                    },
                    complete: function (data) {
                        // Hide image container
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
        });
    });
</script>