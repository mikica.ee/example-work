<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php if (isset($_POST['active'])) { ?>
    <script>
        $(document).ready(function () {
            $('[name=active]').val(<?= $_POST['active'] ?>);
        });
    </script>
<?php } ?>
<?php if (isset($_POST['order'])) { ?>
    <script>
        $(document).ready(function () {
            $('[name=order]').val(<?= $_POST['order'] ?>);
        });
    </script>
<?php } ?>
<?php if (isset($_POST['is_negativ'])) { ?>
    <script>
        $(document).ready(function () {
            $('[name=is_negativ]').val(<?= $_POST['is_negativ'] ?>);
        });
    </script>
<?php } ?>
<?php if (isset($_POST['cat_id'])) { ?>
    <script>
        $(document).ready(function () {
            $('[name=cat_id]').val(<?= $_POST['cat_id'] ?>);
        });
    </script>
<?php } ?>
<?php if (isset($_POST['web'])) { ?>
    <script>
        $(document).ready(function () {
            $('[name=web]').val(<?= $_POST['web'] ?>);
        });
    </script>
<?php } ?>
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title text-center"><b>Листај производи</b></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12 offset-lg-1">
                <div class="card">
                    <h5 class="card-header"><b>Листај производи</b></h5>
                    <div class="card-body">
                        <?php if (validation_errors()) : ?>
                            <div class="col-md-12">
                                <div class="alert alert-danger" role="alert">
                                    <?= validation_errors() ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (isset($error)) : ?>
                            <div class="col-md-12">
                                <div class="alert alert-danger" role="alert">
                                    <?= $error ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <form name="insert" action="<?php echo site_url('listproducts'); ?>" method="post">
                            <div class="form-row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                    <div class="form-group">
                                        <label for="active"><b>Активност:</b></label>
                                        <select class='form-control'name='active'>
                                            <option value="1">Активни</option>
                                            <option value="0">Не Активни</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                    <div class="form-group">
                                        <label for="order"><b>Подредување по:</b></label>
                                        <select class='form-control'name='order'>
                                            <option value="0">Избери...</option>
                                            <option value="1">Залиха ASC</option>
                                            <option value="2">Залиха DESC</option>
                                            <option value="3">Цена ASC</option>
                                            <option value="4">Цена DESC</option>
                                        </select>  
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                    <div class="form-group">
                                        <label for="is_negativ"><b>Тип на залиха</b></label>
                                        <select class='form-control'name='is_negativ'>
                                            <option value="0">Избери...</option>
                                            <option value="1">Позитивна залиха</option>
                                            <option value="2">Негативна залиха</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                    <div class="form-group">
                                        <label for="cat_id"><b>Категорија</b></label><br>
                                        <select class='form-control' id="product_cat_id" name="cat_id">
                                            <option value="0">Избери...</option>
                                            <?php foreach ($cat as $item) { ?>
                                                <option value="<?= $item['cat_id'] ?>"><?= $item['cat_name'] ?> -> <?= $item['cat_slug'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                    <div class="form-group">
                                        <label for="web"><b>Поставен на web</b></label>
                                        <select class='form-control'name='web'>
                                            <option value="1">Избери...</option>
                                            <option value="2">Да</option>
                                            <option value="3">Не</option>
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                    <div class="form-group">
                                        <label for="kom"><b>Коминтент</b><sup>*</sup></label>
                                        <select id='kom' name="kom" class="form-control" data-width="100%">
                                        </select>
                                    </div>
                                </div>
                            </div> 
                            <div class="row pt-2 pt-sm-2 mt-1">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <label class="control-label" for="search-box"> </label>
                                    <p class="text-right">
                                        <button type="reset" class="btn btn-space btn-secondary">Поништи</button>
                                        <button type="submit" class="btn btn-space btn-primary">Додади</button>
                                    </p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function ($) {
        $("#kom").select2({
            width: '100%',
            minimumInputLength: 1,
            placeholder: "Изберете Коминтент",
            allowClear: true,
            ajax: {
                url: "<?= site_url('user/kom') ?>",
                type: "post",
                dataType: 'json',
                delay: 0,
                data: function (params) {
                    return {
                        searchTerm: params.term, // search term
                        //web_city_id: $("#web_city").val()
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: false
            }
        });
    });
</script>