<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php
if ($_SESSION['type'] == 1) {
    $edit = "";
} else {
    $edit = "readonly";
}
//echo '<pre>';
//print_r($res);
//echo '</pre>';
?>

<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <?php
        if (isset($alert)) {
            echo $alert;
        }
        ?>
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header text-center">
                    <h2 class="pageheader-title"><b>Ажурирање на продуктот <?= $res['product'] ?></b></h2>

                </div>
            </div>
        </div>
        <!--                <form name="update" action="<?php echo site_url('updateproduct?product=' . $res['product_id']); ?>" method="post">-->

        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <h5 class="card-header"><b>Генералии</b></h5>
                    <div class="card-body">
                        <form method="post" enctype="multipart/form-data" id="updateproduct" action="javascript:;" name="updateproduct">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="kom"><b>Коминтент</b></label>
                                    <select id='kom' name="kom" class="form-control" data-width="100%">
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <label for="product"><b>Име на Производ</b></label>
                                    <input class="form-control" id="product" name="product" type="text" value="<?= $res['product'] ?>"<?php echo $edit; ?>>
                                    <input class="form-control" id="product_id" name="product_id" value="<?php echo $res['product_id']; ?>" type="hidden">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-xl-6 col-lg-9 col-md-9 col-sm-9 col-9">
                                    <div class="form-group">
                                        <label for="color"><b>Боја</b></label>
                                        <select class='form-control'name='color'>
                                            <?php foreach ($color as $col) { ?>
                                                <?php
                                                if ($col['color_id'] == $res['color']) {
                                                    $sel = " selected";
                                                } else {
                                                    $sel = "";
                                                }
                                                ?>
                                                <option value="<?= $col['color_id'] ?>"<?= $sel ?>><?= $col['color_name'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-3 col-md-3 col-sm-3 col-3 ">
                                    <label for="storage"><b>Големина</b></label>
                                    <input class="form-control" id="size" name="size" type="text" value="<?php echo $res['size']; ?>">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-xl-6 col-lg-9 col-md-9 col-sm-9 col-9 ">
                                    <div class="form-group">
                                        <label for="price"><b>Цена</b></label>
                                        <input class="form-control" id="price" name="price" type="text" value="<?php echo $res['price']; ?>">
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-3 col-md-3 col-sm-3 col-3 ">
                                    <div class="form-group">
                                        <label for="pricen"><b>Набавна</b></label>
                                        <input class="form-control" id="pricen" name="pricen" type="text" value="<?php echo $res['pricen']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                    <label for="product_vat"><b>ДДВ</b></label>
                                    <select id="product_vat" name='product_vat' class='form-control' >
                                        <option value="18">18%</option>
                                        <option value="5">5%</option>
                                    </select> 
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                    <label for="gallery_product_id"><b>Додади нова слика за Продуктот</b></label>
                                    <div class="custom-file mb-3">
                                        <input class="custom-file-input" type="file" id="gallery" name="gallery[]"  multiple >
                                        <label class="custom-file-label" for="customFile">Додади нова слика</label>
                                    </div>
                                </div>                              
                            </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <h5 class="card-header">Залиха</h5>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="is_negativ"><b>Тип на залиха</b></label>
                                    <select class='form-control'name='is_negativ'>
                                        <?php if ($res['is_negativ']) { ?>
                                            <option value="1">Негативна залиха</option>
                                            <option value="0">Позитивна залиха</option>
                                        <?php } else { ?>
                                            <option value="0">Позитивна залиха</option>
                                            <option value="1">Негативна залиха</option>
                                        <?php } ?>
                                    </select>  
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <label for="stock"><b>Залиха</b></label>
                                <input class="form-control" id="stock" name="stock" type="text" value="<?php echo $res['stock']; ?>" disabled>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="storage"><b>Магацинско Место</b></label>
                                    <input class="form-control" id="storage" name="storage" type="text" value="<?php echo $res['storage']; ?>">
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="delay"><b>Достава за:</b></label>
                                    <input class="form-control" id="delay" name="delay" type="text" value="<?php echo $res['delay']; ?>">
                                </div>
                            </div>                               
                        </div>
                        <div class="form-row">
                            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="delivery_type"><b>Цена на Достава</b></label>
                                    <select class='form-control'  id="delivery_type" name='delivery_type'>
                                        <option value="1">99-0%</option>
                                        <option value="2">99-50%</option>
                                        <option value="3">199</option>
                                        <option value="4">299</option>
                                        <option value="5">399</option>
                                        <option value="6">599</option>
                                        <option value="7">799</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <div class="pt-2 pt-sm-2 mt-1">
                                    <div class="col-md-12 form-group">               
                                        <button class="btn btn-info btn-sm" type="reset">Поништи</button>
                                        <button class="btn btn-success btn-sm" type="submit">Ажурирај производ</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                        <form method="post" enctype="multipart/form-data" id="updatestock" action="javascript:;" name="updatestock">
                            <div class="form-row">

                                <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                                    <label for="stockadd"><b>Додади</b></label>
                                    <input class="form-control" id="stockadd" name="stockadd" type="text">
                                    <input class="form-control" id="product_id" name="product_id" type="hidden" value="<?= $res['product_id'] ?>">
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                    <label for="in_invoice_no"><b>Ф-ра</b></label>
                                    <select id='in_invoice_id' name="in_invoice_id" class="form-control" data-width="100%">
                                    </select>
                                </div>
                                <div class="row pt-2 pt-sm-2 mt-3">
                                    <div class="col-md-3 form-group">
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#fraModal">Внеси фактура</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row pt-2 pt-sm-2 mt-1">
                                <div class="col-md-12 form-group">               
                                    <button class="btn btn-info btn-sm" type="reset">Поништи</button>
                                    <button class="btn btn-success btn-sm" type="submit">Ажурирај залиха</button>
                                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal">Извештај</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div id="fraModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Фактура</h4>
                <button type="button" id="modalclose"  class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                    <form method="post" enctype="multipart/form-data" id="insertininvoice" action="javascript:;" name="insertininvoice">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                                <label for="in_invoice_no"><b>Фактури</b></label>
                                                <input class="form-control" id="in_invoice_no" name="in_invoice_no" type="text" autocomplete="off">
                                            </div>
                                        </div>
                                        <br>
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <p class="text-">
                                                <button type="submit" class="btn btn-primary btn-block">Додади фактура</button>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Извештај за продуктот</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                    <div class="card">
                        <h5 class="card-header"><b>Листа на извештаи за продукт : <?= $res['product'] ?></b></h5>
                        <div class="card-body">
                            <div class="row">
                                <hr>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <tbody>
                                            <tr class="danger">
                                                <td><h5><b>Парчиња</b></h5></td>
                                                <td><h5><b>Име на фактура</b></h5></td>   
                                                <td><h5><b>Датум</b></h5></td>
                                            </tr>
                                            <?php
                                            $cs = 0;
                                            $br = 0;
                                            foreach ($stocks as $stock) {
                                                if ($cs) {
                                                    $css = ' class="info"';
                                                    $cs = 0;
                                                } else {
                                                    $css = ' class="active"';
                                                    $cs = 1;
                                                }
                                                ?>

                                                <tr<?php echo $css; ?>>
                                                    <td><?= $stock['addstock_num']; ?></td>                           
                                                    <td><?= $stock['in_invoice_no']; ?></td>
                                                    <td><?= $stock['addstock_date']; ?></td>

                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>                                         
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    $(document).ready(function ($) {
        $("#delivery_type").val("<?= $res['delivery_type']; ?>");
        $("#product_vat").val("<?= $res['product_vat']; ?>");
        $('#kom').empty().append($("<option/>").val("<?= $res['id']; ?>").text("<?= $res['name'] ?>")).val("<?= $res['id'] ?>").trigger("change");
        $('form[id="updateproduct"]').validate({
            //errorClass: 'is-invalid',
            //validClass: 'is-valid',
            rules: {
                kom: 'required',
                product: 'required',
                size: 'required',
                price: 'required',
                stock: 'required',
                delivery_type: 'required',
                //"gallery[]": 'required',
            },
            messages: {
                kom: 'This value is required.',
                product: 'This value is required.',
                size: 'This value is required.',
                price: 'This value is required.',
                stock: 'This value is required.',
                delivery_type: 'This value is required.',
            },
            errorPlacement: function (label, element) {
                if (element.is("textarea")) {
                    label.insertAfter(element.next());
                } else {
                    label.insertAfter(element)
                }
            },
            submitHandler: function (form) {
                var service = new FormData($("#updateproduct")[0]);
                //alert(service);
                $.ajax({
                    url: '<?= site_url('updateproduct2') ?>',
                    type: 'POST',
                    data: service,
                    beforeSend: function () {
                        // Show image container
                    },
                    success: function (data) {
                        alert(data);
                        location.reload();
                    },
                    complete: function (data) {
                        // Hide image container
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
        });
        $('form[id="insertininvoice"]').validate({
            rules: {
                in_invoice_no: 'required',
            },
            messages: {
                in_invoice_no: 'This value is required.',
            },
            errorPlacement: function (label, element) {
                if (element.is("textarea")) {
                    label.insertAfter(element.next());
                } else {
                    label.insertAfter(element)
                }
            },
            submitHandler: function (form) {
                var ininvoice = new FormData($("#insertininvoice")[0]);
                //alert(service);
                $.ajax({
                    url: '<?= site_url('ininvoice/insert_ininvoice') ?>',
                    type: 'POST',
                    data: ininvoice,
                    beforeSend: function () {
                        // Show image container
                    },
                    success: function (data) {
                        alert(data);
                        $("#modalclose").click();
                    },
                    complete: function (data) {
                        // Hide image container
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
        });
        $("#kom").select2({
            width: '100%',
            minimumInputLength: 1,
            placeholder: "Изберете Коминтент",
            allowClear: true,
            ajax: {
                url: "<?= site_url('user/kom') ?>",
                type: "post",
                dataType: 'json',
                delay: 0,
                data: function (params) {
                    return {
                        searchTerm: params.term, // search term
                        //web_city_id: $("#web_city").val()
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: false
            }
        });
        $("#in_invoice_id").select2({
            width: '100%',
            minimumInputLength: 1,
            placeholder: "Изберете фактура",
            allowClear: true,
            ajax: {
                url: "<?= site_url('ininvoice/select_ininvoice') ?>",
                type: "post",
                dataType: 'json',
                delay: 0,
                data: function (params) {
                    return {
                        searchTerm: params.term, // search term
                        //web_city_id: $("#web_city").val()
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: false
            }
        });
        $('form[id="updatestock"]').validate({
            errorClass: 'is-invalid',
            validClass: 'is-valid',
            rules: {
                stockadd: 'required',
                in_invoice_id: 'required'
            },
            messages: {
                stockadd: 'This value is required.',
                in_invoice_id: 'This value is required.'
            },
            errorPlacement: function (label, element) {
                if (element.is("textarea")) {
                    label.insertAfter(element.next());
                } else {
                    label.insertAfter(element)
                }
            },
            submitHandler: function (form) {
                var service = new FormData($("#updatestock")[0]);
                //alert(service);
                $.ajax({
                    url: '<?= site_url('insertstock') ?>',
                    type: 'POST',
                    data: service,
                    beforeSend: function () {
                        // Show image container
                    },
                    success: function (data) {
                        alert(data);
                        location.reload();
                    },
                    complete: function (data) {
                        // Hide image container
                    },
                    error: function (request, error) {
                        console.log(arguments);
                        alert(" Can't do because: " + error);
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
        });

    });
</script>