<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content ">
        <div class="col-md-12">
            <div class="row">
                <div class="gallery-container">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 m-b-10">
                            <h1 class="page-description text-center">Галерија за продуктот: <?php echo $product['product']; ?></h1>
                        </div>
                        <?php if (!empty($gallery)) { ?>
                            <?php foreach ($gallery as $img) { ?>
                                <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
                                    <div class="service-thumbnail">
                                        <div class="service-img-head">
                                            <div class="service-img">
                                                <img src="<?php echo site_url('img/gallery/' . $img['gallery_folder'] . '/' . $img['gallery_file']); ?>" alt="" class="img-fluid"></div>
                                            <div class="caption">
                                                <a href="javascript:;" id="delete" class="ribbons bg-danger" type="submit" gallery_id = "<?= $img['gallery_id'] ?>" imglink = "<?= 'img/gallery/' . $img['gallery_folder'] . '/' . $img['gallery_file'] ?>">Избриши</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } else { ?>
                            <div class="col-sm-6 col-md-4">
                                <div class="panel panel-default">
                                    <div class="panel-heading">нема слики</div>
                                    <div class="panel-body">
                                        <--- Внесете
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<div id="gallery_datem" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Распоред на Датум</h4>
            </div>
            <div class="modal-body">
                <form method="post" enctype="multipart/form-data" id="gallery_update" action="javascript:;" name="gallery_update">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading"><h4 style="text-align: center;">Ажурирај го датумот за редослед</h4></div>
                            <div class="panel-body">
                                <input class="form-control" id="gallery_id" name="gallery_id" type="hidden" required>


                                <div class="form-group">
                                    <label for="feat">Датум</label>
                                    <div class="input-group date" id="datetimepicker" data-target-input="nearest">
                                        <input type="text" id="gallery_date" name="gallery_date" class="form-control datetimepicker-input" data-target="#gallery_date" />
                                        <div class="input-group-append" data-target="#gallery_date" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 form-group">               
                                    <button class="btn btn-info btn-sm" type="reset">Поништи</button>
                                    <button class="btn btn-success btn-sm" type="submit">Ажурирај</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="modalclose" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>-->
<script>
    $(document).ready(function ($) {
        $("a#delete").click(function (e) {
            var gallery_id = $(this).attr("gallery_id");
            var imglink = $(this).attr("imglink");
            var url = "<?php echo site_url('gallery/delete') ?>";
            var obj = {
                gallery_id: gallery_id,
                imglink: imglink
            }
            if (confirm("Избришете ја сликата: " + imglink)) {
                $.post(url, obj, function (data, textStatus, jqXHR) {
                    alert(data);
                }).done(function (data) {
                    location.reload();
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown + ": " + jqXHR.responseText);
                });
            }
        });
    });
</script>