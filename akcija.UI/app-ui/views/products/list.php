<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//echo '<pre>';
//print_r($res);
//echo '</pre>';
?>
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header text-center"><b>Продукти</b></h5>
                <div class="card-body">
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered first">
                            <tbody>
                                <tr class="danger">
                                    <td><h5><b>Бр.</b></h5></td>
                                    <td><h5><b>ИД</b></h5></td>
                                    <td><h5><b>Име на продукт</b></h5></td>
                                    <td><h5><b>залиха</b></h5></td>
                                    <td><h5><b>цена</b></h5></td>
                                    <td><h5><b>набавна</b></h5></td>
                                    <td><h5><b>ДДВ</b></h5></td>
                                    <td><h5><p style="text-align: center;"><b>тип на залиха</b></p></h5></td>
                                    <td><h5><b>Боја</b></h5></td>
                                    <td><h5><b>Големина</b></h5></td>
                                    <td><span class="fas fa-truck" style="font-size:22px;"></span></td>
                                    <td><h5><b>магацин</b></h5></td>
                                    <td><h5><b>активен</b></h5></td>
                                    <td></td>
                                </tr>
                                <?php
                                $cs = "0";
                                for ($x = 0; $x < count($res); $x++) {
                                    $data = $res[$x];
                                    $br = $x;
                                    if ($cs) {
                                        $css = ' class="info"';
                                        $cs = "0";
                                    } else {
                                        $css = ' class="active"';
                                        $cs = "1";
                                    }
                                    ?>
                                    <tr<?php echo $css; ?> id="<?php echo $data['product_id']; ?>">
                                        <td><?php echo ++$br; ?></td>
                                        <td><?php echo $data['product_id']; ?></td>
                                        <td><b><?php echo $data['product']; ?></b></td>
                                        <td><?php echo $data['stock']; ?></td>
                                        <td><?php echo $data['price']; ?></td>
                                        <td><?php echo $data['pricen']; ?></td>
                                        <td><?php echo $data['product_vat']; ?></td>
                                        <td><p style="text-align: center;"><?php
                                                //echo $data['is_negativ']; 
                                                if ($data['is_negativ']) {
                                                    echo '<span class="glyphicon glyphicon-ok" style="color:green;"></span>';
                                                } else {
                                                    echo '<span class="glyphicon glyphicon-remove" style="color:red;"></span>';
                                                }
                                                ?></p></td>
                                        <td><img src="<?= base_url($data['color_link']) ?>" alt="" height="30" width="30"/></td>
                                        <td><?php echo $data['size']; ?></td>
                                        <td><?php echo $data['delay']; ?></td>
                                        <td><?php echo $data['storage']; ?></td>
                                        <td><?php
                                            if ($data['active']) {
                                                echo '<a href="javascript:;" id = "usage" product_id = "' . $data['product_id'] . '" active = "0"><span class="fas fa-thumbs-up" style="color:green;"></span></a>';
                                            } else {
                                                echo '<a href="javascript:;" id = "usage" product_id = "' . $data['product_id'] . '" active = "1"><span class="fas fa-thumbs-down" style="color:red;"></span></a>';
                                                //echo '<span id = "usage" product_id = "'.$data['product_id'].'" active = "1" class="glyphicon glyphicon-remove" style="color:red;"></span>';
                                            }
                                            ?></td>
                                        <td><a href="<?= site_url('updateproduct?product=' . $data['product_id']) ?>"><span class="fas fa-edit" style="font-size:15px;"></span></a></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <?= $pagination ?>
                        <!--            <hr>-->
                    </div>
                </div><!-- .row -->
            </div><!-- .container -->
            <script>
                $(document).ready(function ($) {
                    $("a#usage").click(function (e) {
                        var product_id = $(this).attr("product_id");
                        var active = $(this).attr("active");
                        var url = "<?php echo site_url('usage') ?>";
                        var obj = {
                            product_id: product_id,
                            active: active
                        }
                        if (confirm("Променија активноста на производот: " + product_id)) {
                            $.post(url, obj, function (data, textStatus, jqXHR) {
                                alert(data);
                            }).done(function (data) {
                                location.reload();
                            }).fail(function (jqXHR, textStatus, errorThrown) {
                                alert(errorThrown + ": " + jqXHR.responseText);
                            });
                        }
                    });
                });
            </script>