<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<script type="text/javascript">
    setTimeout(function () {
        $('#fadeout').fadeOut('fast');
    }, 5000);
</script>

<script>
    $(document).ready(function () {
        $("#insertitem").hide();
        $("#product").keyup(function () {
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('products/selectproduct') ?>",
                data: 'keyword=' + $(this).val(),
                beforeSend: function () {
                    $("#product").css("background", "#FFF url(LoaderIcon.gif) no-repeat 165px");
                    $("#product").css("background-color", "#FFF");
                },
                success: function (data) {
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(data);
                    $("#search-box").css("background", "#FFF");
                }
            });
        });
    });

    function selectProductid(val) {

        $("#" + val).css("background-color", "yellow");
    }
</script>

<div class="container">
    <div class="row">
        <h3 style="text-align: center;">Внеси нов производ</h3>
        <hr>
        <div id="fadeout"><?php echo $alert; ?></div>
        <form name="insert" action="<?php echo site_url('products/insert'); ?>" method="post">
            <div class="form-group col-md-3">
                <label for="product">Име на производ</label>
                <input class="form-control" id="product" name="product" type="text" autocomplete="off" required>
                <!--                <div class="row">-->
                <div id="suggesstion-box"></div>
                <!--                </div>-->
            </div>
            <div class="form-group col-md-2">
                <label for="stock">Залиха</label>
                <input class="form-control" id="stock" name="stock" type="text" required>
            </div>
            <div class="form-group col-md-2">
                <label for="price">Цена</label>
                <input class="form-control" id="price" name="price" type="text" required>
            </div>
            <div class="form-group col-md-2">
                <label for="is_negativ">Тип на залиха</label>
                <select class='form-control'name='is_negativ'>
                    <option value="0">Позитивна залиха</option>
                    <option value="1">Негативна залиха</option>
                </select>         
            </div>
            <div class="form-group col-md-3">
                <label for="storage">Магацинско место</label>
                <input class="form-control" id="storage" name="storage" type="text">
            </div>
            <div class="form-group col-md-12">
                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="storage">Боја</label>
                        <input class="form-control" id="storage" name="color" type="text">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="storage">Големина</label>
                        <input class="form-control" id="storage" name="size" type="text">
                    </div>
                </div>
            </div>
            <div class="col-md-12 form-group">               
                <button class="btn btn-info btn-sm" type="reset">Поништи</button>
                <button class="btn btn-success btn-sm" type="submit">Внеси производ</button>
            </div>
        </form>
    </div>
</div>
