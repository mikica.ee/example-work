
<?php
//echo '<pre>';
//print_r($brand);
//echo '</pre>';
?>
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 offset-lg-5">
                <div class="page-header">
                    <h2 class="pageheader-title"><b>Често Поставувани Прашања</b></h2>

                </div>
            </div>
        </div>
        <form method="post" enctype="multipart/form-data" id="updatefaq" action="javascript:;" name="updatefaq">
            <div class="row">
                <div class="col-xl-7 col-lg-6 col-md-12 col-sm-12 col-12 offset-lg-3">
                    <div class="card">
                        <h5 class="card-header"><b>ЧПП</b></h5>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="faq_title"><b>Прашање</b></label>
                                    <input class="form-control" id="faq_id" name="faq_title" type="text" value="<?= $faq[0]['faq_title'] ?>">
                                    <input type="hidden" value="<?= $faq[0]['faq_id'] ?>" id="faq_id" name="faq_id">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <label for="faq_text"><b>Одговор</b></label>
                                    <textarea rows="4" id="faq_text" name="faq_text" <?= $faq[0]['faq_text'] ?>   </textarea>
                                    <input type="hidden" value="<?= $faq[0]['faq_id'] ?>" id="faq_id" name="faq_id">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <label for="faq_date_time"><b>Датум и Време</b></label>
                                    <div class="input-group date" id="faq_date_time" data-target-input="nearest">
                                        <input type="text" id="faq_date_time" name="faq_date_time" class="form-control datetimepicker-input" data-target="#faq_date_time" value="<?= $faq[0]['faq_date_time'] ?>">
                                        <div class="input-group-append" data-target="#faq_date_time" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p class="text-center">
                            <button type="submit" class="btn btn-primary btn-block">Измени</button>
                        </p>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function ($) {
        $('#faq_text').summernote({
            height: 250
        });

        $('#datetimepicker7').datetimepicker({format: 'YYYY-MM-D H:mm:ss'});
        $('#faq_date_time').datetimepicker({
            //viewMode: 'years',
            format: 'YYYY-MM-D H:mm:ss',
            icons: {
                time: "far fa-clock",
                date: "fa fa-calendar-alt",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            date: faq_date_time
        });
        $('form[id="updatefaq"]').validate({
            rules: {
                faq_title: 'required',
            },
            messages: {
                faq_title: 'This value is required.',
            },
            errorPlacement: function (label, element) {
                if (element.is("textarea")) {
                    label.insertAfter(element.next());
                } else {
                    label.insertAfter(element)
                }
            },
            submitHandler: function (form) {
                var faq = new FormData($("#updatefaq")[0]);
                //alert(service);
                $.ajax({
                    url: '<?= site_url('faq/update_faq') ?>',
                    type: 'POST',
                    data: faq,
                    beforeSend: function () {
                        // Show image container
                    },
                    success: function (data) {
                        alert(data);
                        location.reload();
                    },
                    complete: function (data) {
                        // Hide image container
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
        });

    });
</script>