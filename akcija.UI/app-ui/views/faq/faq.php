<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title text-center"><b>Често Поставувани Прашања</b></h2>

                </div>
            </div>
        </div>
        <form method="post" enctype="multipart/form-data" id="insertfaq" action="javascript:;" name="insertfaq">
            <div class="row">
                <div class="col-xl-7 col-lg-6 col-md-12 col-sm-12 col-12 offset-lg-3">
                    <div class="card">
                        <h5 class="card-header"><b>ЧПП</b></h5>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="faq_title"><b>Прашање</b></label>
                                    <input class="form-control" id="faq_title" name="faq_title" type="text" autocomplete="off">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <label for="faq_text"><b>Одговор</b></label>
                                    <textarea rows="4" id="faq_text" name="faq_text"></textarea>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <label for="faq_date_time"><b>Датум и Време</b></label>
                                    <div class="input-group date" id="faq_date_time" data-target-input="nearest">
                                        <input type="text" id="faq_date_time" name="faq_date_time" class="form-control datetimepicker-input" data-target="#faq_date_time" />
                                        <div class="input-group-append" data-target="#faq_date_time" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p class="text-center">
                            <button type="submit" class="btn btn-primary btn-block">Додади</button>
                        </p>
                    </div>
                </div>
            </div>
        </form>
        <div class="col-xl-7 col-lg-6 col-md-12 col-sm-12 col-12 offset-lg-3">
            <div class="card">
                <h5 class="card-header"><b>Прашања и Одговори</b></h5>
                <div class="card-body">
                    <div class="row">
                        <hr>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered first">
                                <tbody>
                                    <tr class="danger">
                                        <td><h5><b>Бр.</b></h5></td>
                                        <td><h5><b>Id.</b></h5></td>
                                        <td><h5><b>Прашања</b><br><b>Одговори</b></h5></td>
                                        <td><h5><b>Датум</b></h5></td>
                                        <td><h5><b></b></h5></td>
                                    </tr>
                                    <?php
                                    $cs = 0;
                                    $br = 0;
                                    foreach ($faqs as $faq) {
                                        if ($cs) {
                                            $css = ' class="info"';
                                            $cs = 0;
                                        } else {
                                            $css = ' class="active"';
                                            $cs = 1;
                                        }
                                        ?>

                                        <tr<?php echo $css; ?>>
                                            <td><?= ++$br; ?></td>
                                            <td><?= $faq['faq_id']; ?></td>
                                            <td><b><?= $faq['faq_title']; ?></b><br><?= $faq['faq_text']; ?></td>
                                            <td><?= $faq['faq_date_time']; ?></td>
                                            <td><a href="<?= site_url('faq/update/' . $faq['faq_id']) ?>"><i class="fas fa-edit"></i></a></td>
                                        </tr>
                                    <?php } ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function ($) {
        $('#faq_date_time').datetimepicker({
            //viewMode: 'years',
            format: 'YYYY-MM-D H:mm:ss',
            icons: {
                time: "far fa-clock",
                date: "fa fa-calendar-alt",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            date: new Date()
        });
        $('#faq_text').summernote({
            height: 250
        });
        $('form[id="insertfaq"]').validate({
            rules: {
                faq_title: 'required',
                faq_text: 'required',
            },
            messages: {
                faq_title: 'This value is required.',
            },
            errorPlacement: function (label, element) {
                if (element.is("textarea")) {
                    label.insertAfter(element.next());
                } else {
                    label.insertAfter(element)
                }
            },
            submitHandler: function (form) {
                var faq = new FormData($("#insertfaq")[0]);
                //alert(service);
                $.ajax({
                    url: '<?= site_url('faq/insert_faq') ?>',
                    type: 'POST',
                    data: faq,
                    beforeSend: function () {
                        // Show image container
                    },
                    success: function (data) {
                        alert(data);
                        location.reload();
                    },
                    complete: function (data) {
                        // Hide image container
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
        });

    });
</script>