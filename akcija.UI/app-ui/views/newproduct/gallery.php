<?php
//echo '<pre>';
//print_r($gallery);
//echo '</pre>';
?>
<div class="container">
    <div class="col-md-12">
        <div class="row">
            <div class="gallery-container">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 m-b-10">
                        <h1 class="page-description text-center">Галерија за продуктот:</h1>
                    </div>
                    <?php if (!empty($gallery)) { ?>
                        <?php foreach ($gallery as $img) { ?>
                            <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
                                <div class="service-thumbnail">
                                    <div class="service-img-head">
                                        <div class="service-img">
                                            <img src="<?= site_url($img['gallery_img']) ?>" alt="" class="img-fluid"></div>
                                            <p>Alt: <?= $img['gallery_alt'] ?></p>
                                        <a href="javascript:;" data-toggle="modal" data-target="#gallery_datem" id="gallery_dateb" class="btn btn-success btn-sm" type="submit" gallery_id = "<?= $img['gallery_id'] ?>" gallery_date = "<?= $img['gallery_date'] ?>" gallery_alt = "<?= $img['gallery_alt'] ?>"> <?= $img['gallery_date'] ?> </a>
                                        <?php if (!$img['gallery_feat']) { ?>
                                            <a href="javascript:;" id="delete" gallery_id ="<?= $img['gallery_id'] ?>" imglink = "<?= $img['gallery_img'] ?>"><div class="ribbons bg-danger">Избриши</div></a>
                                            <a href="javascript:;" id="feat" gallery_id ="<?= $img['gallery_id'] ?>" gallery_p = "<?= $img['gallery_p'] ?>" imglink = "<?= $img['gallery_img'] ?>" class="product-wishlist-btn"><div><i class="fas fa-heart"></i></div></a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="col-sm-6 col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">нема слики</div>
                                <div class="panel-body">
                                    Внесете
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="gallery_datem" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Распоред на Датум</h4>
            </div>
            <div class="modal-body">
                <form method="post" enctype="multipart/form-data" id="gallery_update" action="javascript:;" name="gallery_update">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading"><h4 style="text-align: center;">Ажурирај го датумот за редослед</h4></div>
                            <div class="panel-body">
                                <input class="form-control" id="gallery_id" name="gallery_id" type="hidden" required>
                                <div class="form-group">
                                    <label for="gallery_date">Датум</label>
                                    <div class="input-group date" id="gallery_date" data-target-input="nearest">
                                        <input id="gallery_date" name="gallery_date" type="text" class="form-control datetimepicker-input gallery_datedd" data-target="#gallery_date" required />
                                        <div class="input-group-append" data-target="#gallery_date" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="gallery_alt"><b>Alt</b></label>
                                    <input class="form-control" id="gallery_alt" name="gallery_alt" type="text">
                                </div>
                                <div class="col-md-12 form-group">               
                                    <button class="btn btn-info btn-sm" type="reset">Поништи</button>
                                    <button class="btn btn-success btn-sm" type="submit">Ажурирај</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="modalclose" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function ($) {
        $('#gallery_date').datetimepicker({
            format: 'YYYY-MM-D H:mm:ss',
            date: new Date()
        });
        $('form[id="gallery_update"]').validate({
            rules: {
                gallery_date: 'required',

            },
            messages: {

            },
            errorPlacement: function (label, element) {
                if (element.is("textarea")) {
                    label.insertAfter(element.next());
                } else {
                    label.insertAfter(element)
                }
            },
            submitHandler: function (form) {
                //tinyMCE.triggerSave();
                var gallery = new FormData($("#gallery_update")[0]);
                $.ajax({
                    url: '<?= site_url('newproduct/gallery_update') ?>',
                    type: 'POST',
                    data: gallery,
                    success: function (data) {
                        alert(data);
                        location.reload();
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
        });

        $("a#gallery_dateb").click(function (e) {
            var gallery_id = $(this).attr("gallery_id");
            var gallery_date = $(this).attr("gallery_date");
            var gallery_alt = $(this).attr("gallery_alt");
            $('.gallery_datedd').val(gallery_date);
            $('#gallery_id').val(gallery_id);
            $('#gallery_alt').val(gallery_alt);
        });
        $("a#feat").click(function (e) {
            var gallery_id = $(this).attr("gallery_id");
            var gallery_p = $(this).attr("gallery_p");
            var imglink = $(this).attr("imglink");
            var url = "<?= site_url('newproduct/feat') ?>";
            var obj = {
                gallery_id: gallery_id,
                gallery_p: gallery_p,
                imglink: imglink
            }
            if (confirm("Постави ја сликата за главна")) {
                $.post(url, obj, function (data, textStatus, jqXHR) {
                    alert(data);
                }).done(function (data) {
                    location.reload();
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown + ": " + jqXHR.responseText);
                });
            }
        });
        $("a#delete").click(function (e) {
            var gallery_id = $(this).attr("gallery_id");
            var imglink = $(this).attr("imglink");
            var url = "<?= site_url('newproduct/deleteimg') ?>";
            var obj = {
                gallery_id: gallery_id,
                imglink: imglink
            }
            if (confirm("Избришете ја сликата: " + imglink)) {
                $.post(url, obj, function (data, textStatus, jqXHR) {
                    alert(data);
                }).done(function (data) {
                    location.reload();
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown + ": " + jqXHR.responseText);
                });
            }
        });
    });
</script>