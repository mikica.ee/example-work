  <?php
//echo '<pre>';
//print_r($sims);
//echo '</pre>';
?>
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 style="text-align: center;" class="pageheader-title"><b>Сл Производи и Тагови</b></h2>
                </div>
            </div>
        </div>
        <input type="hidden" value="<?= $product['web_products_id'] ?>" id="simular_product_p" name="simular_product_p">
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <h5 class="card-header"><b>Слични производи</b></h5>
                    <div class="card-body">
                        <form method="post" enctype="multipart/form-data" id="simular" action="javascript:;" name="simular">
                            <input type="hidden" value="<?= $product['web_products_id'] ?>" id="simular_product_p" name="simular_product_p">
                            <div class="form-group">
                                <label for="simular_product_s"><b>Слични производи </b><sup>*</sup></label>
                                <select multiple="multiple" id='simular_product_s' name="simular_product_s[]" class="form-control" data-width="100%">
                                </select>
                            </div>                       
                            <button type="submit" class="btn btn-primary btn-block" id="btnSubmit">Додади го производот</button>
                        </form>
                        <hr>
                        <?php foreach ($sims as $sim) { ?>
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="<?= base_url($sim['web_products_img']) ?>" width="50px" height="50px" alt="alt"/>
                                </div>
                                <div class="col-md-8">
                                    <a id="simular_product_id" href="javascript:;" simular_product_id="<?= $sim['simular_product_id'] ?>" web_products_name = "<?= $sim['web_products_name'] ?>"><?= $sim['web_products_name'] ?><i class="fas fa-trash"></i></a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <h5 class="card-header"><b>Таг</b></h5>
                    <div class="card-body">
                        <form method="post" enctype="multipart/form-data" id="tag" action="javascript:;" name="tag">
                            <input type="hidden" value="<?= $product['web_products_id'] ?>" id="tag_rel_p" name="tag_rel_p">
                            <div class="form-group">
                                <label for="simular_product_s"><b>Тагови </b><sup>*</sup></label>
                                <select multiple="multiple" id='tag_rel_t' name="tag_rel_t[]" class="form-control" data-width="100%">
                                </select>
                            </div>                       
                            <button type="submit" class="btn btn-primary btn-block" id="btnSubmit">Додади го Тагот</button>
                        </form>
                        <hr>
                        <?php foreach ($tags as $tag) { ?>
                                <a id="tag_id" href="javascript:;" tag_rel_id="<?= $tag['tag_rel_id'] ?>" tag_name = "<?= $tag['tag_name'] ?>"><?= $tag['tag_name'] ?><i class="fas fa-trash"></i></a>,
                                <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function ($) {
                $("a#simular_product_id").click(function (e) {
                    var simular_product_id = $(this).attr("simular_product_id");
                    var web_products_name = $(this).attr("web_products_name");
                    var obj = {
                        simular_product_id: simular_product_id
                    }
                    if (confirm("Избриши го Сличниот производ: " + web_products_name)) {
                        $.ajax({
                            url: '<?= site_url('newproduct/del_rel_sim') ?>',
                            type: 'POST',
                            data: obj,
                            success: function (data) {
                                alert(data);
                                location.reload();
                            },
                        });
                    }
                    //alert(relation);
                });
                $("a#tag_id").click(function (e) {
                    var tag_rel_id = $(this).attr("tag_rel_id");
                    var tag_name = $(this).attr("tag_name");
                    var obj = {
                        tag_rel_id: tag_rel_id
                    }
                    if (confirm("Избриши го Тагот: " + tag_name)) {
                        $.ajax({
                            url: '<?= site_url('tag/del_rel_tag') ?>',
                            type: 'POST',
                            data: obj,
                            success: function (data) {
                                alert(data);
                                location.reload();
                            },
                        });
                    }
                    alert(relation);
                });
                $("#tag_rel_t").select2({
                    //width: '100%',
                    minimumInputLength: 1,
                    placeholder: " Add Tags",
                    allowClear: true,
                    ajax: {
                        url: "<?= site_url('tag/selecttag') ?>",
                        type: "post",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                searchTerm: params.term, // search term
                            };
                        },
                        processResults: function (response) {
                            return {
                                results: response
                            };
                        },
                        cache: false
                    }
                });
                $("#simular_product_s").select2({
                    //width: '100%',
                    minimumInputLength: 1,
                    placeholder: " Add simular products",
                    allowClear: true,
                    ajax: {
                        url: "<?= site_url('newproduct/selectproduct_forsim') ?>",
                        type: "post",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                searchTerm: params.term, // search term
                            };
                        },
                        processResults: function (response) {
                            return {
                                results: response
                            };
                        },
                        cache: false
                    }
                });
                $('form[id="simular"]').validate({
                    rules: {
                        web_products_name: 'required',

                    },
                    messages: {
                        cat: 'This value is required.',

                    },
                    errorPlacement: function (label, element) {
                        if (element.is("textarea")) {
                            label.insertAfter(element.next());
                        } else {
                            label.insertAfter(element)
                        }
                    },
                    submitHandler: function (form) {
                        //tinyMCE.triggerSave();
                        var product = new FormData($("#simular")[0]);
                        $.ajax({
                            url: '<?= site_url('newproduct/insert_simular') ?>',
                            type: 'POST',
                            data: product,
                            success: function (data) {
                                alert(data);
                                location.reload();
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });
                    }
                });
                $('form[id="tag"]').validate({
                    rules: {
                        web_products_name: 'required',

                    },
                    messages: {
                        cat: 'This value is required.',

                    },
                    errorPlacement: function (label, element) {
                        if (element.is("textarea")) {
                            label.insertAfter(element.next());
                        } else {
                            label.insertAfter(element)
                        }
                    },
                    submitHandler: function (form) {
                        //tinyMCE.triggerSave();
                        var product = new FormData($("#tag")[0]);
                        $.ajax({
                            url: '<?= site_url('tag/insert_rel') ?>',
                            type: 'POST',
                            data: product,
                            success: function (data) {
                                //alert(data);
                                location.reload();
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });
                    }
                });

            });
        </script>