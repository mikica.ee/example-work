<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h4 style="text-align: center;"><b>Ажурирај Производ <?= $product['web_products_id'] ?></b></h4>
                </div>
            </div>
        </div>
        <form method="post" enctype="multipart/form-data" id="update" action="javascript:;" name="update">
            <input type="hidden" value="<?= $product['web_products_id'] ?>" id="web_products_id" name="web_products_id">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header"><b>Генералии</b></h5>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="web_products_name"><b>Наслов</b></label>
                                    <input class="form-control" id="web_products_name" name="web_products_name" type="text" value="<?= $product['web_products_name'] ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="web_products_name"><b>Линк</b></label>
                                    <input class="form-control" type="text" value="<?= $product['web_products_slug'] ?>" id="web_products_slug" name="web_products_slug">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                                    <div class="form-group">
                                        <label for="feat"><b>Прикажи на Почетна</b></label>
                                        <select id='feat' name="feat" class="form-control">
                                            <option value="1">Да</option>
                                            <option value="0">Не</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                                    <label for="web_type"><b>Тип на Избор</b></label>
                                    <select id='web_type' name="web_type" class="form-control">
                                        <option value="1">Само Боја</option>
                                        <option value="2">Само Големина</option>
                                        <option value="3">Боја и Големина</option>
                                        <option value="4">Еден Продукт</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                                    <div class="form-group">
                                        <label for="web_price"><b>Стара Цена</b></label>
                                        <input class="form-control" id="web_price_old" name="web_price_old" type="text" value="<?= $product['web_price_old'] ?>">
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                                    <div class="form-group">
                                        <label for="web_price"><b>Цена</b></label>
                                        <input class="form-control" id="web_price" name="web_price" type="text" value="<?= $product['web_price'] ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                                    <div class="form-group">
                                        <br>
                                        <div class="custom-file mb-3">
                                            <input class="custom-file-input" type="file" id="gallery" name="gallery[]" multiple>
                                            <label class="custom-file-label" for="file">Слики</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                                    <div class="form-group">
                                        <label for="feat"><b>Време за редослед</b></label>
                                        <div class='input-group date' id='datetimepicker7'>
                                            <input id="web_time" name="web_time" type='text' class="form-control" value="<?= $product['web_time'] ?>" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="web_products_cat">Почетна Категорија</label><br>
                                    <select class='form-control' id="web_products_cat" name="web_products_cat">
                                        <?php foreach ($cat as $item) { ?>
                                            <option value="<?= $item['cat_id'] ?>"><?= $item['cat_name'] ?> -> <?= $item['cat_slug'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="cat"><b>Категорија</b>
                                        <?php foreach ($catrel as $val) { ?>
                                            <a id="catdel" href="javascript:;" cat_rel_id="<?= $val['cat_rel_id'] ?>"><?= $val['cat_name'] ?><i class="fas fa-trash"></i></a>
                                        <?php } ?></label>
                                    <select multiple="multiple" id='cat' name="cat[]" class="form-control" data-width="100%">
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="var"><b>Варијабли (продукт)</b></label>
                                    <?php
                                    if (!empty($products)) {
                                        foreach ($products as $x => $item) {
                                            echo ' <a href="'.site_url("updateproduct?product=".$item['product_id']).'" target="_blank">'.$item['product'] . "</a> ->" . $item['price'] . ' <a href="javascript:;" id="deleteitem" relation="' . $item['product_relation_id'] . '">'
                                            . '<span class="fas fa-trash" style="color:red;"></span></a> , ';
                                        }
                                    } else {
                                        echo 'немате избрано производи';
                                    }
                                    ?>
                                    <select multiple="multiple" id='var' name="var[]" class="form-control" data-width="100%">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#myModal">Додади варијабла (продукт)</button>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">Presentation</h5>
                        <div class="card-body">
                            <div class="pills-regular">
                                <ul class="nav nav-pills mb-1" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="home" aria-selected="true">Опис</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="contact" aria-selected="false">Таб 1</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-contact2-tab" data-toggle="pill" href="#pills-contact2" role="tab" aria-controls="contact2" aria-selected="false">Таб 2</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-contact3-tab" data-toggle="pill" href="#pills-contact3" role="tab" aria-controls="contact3" aria-selected="false">SEO</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                        <div class="col-md-12 p-0">
                                            <div class="form-group">
                                                <label class="control-label sr-only" for="web_products_text">Descriptions </label>
                                                <textarea rows="13" id="web_products_text" name="web_products_text"><?= $product['web_products_text'] ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                                        <div class="col-md-12 p-0">
                                            <div class="form-group">
                                                <label class="control-label sr-only" for="tab1">Descriptions </label>
                                                <textarea rows="15" class="form-control" id="tab1" name="tab1"><?= $product['tab1'] ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-contact2" role="tabpanel" aria-labelledby="pills-contact2-tab">
                                        <div class="col-md-12 p-0">
                                            <div class="form-group">
                                                <label class="control-label sr-only" for="tab2">Descriptions </label>
                                                <textarea rows="15" class="form-control" id="tab2" name="tab2"><?= $product['tab2'] ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-contact3" role="tabpanel" aria-labelledby="pills-contact3-tab">
                                        <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                                <label for="web_keyword"><b>Keyword</b></label>
                                                <input class="form-control" id="web_keyword" name="web_keyword" type="text" value="<?= $product['web_keyword'] ?>">
                                            </div>
                                        </div>
                                        <br>
                                        <div class="col-md-12 p-0">
                                            <div class="form-group">
                                                <label class="control-label sr-only" for="web_seo_des">SEO Descriptions </label>
                                                <textarea rows="15" class="form-control" id="web_seo_des" name="web_seo_des"><?= $product['web_seo_des'] ?></textarea>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="row pt-2 pt-sm-2 mt-1">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <p class="text-center">
                                        <button type="submit" class="btn btn-primary btn-block">Ажурирај го Производот</button>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Додади нов продукт</h4>
            </div>
            <div class="modal-body">
                <form method="post" enctype="multipart/form-data" id="insertproduct" action="javascript:;" name="insertproduct">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header"><b>Генералии</b></h5>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                            <label for="kom"><b>Коминтент</b></label>
                                            <select id='kom' name="kom" class="form-control" data-width="100%">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <label for="product"><b>Име на Производ</b></label>
                                            <input type="text" class="form-control" id="product" name="product">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-xl-6 col-lg-9 col-md-9 col-sm-9 col-9">
                                            <div class="form-group">
                                                <label for="color"><b>Боја</b></label>
                                                <select class='form-control'name='color'>
                                                    <?php foreach ($color as $col) { ?>
                                                        <option value="<?= $col['color_id'] ?>"><?= $col['color_name'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-3 col-md-3 col-sm-3 col-3 ">
                                            <label for="storage"><b>Големина</b></label>
                                            <input class="form-control" id="size" name="size" type="text">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-xl-6 col-lg-9 col-md-9 col-sm-9 col-9 ">
                                            <div class="form-group">
                                                <label for="price"><b>Цена</b></label>
                                                <input type="text" class="form-control" id="price" name="price">
                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-3 col-md-3 col-sm-3 col-3 ">
                                            <div class="form-group">
                                                <label for="pricen"><b>Набавна</b></label>
                                                <input type="text" class="form-control" id="pricen" name="pricen">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                            <label for="brand"><b>Бренд</b></label>
                                            <select multiple="multiple" id='brand' name="brand" class="form-control" data-width="100%">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                            <label for="product_vat"><b>ДДВ</b></label>
                                            <select class='form-control'name='product_vat'>
                                                <option value="18">18%</option>
                                                <option value="5">5%</option>
                                            </select> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Залиха</h5>
                                <div class="card-body">
                                    <div class="form-row">
                                        <div class="col-xl-6 col-lg-9 col-md-9 col-sm-9 col-9">
                                            <div class="form-group">
                                                <label for="is_negativ"><b>Тип на Залиха</b></label>
                                                <select class='form-control'name='is_negativ'>
                                                    <option value="0">Позитивна залиха</option>
                                                    <option value="1">Негативна залиха</option>
                                                </select> 
                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-3 col-md-3 col-sm-3 col-3 ">
                                            <label for="stock"><b>Залиха</b></label>
                                            <input class="form-control" id="stock" name="stock" type="text" required>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-xl-6 col-lg-9 col-md-9 col-sm-9 col-9 ">
                                            <div class="form-group">
                                                <label for="storage"><b>Магацинско Место</b></label>
                                                <input class="form-control" id="storage" name="storage" type="text">
                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-3 col-md-3 col-sm-3 col-3 ">
                                            <div class="form-group">
                                                <label for="delay"><b>Достава за:</b></label>
                                                <input class="form-control" id="delay" name="delay" type="text">
                                            </div>
                                        </div>                               
                                    </div>
                                    <div class="form-row">
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                            <div class="form-group">
                                                <label for="delivery_type"><b>Цена на Достава</b></label>
                                                <input class="form-control" id="delivery_type" name="delivery_type" type="text">  
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row pt-2 pt-sm-2 mt-1">
                                        <div class="col-md-12 form-group">               
                                            <button class="btn btn-info btn-sm" type="reset">Поништи</button>
                                            <button class="btn btn-success btn-sm" type="submit">Внеси производ</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="modalclose" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function ($) {
        $("#web_products_cat").val('<?= $product['web_products_cat'] ?>');
        $("#web_products_cat").change(function () {
            var firstcat = $('#web_products_cat').val();
            $.ajax({
                url: '<?= site_url('newproduct/select_cat_up') ?>/' + firstcat,
                success: function (data) {
                    var cats = $.parseJSON(data);
                    $.each(cats, function (key, value) {
                        var newOption = new Option(value.cat_name, value.cat_id, true, true);
                        $('#cat').append(newOption).trigger('change');
                    });
                },
                cache: false,
                contentType: false,
                processData: false
            });
        });
        $("a#deleteitem").click(function (e) {
            var relation = $(this).attr("relation");
            var url = "<?= site_url('webproduct/udeleteitem') ?>";
            var obj = {
                relation: relation
            }
            //if (confirm("Дали сте сигурни дека сакате да го додадете производот: " + product)) {
            $.post(url, obj, function (data, textStatus, jqXHR) {
                // alert(data);
            }).done(function (data) {
                location.reload();
            }).fail(function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown + ": " + jqXHR.responseText);
            });
            //}
        });
        $('#web_products_text').summernote({
            height: 250
        });
        $('#tab1').summernote({
            height: 250
        });
        $('#tab2').summernote({
            height: 250
        });

        $('#feat').val(<?= $product['feat'] ?>);
        $('#web_type').val(<?= $product['web_type'] ?>);
        //$('#datetimepicker7').datetimepicker({format: 'YYYY-MM-D H:mm:ss'});
        $('form[id="update"]').validate({
            rules: {
                web_products_name: 'required',
                web_products_slug: 'required',
                web_price: 'required',

                web_products_text: 'required',
            },
            messages: {

            },
            errorPlacement: function (label, element) {
                if (element.is("textarea")) {
                    label.insertAfter(element.next());
                } else {
                    label.insertAfter(element)
                }
            },
            submitHandler: function (form) {
                //tinyMCE.triggerSave();
                var product = new FormData($("#update")[0]);
                $.ajax({
                    url: '<?= site_url('newproduct/update_product') ?>',
                    type: 'POST',
                    data: product,
                    success: function (data) {
                        alert(data);
                        location.reload();
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
        });
        $("#brand").select2({
            width: '100%',
            minimumInputLength: 1,
            placeholder: " Add Brand",
            allowClear: true,
            ajax: {
                url: "<?= site_url('brand/selectbrand') ?>",
                type: "post",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term, // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: false
            }
        });
        $("#cat").select2({
            minimumInputLength: 1,
            placeholder: " Add Category",
            allowClear: true,
            ajax: {
                url: "<?= site_url('categories/select') ?>",
                type: "post",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term, // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: false
            }
        });
        $("#var").select2({
            minimumInputLength: 1,
            placeholder: " Избери варијабла (продукт)",
            allowClear: true,
            ajax: {
                url: "<?= site_url('newproduct/selectproduct') ?>",
                type: "post",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term, // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: false
            }
        });
        $("#web_products_slug").keyup(function () {
            $.ajax({
                type: "POST",
                url: "<?= site_url('newproduct/slugcheck') ?>",
                data: 'keyword=' + $(this).val(),
                beforeSend: function () {
                    $("#web_products_slug").css("background", "#FFF url(<?= site_url('LoaderIcon.gif') ?>) no-repeat 165px");
                },
                success: function (data) {
                    //alert(data);
                    if (data == 0) {
                        $("#web_products_slug").css("background", "#cefdce");
                    } else {
                        $("#web_products_slug").css("background", "#f44242");
                    }
                }
            });
        });
        $('form[id="insertproduct"]').validate({
            //errorClass: 'is-invalid',
            //validClass: 'is-valid',
            rules: {
                kom: 'required',
                product: 'required',
                size: 'required',
                price: 'required',
                stock: 'required',
                delivery_type: 'required',
            },
            messages: {
                kom: 'This value is required.',
                product: 'This value is required.',
                size: 'This value is required.',
                price: 'This value is required.',
                stock: 'This value is required.',
                delivery_type: 'This value is required.',
            },
            errorPlacement: function (label, element) {
                if (element.is("textarea")) {
                    label.insertAfter(element.next());
                } else {
                    label.insertAfter(element)
                }
            },
            submitHandler: function (form) {
                var service = new FormData($("#insertproduct")[0]);
                //alert(service);
                $.ajax({
                    url: '<?= site_url('insertproduct2') ?>',
                    type: 'POST',
                    data: service,
                    beforeSend: function () {
                        // Show image container
                    },
                    success: function (data) {
                        alert(data);
                        // location.reload();
                        $("#modalclose").click();
                    },
                    complete: function (data) {
                        // Hide image container
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
        });
        $("#kom").select2({
            width: '100%',
            minimumInputLength: 1,
            placeholder: "Изберете Коминтент",
            allowClear: true,
            ajax: {
                url: "<?= site_url('user/kom') ?>",
                type: "post",
                dataType: 'json',
                delay: 0,
                data: function (params) {
                    return {
                        searchTerm: params.term,
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: false
            }
        });
        $("a#catdel").click(function (e) {
            var cat_rel_id = $(this).attr("cat_rel_id");

            var obj = {
                cat_rel_id: cat_rel_id
            }
            if (confirm("Избриши ја категоријата")) {
                $.ajax({
                    url: '<?= site_url('webproduct/cat_del') ?>',
                    type: 'POST',
                    data: obj,
                    success: function (data) {
                        //alert(data);
                        location.reload();
                    },
                });
            }
        });
    });
</script>
