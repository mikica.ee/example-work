<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//echo '<pre>';
//print_r($products);
//echo '</pre>';
?>
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">WEB Производи</h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                            <select id='cat_id' name="cat_id" class="form-control">
                                <option value="0">Избери категорија</option>
                                <?php foreach ($cat as $item) { ?>
                                    <option value="<?= $item['cat_id'] ?>"><?= $item['cat_name'] ?> -> <?= $item['cat_slug'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                            <div class="form-group">
                                <select id='product' name="product" class="form-control" data-width="100%">
                                </select>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                            <nav aria-label="Page navigation example">
                                <?= $links ?>
                            </nav>
                        </div>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered first">
                            <tbody>
                                <tr>
                                    <td><h5><b>Бр.</b></h5></td>
                                    <td><h5><b>ИД</b></h5></td>
                                    <td><h5><b>Име на производ</b></h5></td>
                                    <td><h5><b>Опис</b></h5></td>
                                    <td><h5><b>цена</b></h5></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <?php
                                $cs = "0";
                                for ($x = 0; $x < count($products); $x++) {

                                    $data = $products[$x];
                                    $br = $x;
                                    ?>
                                    <tr id="<?= $data['web_products_id'] ?>">
                                        <td><a href="http://akcija.com.mk/product/<?= $data['web_products_slug'] ?>"><?php echo ++$br; ?></a></td>
                                        <td><?= $data['web_products_id'] ?></td>
                                        <td><a data-toggle="popover" data-content="<img height='300' src='<?php echo site_url($data['web_products_img']); ?>' />"><?= $data['web_products_name'] ?></a></td>
                                        <td><?= substr(strip_tags($data['web_products_text']), 0, 200) ?></td>
                                        <td><?= $data['web_price']; ?></td>
                                        <td><a href="<?= site_url('newproduct/update/' . $data['web_products_id']) ?>"><i class="fas fa-edit"></i></a></td>
                                        <td><a href="javascript:;" id="deletep" product="<?= $data['web_products_id'] ?>"><i class="fas fa-trash"></i></a></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <hr>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                        <nav aria-label="Page navigation example">
                            <?= $links ?>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function ($) {
        $("#product").select2({
            minimumInputLength: 1,
            placeholder: "Избери производ",
            allowClear: true,
            ajax: {
                url: "<?= site_url('newproduct/selectproduct_forsim') ?>",
                type: "post",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term, // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: false
            }
        }).on('select2:select', function (e) {
            window.location.href = "<?= site_url('newproduct/update/') ?>"+ "/" + e.params.data.id;
        });

        $("#cat_id").change(function () {
            window.location.href = "<?= site_url('newproduct/listing') ?>" + "?cat=" + $("#cat_id").val();
        });
        $('[data-toggle="popover"]').popover({
            html: true
        });
        $("a#deletep").click(function (e) {
            var web_products_id = $(this).attr("product");
            var url = "<?= site_url('webproduct/delproduct') ?>";
            var obj = {
                web_products_id: web_products_id,
            }
            if (confirm("Избриши го производот: " + web_products_id)) {
                $.post(url, obj, function (data, textStatus, jqXHR) {
                    //alert(data);
                }).done(function (data) {
                    location.reload();
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown + ": " + jqXHR.responseText);
                });
            }
        });
<?php
if (isset($_GET['cat'])) {
    echo '$("#cat_id").val("' . $_GET['cat'] . '");';
}
?>
    });
</script>
