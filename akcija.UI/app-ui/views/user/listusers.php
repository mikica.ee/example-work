<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Листа на Корисници</h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12"> 
                            <select id='id' name="id" class="form-control">
                                <option value="0">Избери тип на корисник</option>
                                <option value="1">Администратор</option>
                                <option value="2">Магационер</option>
                                <option value="3">Диспечер</option>
                                <option value="4">Коминтент</option>
                            </select>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                            <div class="form-group">
                                <select id='name' name="name" class="form-control" data-width="100%">
                                </select
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered first">
                            <tbody>
                                <tr>
                                    <td><h5><b>Бр.</b></h5></td>
                                    <td><h5><b>Корисник</b></h5></td>
                                    <td><h5><b>Име на Корисник</b></h5></td>
                                    <td><h5><b>Email</b></h5></td>
                                    <td><h5><b>Тип на корисник</b></h5></td>
                                    <td><h5><b></b></h5></td>
                                </tr>
                                <?php
                                $type = [1 => "Администратор", 2 => "Магационер", 3 => "Диспечер", 4 => "Коминтент"];
                                $cs = 0;
                                $br = 0;
                                foreach ($res as $user) {
                                    if ($cs) {
                                        $css = ' class="info"';
                                        $cs = 0;
                                    } else {
                                        $css = ' class="active"';
                                        $cs = 1;
                                    }
                                    ?>

                                    <tr<?php echo $css; ?>>
                                        <td><?php echo ++$br; ?></td>
                                        <td><?php echo $user['username']; ?></td>
                                        <td><?php echo $user['name']; ?></td>
                                        <td><?php echo $user['email']; ?></td>
                                        <td><?php echo $type[$user['type']]; ?></td>
                                        <td><a href="<?= site_url('user/updateuser?user=' . $user['id']) ?>"><span class="fas fa-edit"></span></a></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function ($) {
        $("#name").select2({
            minimumInputLength: 1,
            placeholder: "Избери име",
            allowClear: true,
            ajax: {
                url: "<?= site_url('user/select_from_list') ?>",
                type: "post",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term, // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: false
            }
        }).on('select2:select', function (e) {
            window.location.href = "<?= site_url('user/updateuser?user=') ?>" + e.params.data.id;
        });

        $("#id").change(function () {
            window.location.href = "<?= site_url('user/listausers') ?>" + "?user=" + $("#id").val();
        });
        $('[data-toggle="popover"]').popover({
            html: true
        });
<?php
if (isset($_GET['user'])) {
    echo '$("#id").val("' . $_GET['user'] . '");';
}
?>
    });
</script>