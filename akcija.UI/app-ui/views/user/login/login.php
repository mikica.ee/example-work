<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <div class="container">
            <div class="row">
                <?php if (validation_errors()) : ?>
                    <div class="col-md-12">
                        <div class="alert alert-danger" role="alert">
                            <?= validation_errors() ?>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if (isset($error)) : ?>
                    <div class="col-md-12">
                        <div class="alert alert-danger" role="alert">
                            <?= $error ?>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="card-body">
                    <div class="page-header">
                        <h1>Login</h1>
                    </div>
                    <?= form_open() ?>
                    <form>
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input class="form-control form-control-lg" id="username" name="username" type="text" placeholder="Username" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input class="form-control form-control-lg" id="password" name="password" type="password" placeholder="Password">
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg btn-block" value="Login">Login</button>
                    </form>
                </div>
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- container.fluid -->
</div><!-- dashboard wrapper -->