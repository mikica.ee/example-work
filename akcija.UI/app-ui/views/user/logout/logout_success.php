<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <div class="container">
            <div class="row">
                <div class="card-body">
                    <div class="text-center page-header">
                        <h1>Logout success!</h1>
                    </div>
                    <form>
                        <div class="text-center form-group">
                            <label for=""><b>You are now logged out!</b></label>
                        </div>
                    </form>
                </div>
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- container.fluid -->
</div><!-- dashboard wrapper -->