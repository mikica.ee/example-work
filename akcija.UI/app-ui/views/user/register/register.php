<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <?php if (validation_errors()) : ?>
            <div class="col-md-12">
                <div class="alert alert-danger" role="alert">
                    <?= validation_errors() ?>
                </div>
            </div>
        <?php endif; ?>
        <?php if (isset($error)) : ?>
            <div class="col-md-12">
                <div class="alert alert-danger" role="alert">
                    <?= $error ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title text-center"><b>Регистрација на нов корисник</b></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-9 offset-lg-2">
                <?= form_open() ?>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <label for="username"><b>Корисничко име</b></label>
                                <input type="text" class="form-control" id="username" name="username" placeholder="Внесете го корисникот">
                                <p class="help-block">Најмалку 4 карактери, дозволени се само латинични букви и бројки</p>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <label for="name"><b>Име</b></label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Внесете го Името">
                                <p class="help-block">Име на Фирмите Коминтенти</p>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <label for="email"><b>Email:</b></label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Внесете ја email адресата">
                                <p class="help-block">email адресата мора да биде валидна</p>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <label for="email"><b>Тип</b></label>
                                <select class='form-control' name='type'>
                                    <option value="1">Администратор</option>
                                    <option value="2">Магационер</option>
                                    <option value="3">Диспечер</option>
                                    <option value="4">Коминтент</option>
                                </select>
                                <p class="help-block">Тип на корисник</p>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <label for="password"><b>Лозинка - Внеси само доколку сакаш да промениш</b></label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Внесете ја лозинката">
                                <p class="help-block">Најмалку 6 карактери</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <label for="password_confirm"><b>Потврди ја лозинката</b></label>
                                <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Повторно внесете ја лозинката">
                                <p class="help-block">Лозинките треба да се софпаѓаат</p>
                            </div>
                        </div>
                    </div>
                    <div class="row pt-2 pt-sm-2 mt-1">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <p class="text-center">
                                <button type="submit" class="btn btn-primary btn-block" value="Регистрирај" >Регистрирај</button>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>