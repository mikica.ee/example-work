<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <div class="container">
            <div class="row">
                <div class="card-body">
                    <div class="text-center page-header">
                        <h1>Thank you for registering your new account!</h1>
                    </div>
                    <form>
                        <div class="text-center form-group">
                            <label for=""><b>You have successfully registered. Please check your email inbox to confirm your email address.</b></label>
                        </div>
                    </form>
                </div>
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- container.fluid -->
</div><!-- dashboard wrapper -->