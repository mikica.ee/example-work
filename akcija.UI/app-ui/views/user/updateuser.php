<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title text-center"><b>Ажурирање на корисник</b></h2>
                </div>
            </div>
        </div>
        <form name="update_user" action="javascript:;">
            <div class="row">
                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-9 offset-lg-2">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="username"><b>Корисничко име: <?= $res['username'] ?></b></label>
                                    <input type="text" class="form-control" id="username" name="username" value="<?= $res['username'] ?>">
                                    <p class="help-block">Најмалку 4 карактери, дозволени се само латинични букви и бројки</p>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <label for="name"><b>Име: <?= $res['name'] ?></b></label>
                                    <input type="text" class="form-control" id="name" name="name"  value="<?= $res['name'] ?>">
                                    <p class="help-block">Име на Фирмите Коминтенти</p>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <label for="email"><b>Email: <?= $res['email'] ?></b></label>
                                    <input type="email" class="form-control" id="email" name="email" value="<?= $res['email'] ?>">
                                    <p class="help-block">email адресата мора да биде валидна</p>
                                </div>
                            </div>
                            <br>
                            <?php
                            $admin = '';
                            $mag = '';
                            $dis = '';
                            $kom = '';
                            switch ($res['type']) {
                                case 1:
                                    $admin = 'selected="selected"';
                                    break;
                                case 2:
                                    $mag = 'selected="selected"';
                                    break;
                                case 3:
                                    $dis = 'selected="selected"';
                                    break;
                                case 4:
                                    $kom = 'selected="selected"';
                                    break;
                            }
                            ?>
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <label for="email"><b>Тип</b></label>
                                    <select id="type" class='form-control' name='type'>
                                        <option value="1"<?= $admin ?>>Администратор</option>
                                        <option value="2"<?= $mag ?>>Магационер</option>
                                        <option value="3"<?= $dis ?>>Диспечер</option>
                                        <option value="4"<?= $kom ?>>Коминтент</option>
                                    </select>
                                    <p class="help-block">Тип на корисник</p>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <label for="password"><b>Лозинка - Внеси само доколку сакаш да промениш</b></label>
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Внесете лозинката само доколку сакате да ја промените">
                                    <p class="help-block">Најмалку 6 карактери</p>
                                </div>
                            </div>
                        </div>
                        <div class="row pt-2 pt-sm-2 mt-1">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <p class="text-center">
                                    <button id="update_user" type="submit" class="btn btn-primary btn-block" value="Ажурирај" <?= $res['id'] ?>">Ажурирај</button>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function ($) {
        $("#update_user").click(function (e) {
            var user_id = $(this).attr("user_id");
            var username = $("#username").val();
            var name = $("#name").val();
            var email = $("#email").val();
            var type = $("#type").val();
            var password = $("#password").val();
            var url = "<?php echo site_url('user/update') ?>";
            var obj = {
                user_id: user_id,
                username: username,
                name: name,
                email: email,
                type: type,
                password: password,
            }
            if (confirm("Ажурирај го корисникот : " + username)) {
                $.post(url, obj, function (data, textStatus, jqXHR) {
                    alert(data);
                }).done(function (data) {
                    location.reload();
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown + ": " + jqXHR.responseText);
                });
            }
        });
    });
</script>