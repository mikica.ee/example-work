<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Feat_model extends CI_Model {

//----------------------------------------------------------------------------//
//    public function __construct() {
//        parent::__construct();
//        $this->table_name = 'faq';
//        $this->table_index = 'faq_id';
//        $this->table_index2 = null;
//        $this->joined_tables = "";

//----------------------------------------------------------------------------//

       public function __construct() {

        parent::__construct();
        $this->load->database();
    }

    public function insert($data) {
        return $this->db->insert('feat', $data);
    }
    

    public function select() {
        $this->db->select('*');
        $this->db->from('feat');
        $this->db->order_by("feat_id", "desc");
        $query = $this->db->get();
        return $query->result_array();
    }
    public function select_type($id) {
        $this->db->select('*');
        $this->db->from('web_products');
        $this->db->join('feat', 'web_products.web_products_id = feat.feat_product');
        $this->db->where('feat_type', $id);
        $this->db->order_by("feat_time", "desc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function select_from_id($id) {
        $this->db->select('*');
        $this->db->from('feat');
        $this->db->where('feat_id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function update($data) {
        $where = "feat_id = " . $data['feat_id'];
        return $this->db->update('feat', $data, $where);
    }
    public function delete($id) {
        return $this->db->delete('feat', array('feat_id' => $id));
        
    }
}