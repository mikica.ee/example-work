<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 *
 * @extends CI_Model
 */
class Ininvoice_model extends CI_Model {

    public function __construct() {

        parent::__construct();
        $this->load->database();
    }

    public function insert($data) {
        return $this->db->insert('in_invoice', $data);
    }
    public function select($limit, $start) {
        $this->db->select('*');
        $this->db->from('in_invoice');
        $this->db->order_by("in_invoice_id", "asc");
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function count() {
        return $this->db->count_all("in_invoice");
    }

   

    public function select_from_no($no) {
        $this->db->select('*');
        $this->db->from('in_invoice');
        $this->db->where('in_invoice_no', $no);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function update($data) {
        $where = "in_invoice_id = " . $data['in_invoice_id'];
        return $this->db->update('in_invoice', $data, $where);
    }

}
