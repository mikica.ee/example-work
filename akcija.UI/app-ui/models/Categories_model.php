<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 * 
 * @extends CI_Model
 */
class Categories_model extends CI_Model {

    /**
     * __construct function.
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->database();
    }

    public function select_cat($id) {
        $this->db->select('*');
        $this->db->from('categories');
        $this->db->where('cat_id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function insert_cat_rel($data) {
        return $this->db->insert('cat_rel', $data);
    }

    public function del_cat_rel($id) {
        return $this->db->delete('cat_rel', array('cat_rel_id' => $id));
    }

    public function select_cat_rel($cat = null, $product = null) {
        $this->db->select('*');
        $this->db->from('cat_rel');
        if ($cat) {
            $this->db->where('cat_rel_c', $cat);
        }
        if ($product) {
            $this->db->where('cat_rel_p', $product);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function select_for_cat($id) {
        $this->db->select('cat_id, cat_name, cat_rel_id');
        $this->db->from('categories');
        $this->db->join('cat_rel', 'cat_rel.cat_rel_c = categories.cat_id');
        $this->db->join('web_products', 'web_products.web_products_id = cat_rel.cat_rel_p');
        $this->db->where('web_products_id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function update($data) {
        $where = "cat_id = " . $data['cat_id'];
        return $this->db->update('categories', $data, $where);
    }
    public function updatefeat($data) {
        $where = "cat_feat = " . $data['cat_feat'];
        return $this->db->update('categories', $data, $where);
    }

    public function insert($data) {
        return $this->db->insert('categories', $data);
    }

    public function get_categories() {

        $this->db->select('*');
        $this->db->from('categories');
        $this->db->where('parent_id', 0);

        $parent = $this->db->get();

        $categories = $parent->result();
        $i = 0;
        foreach ($categories as $p_cat) {

            $categories[$i]->sub = $this->sub_categories($p_cat->cat_id);
            $i++;
        }

        return $categories;
    }

    public function sub_categories($id) {

        $this->db->select('*');
        $this->db->from('categories');
        $this->db->where('parent_id', $id);

        $child = $this->db->get();
        $categories = $child->result();
        $i = 0;
        foreach ($categories as $p_cat) {

            $categories[$i]->sub = $this->sub_categories($p_cat->cat_id);
            $i++;
        }
        //$categories = json_decode(json_encode($categories));
        return $categories;
    }

    public function sub_categories_slug($slug) {

        $this->db->select('*');
        $this->db->from('categories');
        $this->db->where('cat_slug', $slug);

        $child = $this->db->get();
        $categories = $child->result();
        $i = 0;
        foreach ($categories as $p_cat) {

            $categories[$i]->sub = $this->sub_categories($p_cat->cat_id);
            $i++;
        }
        //$categories = json_decode(json_encode($categories));
        return $categories;
    }

}
