<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 *
 * @extends CI_Model
 */
class Brand_model extends CI_Model {

    public function __construct() {

        parent::__construct();
        $this->load->database();
    }

    public function insert($data) {
        return $this->db->insert('brand', $data);
    }
//        public function select() {
//        $this->db->select('*');
//        $this->db->from('brand');
//        $this->db->order_by("brand_id", "desc");
//        $query = $this->db->get();
//        return $query->result_array();
//    }

    public function select($limit, $start) {
        $this->db->select('*');
        $this->db->from('brand');
        $this->db->order_by("brand_id", "desc");
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function count() {
        return $this->db->count_all("brand");
    }

    public function select_from_id($id) {
        $this->db->select('*');
        $this->db->from('brand');
        $this->db->where('brand_id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function select_from_name($name) {
        $this->db->select('*');
        $this->db->from('brand');
        $this->db->where('brand_name', $name);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function update($data) {
        $where = "brand_id = " . $data['brand_id'];
        return $this->db->update('brand', $data, $where);
    }
}
